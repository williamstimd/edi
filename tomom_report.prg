* piv_mom6 reports the errors in the orders to a file that later can
* be printed
* 3-19-00 	added error 6
* 10-24-00 	changed the way comments are reported
* 11-13-01 	recompile to make the report program use a idx instead of an cdx index
* 06/03/02 	jc deleted the c_client initilization since it is a public varriable set up in init and reinitilized in the main program 
* 07-13-02 	added invalid ship method ( we had N for both comment and invalid ship method) 
* 11-28-06 	added code R for restricted product pending client review

@21,10 say "The last step.. building a exception report to file"
*C_client = "ADP"  && 06-03-02 don't need this since we set this varriable at the top of the main program 
*C_client="ROX"
set escape on
select 4
use
use H:\report\tomom2\err_log_rep exclusive	&& an extended file for the reporting of errors
zap
index on mom_order to err_log_rep
select 3
*use H:\REPORT\tomom2\err_log.rox exclusive
use H:\REPORT\tomom2\err_log.adp exclusive	
*use ERR_LOG shared
set filter to ! reported
LASTERR =RECNO()
go top
do while .not. EOF()
	MEM_ERR=RTRIM(ERRORS)
    done=.f.
    do while ! done 
    	CUR_ERR=SUBSTR(MEM_ERR,1,1)
    	select 4
    	append blank
    	replace d.import_num with c.import_num  
    	replace d.mom_cust with c.mom_cust
    	replace d.mom_order with c.mom_order
        do case
                case CUR_ERR="1"
                    replace ERROR_DESC with "No bill to address"
                case CUR_ERR="2"			&& assigned in tomom_cms
                    replace error_desc with "No bill to city"
                case CUR_ERR="3"			&& assigned in fix state
                    replace error_desc with "zip code does not match state"
                case CUR_ERR="4"			&& assigned in tomom_items
                    replace error_desc with "quanity on an item missing"
                case CUR_ERR="5"
                    replace error_desc with "no order number on the order"
                case CUR_ERR="6"
                    replace error_desc with "catalog code does not match adcode"
                case CUR_ERR="7"
                    replace error_desc with "error with on line charging, do not release until resolved"
                case CUR_ERR="8"
                    replace error_desc with "First try to charge credit card on line was declined "
                case CUR_ERR="9"
                    replace error_desc with "Could not find the cust record "+str(tomom.custid) 
                case CUR_ERR="A"
                    replace error_desc with "missing source_key"
                case cur_err="B"			&& assigned in fixstate
                	replace error_desc with "Invalid Province Abbreviation "
                case CUR_ERR="C"			&&fixstate
                    replace error_desc with "in valid country name"
                case CUR_ERR="c"			&& assigned in fix state
                    replace error_desc with "Changed country to Canada"
                case CUR_ERR="D"			&& assigned in tomom_cms
                    replace error_desc with "Check payment without check #"
                case CUR_ERR="d"			&& assigned in tomom_cms
                    replace error_desc with "invalid payment method"
                case CUR_ERR="E"			&& assigned in tomom_cms
                    replace error_desc with "credit card number missing"
                case CUR_ERR="F"			&& assigned in tomom_cms
                    replace error_desc with "invalid credit card type"
                case CUR_ERR="G"			&& assigned in tomom_cms
                    replace error_desc with "invalid credit card number"
                case CUR_ERR="H"			&& assigned in tomom_cms
                    replace error_desc with "expired credit card"
                case CUR_ERR="I"			&& assigned in tomom_items
                    replace error_desc with "Pricing exception on item"
                case CUR_ERR="J"
                    replace error_desc with "invalid product code"
                case CUR_ERR="K"
                    replace error_desc with "shipping price exception"
                case CUR_ERR="L"			&& assigned in tomom_final
                    replace error_desc with "tax calculation error"
                case CUR_ERR="M"
                    replace error_desc with "check order with out check"
                case CUR_ERR="N"			&& assigned in adp_tomom_remedy
                    replace error_desc with "Review comment-"+alltrim(c.comment)
                    * 10-24-00 take out -  replace A->comment with F->COMMENT
                case CUR_ERR="O"			&& assigned in adp_tomom_remedy
                    replace error_desc with "Invalid Ship method"				
                case CUR_ERR="P"
                    replace error_desc with "Purchase order on hold"
                case CUR_ERR="Q"
                    replace error_desc with "Test Credit Card Order"
				case CUR_ERR="R"
                    replace error_desc with "Restricted Items - pending client review"               
                case cur_err="S"		&& assigned in fixstate
                	replace error_desc with "State not found in US & Canada State / Province "
                case cur_err="T"		&& assigned in tomom_custb
                	 replace error_desc with "Tax exempt customer on hold "
        endcase
        *messagebox("1 "+mem_err)
        MEM_ERR =substr(mem_err,2,10)
        if len(alltrim(mem_err))=0
        	done= .t.
        endif
    enddo
    select 3
    replace reported with .t.
    skip
enddo
select 4
report form H:\report\tomom2\err_log_rep preview nowait
report form h:\report\tomom2\err_log_rep ALL NOCONSOLE TO PRINTER prompt
copy to h:\report\adp_in\logerr xl5
