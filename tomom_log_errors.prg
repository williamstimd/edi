* program logs import errors 
* TOMOM_LOG_ERRORS
*	10-24-00	jc added logic to add comment when the error is we have a comment
select err_log
if N_error_to_log=0				&& first error to log on this order
	append blank
	replace import_num with c_odr_num
	*replace mom_cust with M_custnum
	*replace mom_order with n_order
	replace errors with c_error_code
else 
	*REPLACE mom_cust with M_custnum	&& we might have more data on this order so writeover 
	*REPLACE mom_order with n_order
	replace errors with rtrim(errors)+c_error_code
endif
if c_error_code="N"
	replace err_log.comment with c_comment
endif
*if c_error_code<>"e"  6-28-11 we need this count to be over 0 in order to create a telemar record 
if c_error_code<>"!"
	N_error_to_log = n_error_to_log +1
endif