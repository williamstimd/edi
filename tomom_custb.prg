* This program is designed to pre format customer data and load up the cust.dbf table 
* 
* 
* 03-25-00 jc	added if statement before we put in the email address
*				changed entry date to populate for new cust records not old ones 
* 10-24-00 jc	changed the comment section to have the imported comment append to the end of the existing comment
* 07-15-01 jc	changed the determiation for old guy to include last name (two guys at 3com corp with same last name different first got confused) 
* 05-28-02 jc	to find a customer match we were not upper(c_firstname) and therefore we almost never found a match
*				also noticed that we don't find a match unless it is the only guy in that zip code with that last name
*				so I changed the code to loop through the mom customer record to see if there are more records that will match 
* 07-30-2002 JC mark found that we were writing over his ctype2 with default data, as a temp patch we 
*				changed tomom_custs to hold off on the write when we have data in the file 
* 03-20-2003 jc	changed the way we find same guy 
* 09-03-2003 jc changed to inherit from a same guy the tax exempt flag c_taxexempt
* 11-11-2003 jc changed the fixstate program to work better when we have both a good country code and a good zip but a bad state
* 05-06-2004 jc	added unlock function to eliminate the freeze conditons
* 11-28-2006 JC added code so we are still on the correct cust when we find a previous cust in the look up loop 
* 12-16-2010 JC moved around the rlock on same guy to allow us to use an existing customer record even if some one is in that file just now
* 06-24-2015 JC added code to strip out comma from address lines 
* 07-23-2015 JC added code to also strip ut comma from names, company and city 
set near on && needed so we get to the nearest zipcode when we search the zip file 

* check to see if we have a good country code 
c_county = "  "					&& we must set to blank so we don't inherit the old one 
t_country=c_country				&& temporary varriable to pass to the fixcountry.prg 
* m_country=space(3)			&& set a blank return varriable to rec from the fixcountry.prg 
*								on 4-12-00 noticed this was not always assigned in the program and it was better to atleast pass back the orig data
m_country=c_country				&& set a blank return varriable to rec from the fixcountry.prg
t_rcountry=c_rcountry  			&& a temp varriable to pass the country name to the fixcountry.prg
c_lastname = sTRTRAN(c_LASTNAME,",","")		&& 7-23-15 added strtran to clean up incomming commas
C_FIRSTNAME= STRTRAN(C_FIRSTNAME,",","")	&& 7-23-15 added strtran to clean up incomming commas
*C_COMPANY =  STRTRAN(C_COMPANY,",","")		&& 7-23-15 added strtran to clean up incomming commas
c_address1 = STRTRAN(c_address1,",","") 	&& 6-24-15 added since are seeing this more and more from online sites 
*c_address2 = STRTRAN(c_address2,",","") 	&& 6-24-15 added since are seeing this more and more from online sites 
*C_CITY = STRTRAN(C_CITY,",","")				&& 7-23-15 added strt
c_city=ALLTRIM(c_city)
c_address2 = ALLTRIM(c_address2)
C_COMPANY =  ALLTRIM(C_COMPANY)

DO CASE
	CASE INLIST(C_STATE , "NH", "PR", "VI") AND LEN(ALLTRIM(C_ZIPCODE))=3
		C_ZIPCODE = "00"+ALLTRIM(C_ZIPCODE)

	CASE INLIST(C_STATE, "MA","RI","NH","RI","ME","VT","CT", "NJ") AND LEN(ALLTRIM(C_ZIPCODE))=4
		C_ZIPCODE = "0"+ALLTRIM(C_ZIPCODE)
ENDCASE	
do h:\report\fix\fixcountry		&& validate country and leave the country in position to assign tax
c_country=m_country
T_COUNTRY=M_COUNTRY
* below is a quick format clean up for a zip plus 4 zipcode without a hyphen 12-19-06
if len(alltrim(c_zipcode))=9 and at("-",c_zipcode)=0 and c_country="001" and ! isalpha(c_zipcode) 
	c_zipcode=substr(c_zipcode,1,5)+"-"+substr(c_zipcode,6,4)			
endif
* check to see if we have a good state code 
T_state=c_state				&& temporary varriable to pass to the fixstate.prg 
*m_state=space(2)			&& set a blank return variable to rec from fixstate.prg changed 4-12-00 to atleast pass back the orig data if nothing else is assigned in the program 
m_state=c_state				&& set a blank return variable to rec from fixstate.prg
t_zip=c_zipcode
DO h:\report\fix\FIXSTATE	&& fix state and leaves the zipcode file in position to calculate tax
C_STATE=M_STATE
C_COUNTRY=M_COUNTRY 		&& RESET THIS variable INCASE WE CHANGEd IT IN THE FIX STATE PROGRAM
oldguy=.f.

select cust
set order to iem_zip
*messagebox("1")
if not isnull(c_zipcode) and not isnull(c_lastname)	&& found that it is still possible for either one of these variable to be null 
	seek substr(C_zipcode,1,5)+rtrim(upper(C_lastname))
	*messagebox("2")
	if found() 
		gotcust=.f.
		*messagebox("3")
		do while substr(C_zipcode,1,5)+rtrim(upper(C_lastname))=substr(cust.zipcode,1,5)+rtrim(upper(cust.lastname))and .not. gotcust	&& 	3-20-03 Changed this line from the one below where we did not compare last names correclty 
			* do while substr(C_zipcode,1,5)+rtrim(upper(C_lastname))=substr(cust.zipcode,1,5)+rtrim(upper(c_lastname))and .not. gotcust 
			*messagebox("4")
			*if rtrim(upper(c_address1))=rtrim(upper(cust->addr))and rtrim(upper(c_company))=rtrim(upper(cust->company)) and cust.addr<>space(40) .and. rtrim(upper(c_firstname))=rtrim(upper(cust->firstname)).AND.RLOCK()  && 8-22-07 added a company name check because eco was entering store numbers in the company name 
			if rtrim(upper(c_address1))=rtrim(upper(cust->addr))and rtrim(upper(c_company))=rtrim(upper(cust->company)) and cust.addr<>space(40) .and. rtrim(upper(c_firstname))=rtrim(upper(cust->firstname)) && 12-16-10 had to remove the rlock reqirement because we were not finding the 
																																																				&& the Digital wish bill to address too often move the requirment to below
																																																							
					*if rtrim(upper(c_address1))=rtrim(upper(cust->addr)) and cust.addr<>space(40) .and. rtrim(upper(c_firstname))=rtrim(upper(cust->firstname)).AND.RLOCK()  && 7/2004 updated with and cust.addr<>space(40) 
				* we found this customer in the database and now all we will do is update that record 
	    		gotcust = .t.
	    		oldguy =.t.
	    		*messagebox("5")
	   	 		N_cust_rec = n_cust_rec +1 			&& keep in memory a count on new records added
	    		n_CUSTNUM=CUST->CUSTNUM				&& A variable THAT IS RESET EACH NEW ORDER AND USED IN LOGERROR
	    		m_custnum=cust->custnum				&& store into memory the customer number ( we will use this in cms if needed) 
	    		m_lastord=cust->orderrec			&& store the order number for the last order number ( we will use this for cms if needed) 
	    		* update a old record
	    		if RLOCK() 
	    			replace cust->altnum with c_ALTNUM
					*replace cust->COMPANY with proper(c_COMPANY)  **  8-22-07 decided to make a new record if the company name is not the same.
					IF LEN(STRTRAN(c_ADDRESS2,",",""))>0 &&AND LEN(ALLTRIM(cust->addr2))=0
						replace cust->addr2 with proper(STRTRAN(c_ADDRESS2,",",""))
					ENDIF
					replace cust.custtype with "O"
					m_phone=c_phone
					do H:\REPORT\FIX\fixphone
					replace cust->phone with p_phone
		
					m_phone=c_phone2
					do H:\REPORT\FIX\fixphone
					replace cust->phone2 with p_phone
					replace cust.title with proper(c_title)
					replace cust->email with c_email
					replace cust.shiplist WITH c_cust_shiplist
					replace cust->ctype with upper(c_ctype)
					replace cust->last_ad with upper(c_SOURCE_KEY)
					replace cust->odr_date with d_ORDER_DATE
					IF c_COMMENT<>SPACE(40)
						do case 
							case cust.comment=space(40)
								replace cust.comment with c_comment
							case cust.comment<>space(40) .and. cust.comment2=space(40)
								replace cust.comment2 with cust.comment
								replace cust.comment with c_comment
							case len(alltrim(cust.comment))<len(alltrim(cust.comment2))
								replace cust.comment with alltrim(cust.comment)+c_comment
							otherwise
		    					replace cust->comment2 with alltrim(cust.comment2)+c_COMMENt
		    			endcase
					ENDIF
					replace cust->sales_id with upper(c_SALES_ID)
					if cust.ctype2 = " "
						REPLACE CUST->CTYPE2 WITH upper(SUBSTR(c_CTYPE2,1,2))
					endif
					if cust.ctype3 = "  "
						replace cust->ctype3 with UPPER(substr(C_CTYPE3,1,3))
					endif	
					unlock && 05-06-04 added to release record 
				endif && we could get a lock on this record to update, if not we are going to use it without updating it
				
				if cust.exempt and cust.n_exempt and cust.tax_id<>"    "	&& 09-03-03 added this in to work out tax exempt customers 
	    			c_taxexempt ="YES"
	    		endif		
	    		
	    		IF c_taxexempt="YES" AND RLOCK()		&& 11-09-11 ADDED TO SYSTEM 
	    			REPLACE CUST.EXEMPT WITH .T.
	    			REPLACE CUST.N_EXEMPT WITH .T.
	    			IF VARTYPE(c_tax_id) <> 'U'
	    				replace cust.tax_id WITH c_tax_id
	    			endif
	    		ENDIF
	    		
	    		if (cust.exempt or cust.n_exempt or cust.tax_id<>"    ") and (c_TAXEXEMPT=" ".or. upper(c_taxexempt)="N")  	&& 09-03-03 removed in favor of a system to not collect tax on exempts customers 
		    		replace cust->comment2 with "import order tax exempt please check"
		     		c_error_code="T"
		     		DO H:\report\tomom2\TOMOM_LOG_ERRORS
		     		SELECT CUST
				endif
			
*!*					if c_TAXEXEMPT<>" ".and. upper(c_taxexempt)<>"N"		&& 09-03-03 removed in favor of a system to not collect tax on exempts customers 
*!*			    		replace cust->comment2 with "import order tax exempt"
*!*			     		c_error_code="T"
*!*			     		DO TOMOM_LOG_ERRORS
*!*			     		SELECT CUST
*!*					endif
				
			endif && we have found a match in mom
			if .not. gotcust AND NOT EOF() 	&& 11-28-06 added so we are still on the correct cust record when we break this loop 
				select cust
				skip
			endif
		enddo 	&& look at all customer records in this zipcode with this lastname
	endif		&& found at least one customer record in mom with a matching zipcode and last name
ENDIF

if ! oldguy && we did not find a matching customer record in mom and so we must add one
    ** assign a new customer number
    DONE=.F.
    m_lastord=0			&& we will report this to the cms.dbf file if needed in tomom.cust
	select sequence
	seek "CUST"
	if ! found()
		select inlog	&& 03-08-05 changed to inlog from login 
		replace problems with "No cust sequence"
		MESSAGEBOX("PROGRAM ABORTED NO CMS IN SEQUENCE FILE")
		quit
	else
		try=0
		do while ! done .and. try<10		&& 10 trys to get a lock on the sequecne file	
			if rlock()
				replace next_val with next_val+1
				m_custnum=next_val
				done=.t.
			endif
			try=try+1
		enddo
	ENDIF
	IF ! DONE
		MESSAGEBOX("I can't get a lock on the sequence file")
	endif
	n_CUSTNUM=m_CUSTNUM		&& A variable THAT IS RESET EACH NEW ORDER AND USED IN LOGERROR
    select cust
    ** create a new customer record
    append blank
    N_cust_rec = n_cust_rec +1 			&& keep in memory a count on new records added
	replace cust->custnum with m_custnum
	replace cust->altnum with ALLTRIM(UPPER(c_altnum))
	replace cust->custtype with "P"
	replace cust.title with proper(c_title) 	&& 11-09-02 added for adaptec import 
	replace cust->lastname with PROPER(STRTRAN(c_LASTNAME,'"',''))		&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->FIRSTNAME with PROPER(STRTRAN(C_FIRSTNAME,'"',''))	&& 7-23-15 added strtran to clean up incomming quotes
	*replace cust->COMPANY with PROPER(STRTRAN(C_COMPANY,'"',''))		&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->COMPANY with PROPER(C_COMPANY)		&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->ADDR with PROPER(STRTRAN(C_ADDRESS1,'"',''))			&& 7-23-15 added strtran to clean up incomming quotes
	*replace cust->addr2 with PROPER(STRTRAN(C_ADDRESS2,'"',''))			&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->addr2 with PROPER(C_ADDRESS2)			&& 7-23-15 added strtran to clean up incomming quotes
	*replace cust->city with PROPER(STRTRAN(C_CITY,'"',''))				&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->city with PROPER(C_CITY)				&& 7-23-15 added strtran to clean up incomming quotes
	replace cust->county with c_county
	replace cust->state with UPPER(C_STATE)
	replace cust->zipcode with UPPER(C_ZIPCODE)
	replace cust->country with c_COUNTRY
	M_PHONE=C_PHONE
	DO h:\report\fix\FIXPHONE			&& STANDARIZE THE PHONE FORMAT
	replace cust->phone with P_PHONE

	M_PHONE=C_PHONE2
	DO h:\report\fix\FIXPHONE			&& STANDARIZE THE PHONE FORMAT
	replace cust->phone2 with P_PHONE
	if at("@",c_email)>0
		replace cust->email with c_email
	ENDIF
	replace cust.shiplist WITH c_cust_shiplist
	replace cust->orig_ad with UPPER(C_SOURCE_KEY)
	replace cust->ctype with UPPER(C_ctype)
	replace cust->last_ad with UPPER(C_SOURCE_KEY)
	replace cust->odr_date with D_ORDER_DATE
	replace cust->entrydate with date()
	replace cust->comment with c_COMMENT
	
	IF VARTYPE(c_taxexempt) <> 'U'
 		IF c_taxexempt="YES" 		&& 11-17-16 ADDED TO SYSTEM it was missing for new customer records but was in for old guy
			REPLACE CUST.EXEMPT WITH .T.
			REPLACE CUST.N_EXEMPT WITH .T.
			IF VARTYPE(c_tax_id) <> 'U'
				replace cust.tax_id WITH c_tax_id
			endif
		ENDIF
	endif
*!*		if c_TAXEXEMPT<>" ".and. upper(c_taxexempt)<>"N"				&& 09-03-03 out in favor of a system to mark some customers as tax exempt
*!*		    replace cust->comment2 with "import order tax exempt"
*!*		     c_error_code="T"
*!*		     DO TOMOM_LOG_ERRORS
*!*		     SELECT CUST
*!*		endif
	replace cust->sales_id with UPPER(C_SALES_ID)
	replace cust->ctype2 with UPPER(substr(C_CTYPE2,1,2))
	replace cust->ctype3 with UPPER(substr(C_CTYPE3,1,3))	
	
endif


N_billto=cust.custnum