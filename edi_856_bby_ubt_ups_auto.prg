** 12-07-06 JC 	This program has been designed to create a 856 order shipment notice for 
**				costco related orders 
** 
**
** 12-18-06	JC	The resulting files are sent via FTP to Sterling Informaiton Broker.  
**				our Mailbox Id : 	SJMINFTO 	- Data Slot outbound
**									SJMINFTI	- Data Slot inbound
**									SJMINFTR	- Report Slot
**				Password			4088482
**				Communications Id :	12			- 12=our phone number is our id 
**												  ZZ= our name is our id
**			prod Communicaitons Id	4088482282	- What Patrik set up with Sterling as our account
**									4088483525	- is what we use with GSX
**
** 				IP address for FTP 	209.95.224.133 default ports 20 and 21 data needs to be sent
**									Ascii (not binary) and anything over 512 bytes needs to be 
**									streamed.
** 
**				web address : 		sciftp.commerce.stercomm.com
**				
**  Note that you must initally log in the outbound slot and perform a change Directory command
**	to access any other slots.  Also be sure to enter the slots in all caps

**	Technical support hotline number for your mailbox is 877-432-4300.  

** 	in this program we use 
**							Segment Terminator 	-  a line feed ( Market point we use ~"
**							element seperator	-  * (Jc Penny uses a bell character)
**							subelement seperator-  > on the ISA line 

**	12-24-06	JC 	moved ST segment inside the order loop
**					created variable N_Inv_run_count to us in the GE segment	

**	12-26-06	JC	added code to deal with times when we dont have any orders to process and to move the send funciton indide the loop for good orders 
**	01-02-06 	JC 	added code to only run the routine if cms.userid="CCO" and invoice.authcode="  " instead of just authocode="  "
** 	04-16-07	JC	changed location of CDI
**  04-24-07 	JC  fixed bug that assumse the work station is mapped to M for ro-gilroy-cdi-1
** 	05-20-07	JC 	modify to allow auto run at 2 pm each day
**  07-31-08 	jC 	modified for generic use for new EDI clients
**  11-07-08 	JC  review for OSH
** 05-05-09		jc 	USED FOR PRODUCTION ON COSTCO DROP SHIP 
** 11-16-09 	JC 	several updates to deal with invoice b 
**  11-17-09 	JC  added code to keep us from working on a order without a edi_cms record
**  01-22-10 	JC	quick recompile to clean up opening of edi_ucc128 
**  12-15-10 	JC 	UPDATE TO WORK WITH NRI COSTCO
** 	12-05-11 	JC 	modify for use with bluestem
**  02-21-12 	JC 	additional work, this will be ready to compile after one or two more checked runs
**  03-15-12	SA	Added new error handler
**  03-19-12 	JC	changed as per update form Amanda Wooden
**	04-25-12 	JC  corrected spelling of edi_ucc128 in line 722
**	05-04-12	SA	Commented out line of code that attempts to move a file to the SENT directory when it was already moved to SENT by FTP_STERLING2.prg.
**  06-14-12 	JC	copied from bluestem to work with best Buy
**  08-10-12	jc 	Set to work for on ASN for each box shipped via small parcel carrier
**	10-21-12 	jc  changed the code to put in UPSN instead of UPSG as per email form Best Buy Parallel edi paper check list
** 	02-01-13	JC 	updated with edi_cms.sh_856 data
**  09-12-13	jc	UPDATED 
**  08-08-16	JC  fixed to work on invoice b
**  01-30-17    TW  modified for automation
**
ON ERROR DO H:\REPORT\ADVANCED_ERR_HANDLER\ERR_HANDLE_CALL.PRG WITH ;
  ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
 
	  
	public C_program_name 
	public c_username
	public c_machine
	public c_currentpath
	public c_executable
	PUBLIC c_compile_date
	public c_exe_size
	PUBLIC c_home
	
	C_program_name = 'EDI_856_Best_Buy_ubt_AUTO'						&&<-------------Enter program name here!
	c_home= 'H:\REPORT\EDI\'											&&<-------------Enter program home directory here!
	c_username = alltrim(substr(sys(0),at("#",sys(0))+1,len(sys(0))))  	&& capture the user name
	c_machine = alltrim(substr(sys(0),1,at("#",sys(0))-1))				&& capture the workstation 
	c_currentpath = sys(5)+sys(2003)									&& capture the current path 
	c_executable = SYS(16)	

	fso = CreateObject("Scripting.FileSystemObject")
	lmtest=fso.getfile(c_executable)
	c_compile_date=TTOC(lmtest.datelastmodified)
	c_exe_size=lmtest.size
	c_exe_size=ALLTRIM(STR(c_exe_size)) 
 
 
STRTOFILE(TTOC(DATETIME())+' PROGRAM START - '+c_program_name+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)	
 
 
set delete on
set safety off
clear
close database
cd h:\report\company
c_program_name = "EDI_856_Best_Buy_ubt"
@1,1 say " Program create an EDI 856 report based on invoice transactions "

public ftp_name
public ftp_login
public ftp_pword
public ftp_path
public ftp_send
public ftp_rename 
public ftp_bye
public ftp_filename
public ftp_script


IF vartype(n_target_order)="U"	&& This will let us know if we have already created these variables
	PUBLIC n_target_order
	public c_target_inpart
endif


cd h:\report\company

SELECT 1
use H:\mom\profile alias company shared
set order to code 
locate for code="UBT"
*do form h:\report\company\company to c_code	5-13-09 removed to go faster 
if code="UBT"
	c_code="yes"
	public m_dataloc
	*m_dataloc="\\ro-gilroy-ad-1\win_mom"+company.dataloc
	m_dataloc=alltriM(company.drive)+company.dataloc
	*messagebox(m_dataloc)
	M_CLIENT=PROPER(COMPANY.NAME)
	c_target_client = company.code
else
	quit
endif


select 101
if used("master_prg_run_log")
	select master_prg_run_Log
else
	select 101
	use H:\mom\master_prg_run_log shared
endif
append blank
replace prg_name with c_program_name
replace client with company.code
replace date with date()
replace time with time()
replace comments with "started program"
*!*	REPLACE FUTURE2 WITH STR(N_LAST_TRANS_ID)							&& this is our start point for the this run ( the last record we looked at for the last report 
*!*	replace future3 with str(n_last_trans_id) 							&& put this in for now in case we don't find any new record for this report



public c_filename 
public c_tempname 
public N_total_ref_needed
public N_total_run_count 
public runline

select 288
use h:\report\edi\edi_log shared
if ! file("h:\report\edi\edi_log.cdx")
	index on filename tag filename OF h:\report\edi\edi_log.cdx
	index on control_No tag control_no OF h:\report\edi\edi_log.cdx
	index on ponum tag ponum OF h:\report\edi\edi_log.cdx
	index on mom_order tag mom_order of h:\report\edi\edi_log.cdx
ENDIF
set order to mom_order

IF company.edi_id<>"          " 						&& we are expecting all of these companies to have edi data in the company profile
														&& this data is entered by the EDI programmer when we first set the client up for EDI trading
														&& and we need a unique control number at least 10 digits long 
	CD (m_dataloc)	
	select 31
	if file("edi_ucc128.dbf")			
		use edi_ucc128 shared						&& our location for our label data with the serialized shipment container SSCC Label 
	else
		use h:\report\edi\master_edi_ucc128 shared
		copy stru to edi_ucc128
		use edi_ucc128 exclusive
	endif
	if not file("edi_ucc128.cdx")
		index on ordernum tag ordernum of edi_ucc128.cdx
		index on alltrim(ordernum)+number tag orderitem of edi_ucc128.cdx
	endif
	set order to orderitem
	
	SELECT 2
	USE BOX SHARED
	SET ORDER TO ORDER
	SET FILTER TO LEN(ALLTRIM(TRACKINGNO))>0
	
	select 3 
	use carrier shared
	set order to ca_code
	
	
	select 50					&& each client will have its own table in the client directory to hold customer specific trading data
	if file("EDI_cms.dbf")
		use edi_cms shared		&& each trading partner will have its own record in this table 
	else
		use h:\report\edi\master_edi_cms shared
		copy stru to edi_cms 
		use EDI_cms
		append blank
		*messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("edi_CMS.cdx")
		index on order tag order OF edi_cms.cdx
		index on custnum tag custnum OF edi_cms.cdx
	endif
	set order to order

	select 4
	use cms shared
	set order to order
	set relation to shiplist into carrier 
	set relation to order into edi_cms additive
	
	select 6
	use h:\report\audit\adp_co shared
	set order to country

	select 7
	use cust shared  &&exclusive &&
	*set order to tag title of cust.cdx
	*index on title tag title of cust.cdx
	set order to custnum
	*set relation to altnum into edi_cust 
	set relation to country into adp_co additive
	
	select 8
	use stock shared
	set order to number
	
	select 9
	if file("REC_ID.dbf")
		use REC_ID shared		&& each trading partner will have its own record in this table 
	else
		use H:\report\edi_rec_id_master shared
		copy stru to REC_ID 
		use REC_ID
		append blank
		*messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("REC_ID.cdx")
		index on mom_itemid tag mom_itemid OF rec_id.cdx
	endif
	*use h:\mom\nap\rec_id.dbf shared					&&  costco sends to us the line item along with a record_id
	set order to mom_itemid 							&& 	we need to store this and match it back up with the item
														&&  sold when we send back the tacking number file.  
	select 10
	use items shared
	set order to inpart
	set relation to item into stock ADDITIVE
	set relation to item_id into rec_id additive
	set relation to alltriM(company.code)+alltrim(str(order,10,0))+inpart+item into edi_ucc128 additive
	
	select 11
	use invoice shared
	set relation to order into cms additive
	set relation to custnum into cust additive
	set relation to order into edi_log additive
	SET relation to str(order,10,0)+inpart into box additive
	set relation to str(order,10,0)+inpart into items additive
	
	N_Inv_run_count = 0					&& number of invocies sent
	*locate for cms.userid="CCO" and invoice.authcode="   "
	
	*locate for invoice.order=n_target_order and invoice.inpart=c_target_inpart
	N_target_order =  0
	c_Inpart = "A"
	N_cnts_shipped = ALLTRIM(edi_cms.cartons)  && not used but collected to make sure user sees difference between cnts shippped and the carton number of the total cartons shiping we are sending data on now
	N_usscc = ALLTRIM(edi_ucc128.sscc_18_h)
	c_target_item = ALLTRIM(edi_ucc128.number) 
	send_message=""
	n_ctn_num=1
	

	
	*locate for order=n_target_order and inpart=c_inpart
	*messagebox("pending some code to get a target order number")
	
	* locate for cms.cl_key="EDI_COSTC" AND INVOICE.AUTHCODE="  " and box.trackingno<>"               " and inpart="A" && 6-27-11 added below to deal with resets 
	
	SELECT invoice
	locate FOR (cms.cl_key="BBY" OR cms.cl_key="EDI_BBY") AND EMPTY(INVOICE.AUTHCODE) and !EMPTY(box.trackingno) and inpart="A" AND box.trackingno<>"CANCELLED" AND INV_DATE>CTOD("08/01/17") AND ACTIVE="A"
	DO WHILE NOT EOF()
		
		select 102
		use h:\report\edi\edi_vals shared
		GO TOP
		replace edi_vals.isa with alltrim(str(val(edi_vals.isa)+1))
		replace edi_vals.gs with alltrim(str(val(edi_vals.gs)+1))
		replace edi_vals.st with alltrim(str(val(edi_vals.st)+1))
		c_control_isa = edi_vals.isa
		c_control_gs = edi_vals.gs
		C_CONTROL_ST = EDI_VALS.ST
		n_target_order=invoice.order
		
		n_target_order=invoice.order		
		IF EMPTY(edi_cms.scac) OR EMPTY(edi_cms.shipweight) OR EMPTY(edi_cms.cu_ft) OR EMPTY(edi_cms.cartons)
			&& THIS ONE IS NOT READY TO GO 
			send_to = "ubt-orderadmin@rushorder.com" 
			send_from = "edi@rushorder.com" 
			send_subject = ALLTRIM(company.code)+ "  Load 856 Data Required"
			send_cc = "" 
			send_bcc = "twilliams@rushorder.com"
			send_message = send_message+ "We can not process a Best Buy 856 because missing data for Order #"+str(invoice.order,8,0)+CHR(13)+CHR(10)

				
		ELSE
			IF (!EMPTY(edi_cms.ref_fi) OR !EMPTY(edi_cms.trans_meth)) AND (ALLTRIM(edi_cms.ref_fi)<>"U" AND ALLTRIM(edi_cms.trans_meth)<>"U")
				SELECT edi_ucc128
				LOCATE FOR edi_ucc128.ordernum="UBT"+alltrim(STR(n_target_order,10,0))+c_inpart 
				l_gotit = .f.
				*DO WHILE edi_ucc128.ordernum="UBT"+alltrim(STR(n_target_order,10,0))+c_inpart  AND NOT EOF() AND NOT l_gotit  && 9-12-13 MOVED TO BELOW AND edi_ucc128.number=ALLTRIM(c_target_item)
					*browse
					* find the target order 
					** we are doing this here instead of the items loop because we want to match up the trackingnumber for a segment closer to the top of the record 
					*IF n_ctn_num = edi_ucc128.box_num AND edi_ucc128.number=ALLTRIM(c_target_item)
						** this is the box number for this order and we capture the usscc for the target box
						N_usscc =  ALLTRIM(edi_ucc128.sscc_18_b)
						N_total_box = edi_ucc128.total_box
						c_track = UPPER(edi_ucc128.track_no)   && 8-31-13 soon shawn will be getting these number at the time of shipping
						*?n_total_box
						l_gotit=.t.
						*MESSAGEBOX("gotit")
					*ELSE
						*skip
					*ENDIF
					*browse
					*SKIP
					
				*ENDDO
				
				
				
				SELECT invoice
				if edi_cms.order<>invoice.order AND invoice.ACTIVE="A" AND invoice.authcode="  "
					** 11-17-09 added when we don't have a link with edi_cms
					SELECT 103
					USE h:\Report\email\FILENUMBER SHARED

					GO TOP
					n_FILENUMBER = FILENUMBER.N_FILENUM && we store this value for now the send program increments it and we place the new value in at the end of this routine
			 
					send_to = "jchapman@rushorder.com" && This was for Testing
					send_from = "ediorders@rushorder.com" && This was for Testing
					send_subject = "EDI 856 File Error"
					send_cc = "pahlin@RUSHORDER.COM" && This was for Testing
					send_bcc = "jchapman@rushorder.com" && This was for Testing
					send_message = "We can not process an 856 because no edi_cms record for Order #"+str(invoice.order,8,0)

					do H:\Report\email_new\sendhtml		&&** put this back in if you wan to use this code
					select 103 
					replace n_filenum with n_FILENUMBER
					select invoice 
					replace invoice.authcode with "NO edi_cms"
					quit
					
				endif
				*browse fields order,inpart,inv_date,authcode,cms.odr_date,cms.cl_key,box.trackingno,CMS.SHIPLIST,BOX.SHIPLIST,cms.checknum
				*c_filename ="\\marvin\users\sterling\outbox\EDI"+dtos(date())+strtran(time(),":","")+".001"
				C_FILENAME="m:\sterling\outbox\"+alltrim(company.edi_id)+"\EDI_856_bby_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+".001"
				*c_filename ="\\ro-gilroy-cdi-1\users\sterling\outbox\EDI_"+dtos(date())+strtran(time(),":","")+".001"
				c_tempname = ALLTRIM(company.drive)+alltrim(company.dataloc)+"\lmm\edi_856_bby"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+".txt"
				*c_tempname ="H:\mom\nap\lmm\EDI"+dtos(date())+strtran(time(),":","")+".txt"

				* test mode runline = 'ISA*00*          *00*          *12*4088482282     *ZZ*COSTCOEWHSE    *'+substr(dtoc(date()),7,2)+substr(dtoc(date()),1,2)+substr(dtoc(date()),4,2)+'*'+substr(time(),1,2)+substr(time(),4,2)+'*U*00403*'+alltrim(edi_vals.isa)+'*0*T*>'
				
				* runline = 'ISA*00*          *00*          *12*4088482282     *ZZ*COSTCOEWHSE    *'+substr(dtoc(date()),7,2)+substr(dtoc(date()),1,2)+substr(dtoc(date()),4,2)+'*'+substr(time(),1,2)+substr(time(),4,2)+'*U*00403*'+alltrim(edi_vals.isa)+'*0*P*>'

				*unlinw = "ISA* 1*    2     * 3* 4        *5 *"+6                          +"*"+7             "*"+8                               +"*"+            9          "*"                              10     +"*11*12   *"+alltrim(c_control_num)+"*0*P*>"
				runline = "ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*"+edi_cms.qual+"*"+substr(edi_cms.interch_id,1,15)+"*"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*<*00403*"+alltrim(c_control_isa)+"*1*T*|"
				*messagebox("set up for test not production in the Interchange segment, and not set to send via FTP")
				* produciton mode runline = 'ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*"+edi_cms.qual+"*"+substr(edi_cms.interch_id,1,15)+"*"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*U*00401*"+alltrim(c_control_isa)+"*0*P*>'
				strtofile(runline+chr(13),c_tempname,.f.)					&& This line of code creates the txt version of the file
				
				if edi_cms.interch_id="0600603000003" or edi_cms.interch_id="600603ISTEST" && for best buy
					runline = "GS*SH*"+ALLTRIM(company.edi_id)+"*"+ALLTRIM(edi_cms.interch_id)+"*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(C_control_gs)+"*X*004030"  && BBB
				ELSE
					*MESSAGEBOX("are you sure this is a Best Buy order for WWI")
					runline = "GS*SH*"+alltrim(company.edi_id)+"*"+alltrim(edi_cms.interch_id)+"*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(c_control_gs)+"*X*004010"  && normal
					*runline = "GS*SH*4088482282*COSTCOEWHSE*20"+substr(dtoc(date()),7,2)+substr(dtoc(date()),1,2)+substr(dtoc(date()),4,2)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(edi_vals.gs)+"*X*004010"
				endif
				strtofile(runline+chr(13),c_tempname,.t.)
				
																			
				@11,10 SAY "Now processing customer record #" +str(cust.custnum,8,0)
				@12,10 say "                        order  #"+str(invoice.order,8,0)
				*if (cms.cl_key="EDI_BBY" AND INVOICE.AUTHCODE="  " and box.trackingno<>"               " and inpart="A" and not eof()) OR bby_test&& we have an record we have not previously sent, and either it is a customer record we have sent before or is eligible to set up 
				if (cms.cl_key="EDI_BBY"  AND INVOICE.AUTHCODE="  " and box.trackingno<>"               " and inpart=c_Inpart and not eof()) &&OR bby_test&& we have an record we have no
					N_Inv_run_count = N_Inv_run_count +1	
					runline = "ST*856*"+alltrim(C_control_st)
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count =1
					DO CASE
						CASE edi_cms.interch_id="0600603000003" && best Buy
							&& We are going to use the standard carton hierarchical applicaiton structure of shipment, order,item pack
							runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+"*"+DTOS(invoice.INV_DATE)+"*"+strtran(time(),":","")	
						CASE edi_cms.interch_id="4253138601CH" && COSTCO 
							&& We are going to use the standard carton hierarchical applicaiton structure of shipment, order,item pack
							runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0002"		&& Beginning Segment for Ship Not
						case edi_cms.interch_id="9086880888" or edi_cms.interch_id="9086886731"  && for bbb 
							runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"		&& Beginning Segment for Ship Notice 
						case (edi_cms.interch_id="BLUESTEM" or CMS.CL_KEY="EDI_BLUE") AND ALLTRIM(edi_cms.warehouse)="DS"
							runline =  "BSN*06*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"	&&
						case (edi_cms.interch_id="BLUESTEM" or CMS.CL_KEY="EDI_BLUE") AND ALLTRIM(edi_cms.warehouse)<>"DS"
							runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"	&&
						otherwise
							runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001*AS"	&& Beginning Segment for Ship Notice 
					ENDCASE
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
						****** ----- Order level detail --------********
						
					runline =  'HL*1**S'																					&& Hierarchical Level for the Order 
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
					
					n_ctns_in_shipmt = ALLTRIM(edi_cms.cartons)
					IF box.calc_weigh<1
						n_gross_weight = 20.00
					ELSE
						n_gross_weight = box.calc_weigh
					ENDIF
					n_VOLUME = ALLTRIM(edi_cms.cu_ft)
					
					
					
					IF edi_cms.interch_id="0600603000003" 
						runline =  'TD1*CTN25*'+alltrim(edi_cms.cartons)+'****G*'+alltrim(edi_cms.shipweight)+'*LB'	&& Carrier Details (quantity and weight of the entire shipment)
					ELSE
						runline =  'TD1*CTN25*****G*'+alltrim(edi_cms.cartons)+'*LB'		&& Carrier Details (quantity and weight of the entire shipment)
					ENDIF
					if edi_cms.interch_id="4253138601CH" and invoice.active<>"A"
						&& canceled order and we are excluding the td1 segment
					else
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
					ENDIF
					
					runline = "TD5**2*"+ALLTRIM(edi_cms.scac)+"*"+ALLTRIM(edi_cms.trans_meth)+"********CG"
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
						
					
					*MESSAGEBOX(" WE NEED A BOL") 
					IF edi_cms.interch_id="0600603000003"
						runline =  "REF*BM*"+alltriM(box.trackingno)						&& Reference Identifiction 
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
						
						runline =  "REF*CN*"+alltriM(box.trackingno)						&& Reference Identifiction 
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
					ENDIF
					
					d_shipdate =date()
					*@10,10 SAY "Enter the Date this order will ship (or has shipped) " get d_shipdate picture "@D"
					*read	
					
					if edi_cms.interch_id="4253138601CH" and invoice.active<>"A"
						&& canceled order and we are excluding the td1 segment	
					else
						runline =  "DTM*011*"+dtos(d_shipdate)+"*"+strtran(time(),":","")+"*PS"					&& Date time Refernce (ship date)
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
					endif
					
					IF edi_cms.interch_id="0600603000003"
						runline =  "N1*ST**92*"+Upper(ALLTRIM(EDI_CMS.STORE))
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
						
						runline =  "N1*SF*"+ALLTRIM(UPPER(company.name))+"*91*"+Upper(ALLTRIM(EDI_CMS.VENDOR))
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
						
						runline =  "N3*"+alltrim(upper(company.co_addr))			&& address Information ( ship from ) 92=vendor number  9 = dunns number
						strtofile(runline+chr(13),c_tempname,.t.)												&& 
						N_total_run_count = N_total_run_count + 1
					
						if UPPER(company.co_city)="GILROY"	
							runline =  "N4*GILROY*CA*95020*US"							&& geographic Information ( ship from ) 92=vendor number  9 = dunns number
						ELSE
							runline =  "N4*"+alltrim(upper(SUBSTR(company.co_city,1,AT(",",COMPANY.CO_CITY)-1)))+"*"+alltrim(upper(SUBSTR(company.co_city,AT(",",COMPANY.CO_CITY)+2,2)))+"*"+alltrim(upper(SUBSTR(company.co_city,AT(",",COMPANY.CO_CITY)+5,5)))
						ENDIF
						strtofile(runline+chr(13),c_tempname,.t.)												&& 
						N_total_run_count = N_total_run_count + 1
						
						if cms.shipnum<>cms.custnum	 and cms.shipnum<>0											&& we have a seperate ship to address for this order
							N_wasat = recno()
							select cust 																		&& get to the ship to address
							seek cms.shipnum
							if ! found()
								*Messagebox(" We can't locate the ship to address for order # "+str(cms.order,10,0))
								goto N_wasat
							endif
							select invoice
						ENDIF
						
					ENDIF
					
					

						
					runline =  'HL*2*1*O'																&& Hierichial Level for order 											&& Hierarchical Level for the Order 
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
					
					IF edi_cms.interch_id="0600603000003"
						*runline =  "PRF*"+UPPER(alltrim(substr(cms.checknum,1,2)))+"***"+dtos(cms.odr_date)	&& Purchase Order Reference
						runline =  "PRF*"+UPPER(alltrim(edi_cms.po_num))
						strtofile(runline+chr(13),c_tempname,.t.)											&& we dont know what IK is it is not DP = Department Number, IA= internal Vendor Number, IV = Sellers Invoice Number, VN = Vendor Order Number
						N_total_run_count = N_total_run_count + 1
						
					ELSE
						*runline =  "PRF*"+UPPER(alltrim(substr(cms.checknum,1,2)))+"***"+dtos(cms.odr_date)	&& Purchase Order Reference
						runline =  "REF*"+UPPER(alltrim(edi_cms.po_num))
						strtofile(runline+chr(13),c_tempname,.t.)											&& we dont know what IK is it is not DP = Department Number, IA= internal Vendor Number, IV = Sellers Invoice Number, VN = Vendor Order Number
						N_total_run_count = N_total_run_count + 1

						runline =  'TD1*CTN25*'+alltrim(str(n_ctns_in_shipmt,4,0))							&& Carrier Details (quantity and weight of the entire shipment)
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
					
						runline =  'REF*IA*'+alltrim(EDI_CMS.VENDOR)							&& Carrier Details (quantity and weight of the entire shipment)
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
						
						runline =  'REF*CO*'+alltrim(EDI_CMS.REF_CO)							&& Carrier Details (quantity and weight of the entire shipment)
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
						
						runline =  "REF*IK*"+ALLTRIm(STR(INVOICE.ORDER,10,0))+INVOICE.INPART							&& Carrier Details (quantity and weight of the entire shipment)
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
						
						runline =  "DTM*003*"+dtos(INVOICE.INV_DATE)					&& Date time Refernce (ship date)
						*runline =  "DTM*017*"+dtos(d_deldate)					&& Estimated Delivery (ship date)
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
					ENDIF
					
					
					
					
					n_hl = 2				&& varriable to keep track of the hierarchical Levels 
					
					select items
					n_SNL01 = 0 
					l_gotit = .f.
					****** ----- line level detail --------********
					do while items.order = invoice.order and items.inpart=invoice.inpart and not eof()
						n_snl01 = n_snl01 +1
						
						SELECT EDI_UCC128
						DO WHILE ALLTRIM(edi_ucc128.ordernum)=ALLTRIM(company.code)+alltrim(STR(n_target_order,10,0))+"A" AND ALLTRIM(items.item)=ALLTRIM(edi_ucc128.number) AND NOT EOF()
							do case
								case  ALLTRIM(edi_cms.interch_id)="0600603000003"  
									&& Best Buy 
									n_hl = n_hl +1			
									runline =  "HL*"+alltrim(str(n_hl,3,0))+"*2*P"	
									*runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_snl01-1),3,0))+"*P"		
									*runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_hl-1),3,0))+"*P"																	&& Hierarchical Level - for the Tare / pallet
									strtofile(runline+chr(13),c_tempname,.t.)
									N_total_run_count = N_total_run_count + 1
									
									runline =  "MAN*GM*"+alltrim(n_usscc)																	&& Hierarchical Level - for the Tare / pallet
									strtofile(runline+chr(13),c_tempname,.t.)
									N_total_run_count = N_total_run_count + 1
									
									&& walgreens seems to do a new h segment here Osh 
									n_hl = n_hl +1			
									runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_hl-1),3,0))+"*I"																	&& Hierarchical Level - for the Tare / pallet
									strtofile(runline+chr(13),c_tempname,.t.)
									N_total_run_count = N_total_run_count + 1
									
									runline =  "LIN**UP*"+ALLTRIM(STOCK.UPCCODE)		&& item identificaiton														&& Hierarchical Level
									strtofile(runline+chr(13),c_tempname,.t.)
									N_total_run_count = N_total_run_count + 1
								
									runline =  "SN1**"+ALLTRIM(STR(edi_ucc128.unitsinbox))+"*EA"			&& item identificaiton														&& Hierarchical Level
									strtofile(runline+chr(13),c_tempname,.t.)
									N_total_run_count = N_total_run_count + 1
							endcase		
						*	SKIP
						select items
						REPLACE ITEMS.CUSTOMINFO WITH ALLTRIM(ITEMS.CUSTOMINFO)+"EDI 856 File sent on :"+chr(10)+dtoc(date())+chr(10)+"EDI"+dtos(date())+strtran(time(),":","")+".001"
						select EDI_UCC128
						skip
						ENDDO
					SELECT items
					SKIP 	
					enddo
					
					select invoice
		
					IF edi_cms.interch_id="0600603000003"			
						runline =  "CTT*"+ALLTRIM(STR(N_HL,5,0))																	&& Transaction totals  - Number of Hierarchal sets 
						Strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
					ENDIF 
							
					runline =  "SE*"+alltrim(str((N_total_run_count+1),5,0))+"*"+alltrim(edi_vals.st)	
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
					
					runline ="GE*1*"+alltrim(edi_vals.gs)
					strtofile(runline+chr(13),c_tempname,.t.) 
				
					runline ="IEA*1*"+alltrim(edi_vals.isa)
					strtofile(runline+chr(13),c_tempname,.t.)   
				  
				  	*Messagebox("Please note the 856 document control number "+edi_vals.gs)
				  	*messagebox("please copy down this control number :"+alltrim(C_control_st))
					*modify command (c_tempname)
					*modify command (c_filename)
					rename (c_tempname) to (c_filename)
					?edi_ucc128.box_num
					?edi_ucc128.total_box
					select invoice && 11-16-09 added to make sure we are working even at end of file 
					*IF edi_ucc128.box_num = edi_ucc128.total_box 
					
					replace edi_log.asn_date with date() 
					replace edi_log.asn_file with sUBStr(c_filename,at("EDI_856",UPPER(C_FILENAME))
					
					
					&& Update Master Program Run Log
					select master_prg_run_log
					L_goodtogo = .t.
					*@17,10 say "Is this file good to send? " get L_goodtogo picture "@L"
					*READ
					
					*IF L_goodtogo
						*Messagebox("Please note the 856 document control number "+edi_vals.gs)
						DO h:\report\edi\ftp_Sterling2.prg	
						
						SELECT edi_cms
						replace edi_cms.sh_856 WITH edi_vals.gs
						replace edi_cms.sh_856_d WITH DATE()
						
						*IF N_ctn_num = n_total_box && 10-12-12 replace above line 
						** this is the last box we are sending for this shipment so lets mark the invoice as processed
							*@18,10 say "Is this the last Box for this shipment ?" get L_goodtogo picture "@L"
							*read
							IF L_goodtogo
								replace invoice.authcode with alltrim(edi_vals.gs)
							ENDIF
					n_ctn_num=n_ct
				ENDIF
			ELSE
				IF ALLTRIM(edi_cms.trans_meth)="U" OR ALLTRIM(edi_cms.ref_fi)="U"
					SELECT edi_ucc128
					LOCATE FOR edi_ucc128.ordernum="UBT"+alltrim(STR(n_target_order,10,0))+c_inpart AND EMPTY(edi_cms.gs_num)
					l_gotit = .f.
					*DO WHILE edi_ucc128.ordernum="UBT"+alltrim(STR(n_target_order,10,0))+c_inpart  AND NOT EOF() AND NOT l_gotit  && 9-12-13 MOVED TO BELOW AND edi_ucc128.number=ALLTRIM(c_target_item)
						*browse
						* find the target order 
						** we are doing this here instead of the items loop because we want to match up the trackingnumber for a segment closer to the top of the record 
							** this is the box number for this order and we capture the usscc for the target box
							n_ctn_num=edi_ucc128.box_num
							N_usscc =  edi_ucc128.sscc_18_b
							N_total_box = edi_ucc128.total_box
							c_track = UPPER(edi_ucc128.track_no)   && 8-31-13 soon shawn will be getting these number at the time of shipping
							*?n_total_box
							l_gotit=.t.
							*MESSAGEBOX("gotit")

						*browse
						*SKIP
						
					*ENDDO
					IF l_gotit 
						
						@10,20 say "USSCC : "+n_usscc

					endif
					SELECT invoice
					if edi_cms.order<>invoice.order AND invoice.ACTIVE="A" AND invoice.authcode="  "
						** 11-17-09 added when we don't have a link with edi_cms
						SELECT 103
						USE h:\Report\email\FILENUMBER SHARED

						GO TOP
						n_FILENUMBER = FILENUMBER.N_FILENUM && we store this value for now the send program increments it and we place the new value in at the end of this routine
				 
						send_to = "jchapman@rushorder.com" && This was for Testing
						send_from = "ediorders@rushorder.com" && This was for Testing
						send_subject = "EDI 856 File Error"
						send_cc = "pahlin@RUSHORDER.COM" && This was for Testing
						send_bcc = "jchapman@rushorder.com" && This was for Testing
						send_message = "We can not process an 856 because no edi_cms record for Order #"+str(invoice.order,8,0)

						do H:\Report\email_new\sendhtml		&&** put this back in if you wan to use this code
						select 103 
						replace n_filenum with n_FILENUMBER
						select invoice 
						replace invoice.authcode with "NO edi_cms"
						quit
						
					endif
					C_FILENAME="\\ro-gilroy-cdi-1\users\sterling\outbox\"+alltrim(company.edi_id)+"\EDI_856_bby_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+".001"
					c_tempname = ALLTRIM(company.drive)+alltrim(company.dataloc)+"\lmm\edi_856_bby"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+".txt"


					runline = "ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*"+edi_cms.qual+"*"+substr(edi_cms.interch_id,1,15)+"*"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*<*00403*"+alltrim(c_control_isa)+"*1*P*|"
					strtofile(runline+chr(13),c_tempname,.f.)					&& This line of code creates the txt version of the file
					
					if edi_cms.interch_id="0600603000003" or edi_cms.interch_id="600603ISTEST" && for best buy
						runline = "GS*SH*"+ALLTRIM(company.edi_id)+"*"+ALLTRIM(edi_cms.interch_id)+"*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(C_control_gs)+"*X*004030"  && BBB
					ELSE
						runline = "GS*SH*"+alltrim(company.edi_id)+"*"+alltrim(edi_cms.interch_id)+"*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(c_control_gs)+"*X*004010"  && normal
					endif
					strtofile(runline+chr(13),c_tempname,.t.)
					
																				
					@11,10 SAY "Now processing customer record #" +str(cust.custnum,8,0)
					@12,10 say "                        order  #"+str(invoice.order,8,0)
					*if (cms.cl_key="EDI_BBY" AND INVOICE.AUTHCODE="  " and box.trackingno<>"               " and inpart="A" and not eof()) OR bby_test&& we have an record we have not previously sent, and either it is a customer record we have sent before or is eligible to set up 
					if (cms.cl_key="EDI_BBY"  AND INVOICE.AUTHCODE="  " and box.trackingno<>"               " and inpart=c_Inpart and not eof()) &&OR bby_test&& we have an record we have no
						N_Inv_run_count = N_Inv_run_count +1	
						runline = "ST*856*"+alltrim(C_control_st)
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count =1
						DO CASE
							CASE edi_cms.interch_id="0600603000003" && best Buy
								&& We are going to use the standard carton hierarchical applicaiton structure of shipment, order,item pack
								runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+RIGHT(ALLTRIM(c_target_Item),2)+ALLTRIM(STR(n_ctn_num))+"*"+DTOS(invoice.INV_DATE)+"*"+strtran(time(),":","")	
							CASE edi_cms.interch_id="4253138601CH" && COSTCO 
								&& We are going to use the standard carton hierarchical applicaiton structure of shipment, order,item pack
								runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0002"		&& Beginning Segment for Ship Not
							case edi_cms.interch_id="9086880888" or edi_cms.interch_id="9086886731"  && for bbb 
								runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"		&& Beginning Segment for Ship Notice 
							case (edi_cms.interch_id="BLUESTEM" or CMS.CL_KEY="EDI_BLUE") AND ALLTRIM(edi_cms.warehouse)="DS"
								runline =  "BSN*06*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"	&&
							case (edi_cms.interch_id="BLUESTEM" or CMS.CL_KEY="EDI_BLUE") AND ALLTRIM(edi_cms.warehouse)<>"DS"
								runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001"	&&
							otherwise
								runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(INV_DATE)+"*"+strtran(time(),":","")+"*0001*AS"	&& Beginning Segment for Ship Notice 
						ENDCASE
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
							****** ----- Order level detail --------********
							
						runline =  'HL*1**S'																					&& Hierarchical Level for the Order 
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
						
						n_ctns_in_shipmt = 1
						IF box.calc_weigh<1
							n_gross_weight = 20.00
						ELSE
							n_gross_weight = box.calc_weigh
						ENDIF
						n_VOLUME = 1 * n_ctns_in_shipmt
						
						
						
						if  edi_cms.interch_id="0600603000003" 
							runline =  'TD1*CTN25*'+alltrim(str(n_ctns_in_shipmt,10,0))+'****G*'+alltrim(str(N_gross_weight,10,2))+'*LB'	&& Carrier Details (quantity and weight of the entire shipment)
						ELSE
							runline =  'TD1*CTN25*****G*'+alltrim(str(N_gross_weight,10,2))+'*LB'		&& Carrier Details (quantity and weight of the entire shipment)
						ENDIF
						if edi_cms.interch_id="4253138601CH" and invoice.active<>"A"
							&& canceled order and we are excluding the td1 segment
						else
							strtofile(runline+chr(13),c_tempname,.t.)	
							N_total_run_count = N_total_run_count + 1
						ENDIF
						
						runline = "TD5**2*"+ALLTRIM(EDI_CMS.SCAC)+"*U********CG"
						if edi_cms.interch_id="4253138601CH" and invoice.active<>"A"
							&& canceled order and we are excluding the td5 segmentENDIF
						else
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1		
						endif
						
						*MESSAGEBOX(" WE NEED A BOL") 
						IF edi_cms.interch_id="0600603000003"
							runline =  "REF*BM*"+alltriM(c_track)						&& Reference Identifiction 
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
							
							runline =  "REF*CN*"+alltriM(c_track)						&& Reference Identifiction 
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
						ENDIF
						
						d_shipdate =date()
						*@10,10 SAY "Enter the Date this order will ship (or has shipped) " get d_shipdate picture "@D"
						*read	
						
						if edi_cms.interch_id="4253138601CH" and invoice.active<>"A"
							&& canceled order and we are excluding the td1 segment	
						else
							runline =  "DTM*011*"+dtos(d_shipdate)+"*"+strtran(time(),":","")+"*PS"					&& Date time Refernce (ship date)
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
						endif
						
						IF edi_cms.interch_id="0600603000003"
							runline =  "N1*ST**92*"+Upper(ALLTRIM(EDI_CMS.STORE))
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
							
							runline =  "N1*SF*UBTECH ROBOTICS*91*"+Upper(ALLTRIM(EDI_CMS.VENDOR))
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
							
							runline =  "N3*"+alltrim(upper(company.co_addr))			&& address Information ( ship from ) 92=vendor number  9 = dunns number
							strtofile(runline+chr(13),c_tempname,.t.)												&& 
							N_total_run_count = N_total_run_count + 1
						
							if UPPER(company.co_city)="GILROY"	
								runline =  "N4*GILROY*CA*95020*US"							&& geographic Information ( ship from ) 92=vendor number  9 = dunns number
							ELSE
								runline =  "N4*"+alltrim(upper(SUBSTR(company.co_city,1,AT(",",COMPANY.CO_CITY)-1)))+"*"+alltrim(upper(SUBSTR(company.co_city,AT(",",COMPANY.CO_CITY)+2,2)))+"*"+alltrim(upper(SUBSTR(company.co_city,AT(",",COMPANY.CO_CITY)+5,5)))
							ENDIF
							strtofile(runline+chr(13),c_tempname,.t.)												&& 
							N_total_run_count = N_total_run_count + 1
							
							if cms.shipnum<>cms.custnum	 and cms.shipnum<>0											&& we have a seperate ship to address for this order
								N_wasat = recno()
								select cust 																		&& get to the ship to address
								seek cms.shipnum
								if ! found()
									Messagebox(" We can't locate the ship to address for order # "+str(cms.order,10,0))
									goto N_wasat
								endif
								select invoice
							ENDIF
						
						ENDIF
						
						

							
						runline =  'HL*2*1*O'																&& Hierichial Level for order 											&& Hierarchical Level for the Order 
						strtofile(runline+chr(13),c_tempname,.t.)	
						N_total_run_count = N_total_run_count + 1
						
						IF edi_cms.interch_id="0600603000003"
							*runline =  "PRF*"+UPPER(alltrim(substr(cms.checknum,1,2)))+"***"+dtos(cms.odr_date)	&& Purchase Order Reference
							runline =  "PRF*"+UPPER(alltrim(edi_cms.po_num))
							strtofile(runline+chr(13),c_tempname,.t.)											&& we dont know what IK is it is not DP = Department Number, IA= internal Vendor Number, IV = Sellers Invoice Number, VN = Vendor Order Number
							N_total_run_count = N_total_run_count + 1
							
						ELSE
							*runline =  "PRF*"+UPPER(alltrim(substr(cms.checknum,1,2)))+"***"+dtos(cms.odr_date)	&& Purchase Order Reference
							runline =  "REF*"+UPPER(alltrim(edi_cms.po_num))
							strtofile(runline+chr(13),c_tempname,.t.)											&& we dont know what IK is it is not DP = Department Number, IA= internal Vendor Number, IV = Sellers Invoice Number, VN = Vendor Order Number
							N_total_run_count = N_total_run_count + 1

							runline =  'TD1*CTN25*'+alltrim(str(n_ctns_in_shipmt,4,0))							&& Carrier Details (quantity and weight of the entire shipment)
							strtofile(runline+chr(13),c_tempname,.t.)	
							N_total_run_count = N_total_run_count + 1
						
							runline =  'REF*IA*'+alltrim(EDI_CMS.VENDOR)							&& Carrier Details (quantity and weight of the entire shipment)
							strtofile(runline+chr(13),c_tempname,.t.)	
							N_total_run_count = N_total_run_count + 1
							
							runline =  'REF*CO*'+alltrim(EDI_CMS.REF_CO)							&& Carrier Details (quantity and weight of the entire shipment)
							strtofile(runline+chr(13),c_tempname,.t.)	
							N_total_run_count = N_total_run_count + 1
							
							runline =  "REF*IK*"+ALLTRIm(STR(INVOICE.ORDER,10,0))+INVOICE.INPART							&& Carrier Details (quantity and weight of the entire shipment)
							strtofile(runline+chr(13),c_tempname,.t.)	
							N_total_run_count = N_total_run_count + 1
							
							runline =  "DTM*003*"+dtos(INVOICE.INV_DATE)					&& Date time Refernce (ship date)
							*runline =  "DTM*017*"+dtos(d_deldate)					&& Estimated Delivery (ship date)
							strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
						ENDIF
						
						

						
						
						n_hl = 2				&& varriable to keep track of the hierarchical Levels 
						
						select items
						n_SNL01 = 0 
						l_gotit = .f.
						****** ----- line level detail --------********
						do while items.order = invoice.order and items.inpart=invoice.inpart and not eof()
							n_snl01 = n_snl01 +1
							IF ALLTRIM(c_target_item)=ALLTRIM(items.item)
								
								do case
									case  edi_cms.interch_id="0600603000003"  
										&& Best Buy 
										n_hl = n_hl +1			
										runline =  "HL*"+alltrim(str(n_hl,3,0))+"*2*P"	
										*runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_snl01-1),3,0))+"*P"		
										*runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_hl-1),3,0))+"*P"																	&& Hierarchical Level - for the Tare / pallet
										strtofile(runline+chr(13),c_tempname,.t.)
										N_total_run_count = N_total_run_count + 1
										
										runline =  "MAN*GM*"+alltrim(n_usscc)																	&& Hierarchical Level - for the Tare / pallet
										strtofile(runline+chr(13),c_tempname,.t.)
										N_total_run_count = N_total_run_count + 1
										
										&& walgreens seems to do a new h segment here Osh 
										n_hl = n_hl +1			
										runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_hl-1),3,0))+"*I"																	&& Hierarchical Level - for the Tare / pallet
										strtofile(runline+chr(13),c_tempname,.t.)
										N_total_run_count = N_total_run_count + 1
										
										runline =  "LIN**UP*"+ALLTRIM(STOCK.UPCCODE)		&& item identificaiton														&& Hierarchical Level
										strtofile(runline+chr(13),c_tempname,.t.)
										N_total_run_count = N_total_run_count + 1
									
										runline =  "SN1**"+ALLTRIM(STR(edi_ucc128.unitsinbox))+"*EA"			&& item identificaiton														&& Hierarchical Level
										strtofile(runline+chr(13),c_tempname,.t.)
										N_total_run_count = N_total_run_count + 1
								endcase		
							*	SKIP
							ENDIF  &&ALLTRIM(c_target_item)=items.item
							select items
							REPLACE ITEMS.CUSTOMINFO WITH ALLTRIM(ITEMS.CUSTOMINFO)+"EDI 856 File sent on :"+chr(10)+dtoc(date())+chr(10)+"EDI"+dtos(date())+strtran(time(),":","")+".001"
							skip
						enddo
						
						select invoice

						IF edi_cms.interch_id="0600603000003"			
							runline =  "CTT*"+ALLTRIM(STR(N_HL,5,0))																	&& Transaction totals  - Number of Hierarchal sets 
							Strtofile(runline+chr(13),c_tempname,.t.)
							N_total_run_count = N_total_run_count + 1
						ENDIF 
								
						runline =  "SE*"+alltrim(str((N_total_run_count+1),5,0))+"*"+alltrim(edi_vals.st)	
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
						
						runline ="GE*"+alltrim(str(N_Inv_run_count,10,0))+"*"+alltrim(edi_vals.gs)
						strtofile(runline+chr(13),c_tempname,.t.) 
					
						runline ="IEA*1*"+alltrim(edi_vals.isa)
						strtofile(runline+chr(13),c_tempname,.t.)   
					  

						rename (c_tempname) to (c_filename)
						?edi_ucc128.box_num
						?edi_ucc128.total_box
						select invoice && 11-16-09 added to make sure we are working even at end of file 
						*IF edi_ucc128.box_num = edi_ucc128.total_box 
						
						replace edi_log.asn_date with date() 
						replace edi_log.asn_file with sUBStr(c_filename,at("EDI_856",UPPER(C_FILENAME)))


						
						&& Update Master Program Run Log
						select master_prg_run_log
						L_goodtogo = .t.

						
						IF L_goodtogo
							Messagebox("Please note the 856 document control number "+edi_vals.gs)
							DO h:\report\edi\ftp_Sterling2.prg	
							
							SELECT edi_cms
							replace edi_cms.sh_856 WITH edi_vals.gs
							replace edi_cms.sh_856_d WITH DATE()
							replace edi_ucc128.gs_num WITH edi_vals.gs
							IF N_ctn_num = n_total_box && 10-12-12 replace above line 
							** this is the last box we are sending for this shipment so lets mark the invoice as processed
								replace invoice.authcode with alltrim(edi_vals.gs)
							endif
							
						endif

					else
						replace master_prg_run_log.comments with "No orders with tracking to process"
						*messagebox("No orders with tracking numbers ready to send")
					endif  && WE FOUND OUR TARGET ORDER AND WE CONFIRMED IT HAS NOT BEEN PROCESSED BEFORE
					
					
				ENDIF 
			ENDIF 			
		endif 
	SELECT invoice
	continue	
	ENDDO 	
	IF !EMPTY(send_message)
		do H:\Report\email_new\sendhtml		&&** put this back in if you want to use this code
 	ENDIF 
ELSE
	*MESSAGEBOX("OUR SEQUENCE FILE IS CORRUPT OR THIS CLIENT IS NOT SET UP FOR EDI< THIS PROGRAM WILL TERMINATE")
	
ENDIF

STRTOFILE(TTOC(DATETIME())+' PROGRAM COMPLETE - '+c_program_name+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)	
