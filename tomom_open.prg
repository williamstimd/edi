* this routine simply opens up the tables 
*	(1) tomom			inbound data file
*	(2) inlog			import log with data and number of orders
*	(3)	ERR_LOG			individual order error log
*	(4)					available for future use
*	(5)					available for future use
*			*** below loaded by tomom_open.prg ***
*	(6)	cust			with index on subst(zipcode,1,5)+lastname 	
*	(7) cms
*	(8)	items
*			     |--->price  (9)
*	(11)stock----|
*	             |--->breakout (10)
*
*	(12)serial
* 	(13)g:\programs\country
*
*              |---->county (15)
*	(16)zip----|
*              |---->state (14)
*	(17) box
*	(18) sequence
*	(19) Zones
*	(20) carrier
*	(21) adcosts
*	(22) journal
*   (23) grx 
* 	(24) third_party 	7-10-06 added to support our new third party shipping system
*	(25) invoice 		7-15-07 added to supoort fufilled orders (tomom_invoice) 
*	(33) Telemark
*	(34) notes
*	(35) Contact
*	
*	PA modified to open contact
*	SA removed the "poofing" of third_party if not exist
*	PA 12/19/2012 Removed the not file statement (payflow.dbf) to try to keep the table from going POOF
* 	JC	2-12-14 added demgraph

&& CODE BELOW USED FOR ENCRYPTION OF CC 
SELECT 147
IF NOT USED("company")
	USE H:\MOM\PROFILE SHARED ALIAS COMPANY
ENDIF

*sql_server = "ro-gilroy-cdi-1"
*sql_server = "10.0.5.2"
sql_server = "ro-gilroy-sqldb"
sql_uid = "sa"
sql_password = "Lag0t0siS"
sql_database = "storage"


mysqlconnection="DRIVER=SQL SERVER;";
	+ "Description=Connection To Anything;";
	+ "SERVER="+sql_server+";";
	+ "UID="+sql_uid+";";
	+ "PWD="+sql_password+";";
	+ "APP=Microsoft Data Access Components;";
	+ "WSID=PAHLINP4-1500;";
	+ "DATABASE="+sql_database+";";
	+ "Network=DBMSSOCN;";
	+ "Address="+sql_server+",1433"

&& Open Connection
Store Sqlstringconnect(mysqlconnection) To C_SQL_STATE
SQLSetprop(C_SQL_STATE, 'asynchronous', .F.)
&&&--------------- END OF CODE TO OPEN ENCRYPTED CC TABLES ------------"

select 6
use cust shared
set order to tag iem_zip

select 7
use cms shared
set order to internetid && 11-08-06 added to trap duplicate orders from comming in to the system 


select 9
use price shared
set order to tag number 

select 10
use breakout shared
set order to tag number

SELECT 26 
USE INVENTOR SHARED
SET ORDER TO NUMBER

select 11
use stock shared
set order to tag number
set relation to number into price additive
set relation to number into inventor additive
set relation to number into breakout additive

select 8
use items shared
set relation to item into stock 	&& 1-21-09 we need this relation to set nontax flag in tomm_final

select 12
use serial shared
set order to tag stock

select 13
USE COUNTRY	SHARED						&& 12-14-09 client needs sales tax rates for canada		
count for ntaxr>0 to n_ntaxcount
if n_ntaxcount=0	
	use h:\report\fix\country shared  	&& this is the version of the country file with lots of 	
endif 									&& ways to spell each valid country 

select 14 
use state shared
set order to tag state

select 15 
use county shared
set order to tag county

select 16
use zip shared
set order to tag zip
set relation to country+state into state
set relation to country+state+county into county additive

select 17
use box shared

select 18
use sequence shared
set order to tag file

select 19
use zones shared
set order to tag table

select 20
use carrier shared
set order to tag ca_code
set relation to ca_z_table into zones

select 21
use adcosts shared
set order to tag adkey

select 22
use journal shared
set order to tag order

select 23
use gxr shared

*!*	if not file("third_party.dbf")
*!*		select 24
*!*		use h:\mom\template\third_party shared
*!*		copy stru to third_party
*!*	endif

select 24
use third_party shared

select 25 					&& 7-15-07 added to deal with fulfilled orders 
use invoice shared

select 26 
*!*	see above inventor opened and relation set into stock

SELECT 27 
USE INVTRANS shared

select 33
use telemark shared

select 34
use notes shared

select 35
use contact shared

SELECT 36
USE demgraph shared

*!*	if not file("payflow.dbf")
*!*		select 72							&& 7-2-08 Changed to select 72 not select 240 to be compatible with xlm_tomom
*!*		use h:\mom\ali\payflow shared
*!*		copy stru to payflow
*!*	ELSE
	select 72							&& 7-2-08 Changed to select 72 not select 240 to be compatible with xlm_tomom
	use payflow share
*!*	endif ** PA 12/19/2012 Removed the not file statement to try to keep the table from going POOF



