* this routine will log into MOM any details related to a import error or reasons an order is put on hold

*** NOTE THIS ROUTINE HAS BEEN REPLACED WITH tomom_err_ordmemo.prg THAT PUTS THE ORDER NOTES IN THE TABLE THAT 
*** IS LINLED WITH 4.3 'S ORDER***** 

select sequence
seek "CONTACT"
if ! found()
	select inlog
	replace problems with "No contact sequence"
	MESSAGEBOX("PROGRAM ABORTED NO CONTACT IN SEQUENCE FILE")
	quit
else
	try=0
	DONE=.F.
	n_CONTACT=0
	do while ! done .and. try<10		&& 10 trys to get a lock on the sequence file
		if rlock()
			replace next_val with next_val+1
			n_CONTACT=next_val
			done=.t.
		endif
		try=try+1
	enddo
    select TELEMARK
    append blank
    replace telemark.call_date with date()
    replace telemark.custnum with cms.custnum
    replace telemark.sales_id with "OLS"
    replace telemark.summary with alltrim(str(cms.order,10,0))+" - Order imported with the following error(s):"
    replace telemark.call_made with .t.
    replace telemark.time with time()
    replace telemark.who with rtrim(proper(cust.firstname))+" "+ proper(cust.lastname)
    replace telemark.extract with alltrim(str(cms.order,10,0))+" - Order imported with error(s):"
    replace telemark.state with cust.state
  	replace telemark.contact_id with N_contact
  	replace telemark.cl_key with cms.cl_key
  	
  	MEM_ERR=RTRIM(err_log.ERRORS)
    done=.f.
    do while ! done 
    	CUR_ERR=SUBSTR(MEM_ERR,1,1)
    	
        do case
                case CUR_ERR="1"
                    replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - No bill to address"
                case CUR_ERR="2"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - No bill to city"
                case CUR_ERR="3"			&& assigned in fix state
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Zip code does not match state"
                case CUR_ERR="4"			&& assigned in tomom_items
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Quanity on an item missing"
                case CUR_ERR="5"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - No order number on the order"
                case CUR_ERR="6"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Catalog code does not match adcode"
                case CUR_ERR="A"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Missing source_key"
                case cur_err="B"			&& assigned in fixstate
                	 replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid Province Abbreviation "
                case CUR_ERR="C"			&&fixstate
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - In valid country name"
                case CUR_ERR="c"			&& assigned in fix state
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Changed country to Canada"
                case CUR_ERR="D"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Check payment without check #"
                case CUR_ERR="d"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid payment method"
                case CUR_ERR="E"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Credit card number missing"
                case CUR_ERR="F"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid credit card type"
                case CUR_ERR="G"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid credit card number"
                case CUR_ERR="H"			&& assigned in tomom_cms
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Expired credit card"
                case CUR_ERR="I"			&& assigned in tomom_items
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Pricing exception on item"
                case CUR_ERR="J"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid product code"
                case CUR_ERR="K"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Shipping price exception"
                case CUR_ERR="L"			&& assigned in tomom_final
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Tax calculation error"
                case CUR_ERR="M"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - check order without check"
                case CUR_ERR="N"			&& assigned in adp_tomom_remedy
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - CRS required to review comment-"+alltrim(c.comment)
                    * 10-24-00 take out -  replace A->comment with F->COMMENT
                case CUR_ERR="O"			&& assigned in adp_tomom_remedy
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Invalid Ship method"				
                case CUR_ERR="P"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Purchase order on hold"
                case CUR_ERR="Q"
                     replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Test Credit Card Order"
                case cur_err="S"		&& assigned in fixstate
                	 replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - State not found in US & Canada State / Province "
                case cur_err="T"		&& assigned in tomom_custb
                	  replace telemark.summary with rtrim(telemark.summary) + chr(13)+chr(10)+" - Tax exempt customer on hold "
        endcase
        *messagebox("1 "+mem_err)
        MEM_ERR =substr(mem_err,2,10)
        if len(alltrim(mem_err))=0
        	done= .t.
        endif
    enddo
endif
  	