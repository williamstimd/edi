* tomom_cust3
* 3-26-98   deisigned to add a customer record for the ship customers
* 12-29-99 	modified piv_mom4 for windows use 
* 02-12-00 	need to use this program to wrap up the order with tax calculations based on ship to 
* 03-25-00 	added if statement before we put in the email address
*			added entry date 
* 10-3-00	added code to add shipto address even if the firstname is different
* 07-30-2002 JC mark found that we were writing over his ctype2 with default data, as a temp patch we 
*				changed tomom_custs to hold off on the write when we have data in the file 
* 09-12-2002 JC Mark requested the same change to ctype3 
* 04-15-2003 JC CMP found a flaw in the program when they sent through an order with a us bill to and a uK ship to address, we charged sales tax upon import
* 03-05-2005 JC found a flaw in the program re bill to address in a taxable state with county tax and the ship to address out side the US  
* 04-25-2006 JC added c_saltnum to import an alternate customer number for ship to locations
* 06-24-2015 JC added code to strip out commas from address lines 
* 10-12-2016 JC added routine to load c_comment ( that is sometimes initalized in fixphone ) 

c_saddress1 =  STRTRAN(c_saddress1,",","") && 6-24-15 added since are seeing this more and more from online sites 
c_saddress2 =  STRTRAN(c_saddress2,",","") && 6-24-15 added since are seeing this more and more from online sites 
*Messagebox("checking tomom_custs")
* below is a quick format of a zip code for a zip plus 4 that came without a hyphen 12-19-06

if len(alltrim(c_szipcode))=9 and at("-",c_szipcode)=0 and c_scountry="001" and ! isalpha(c_szipcode) 
	c_szipcode=substr(c_szipcode,1,5)+"-"+substr(c_szipcode,6,4)			
endif

DO CASE
	CASE INLIST(C_SSTATE , "NH", "PR", "VI") AND LEN(ALLTRIM(C_SZIPCODE))=3 AND ALLTRIM(c_scountry)="001"
		C_SZIPCODE = "00"+ALLTRIM(C_SZIPCODE)

	CASE INLIST(C_SSTATE, "MA","RI","NH","RI","ME","VT","CT", "NJ") AND LEN(ALLTRIM(C_SZIPCODE))=4 AND ALLTRIM(c_scountry)="001"
		C_SZIPCODE = "0"+ALLTRIM(C_SZIPCODE)
ENDCASE	
* check to see if we are setting up a ship to address

l_shipto=.f.
if alltrim(c_firstNAME)<>alltrim(c_sfirstNAME).and.c_sfirstname<>space(10)
    l_shipto=.t.	&& we have a seperate ship to address 
endif
if alltrim(c_lASTNAME)<>alltrim(c_SLASTNAME).and.c_slastname<>space(10)
    l_shipto=.t.	&& we have a seperate ship to address 
endif
if alltrim(c_COMPANY)<>alltrim(c_scompany).and.c_scompany<>space(10)
    l_shipto=.t.
endif
if alltrim(c_ADDRESS1)<>alltrim(c_saddress1).and. c_saddress1<>space(10)
   	l_shipto=.t.
endif
if alltrim(C_ADDRESS2)<>alltrim(C_saddress2).and. C_saddress2<>space(10)
    l_shipto=.t.
endif
if alltrim(c_CITY)<>alltrim(c_scity).and.C_scity<>space(10)
    l_shipto=.t.
ENDIF
*!*	IF ALLTRIM(c_zipcode)<>ALLTRIM(c_szipcode) AND C_scity<>space(10)
*!*		l_shipto=.t.
*!*	endif
if len(alltrim(c_email))>0 and len(alltrim(c_semail))>0 and alltrim(c_email)<>alltrim(c_semail)
	l_shipto=.t.
endif
if l_shipto
    * assign a new customer number
    *messagebox("we have a shipto")
    *messagebox("c_scountry="+c_scountry)
 	DONE=.F.
	select sequence
	seek "CUST"
	if ! found()
		select login
		replace problems with "No cust sequence"
		MESSAGEBOX("PROGRAM ABORTED NO Cust IN SEQUENCE FILE")
		quit
	else
		try=0
		do while ! done .and. try<10		&& 10 trys to get a lock on the sequecne file
			if rlock()
				replace next_val with next_val+1
				m_custnum=next_val
				done=.t.
			endif
			try=try+1
		enddo
	ENDIF
	IF ! DONE
		MESSAGEBOX("I can't get a lock on the sequence file")
	endif
	
    * check to see if we have a good country code 
	t_country=c_scountry			&& temporary varriable to pass to the fixcountry.prg 
	*m_country=space(3)				&& set a blank return varriable to rec from the fixcountry.prg
	m_country=" "					&& set a blank return varriable to rec from the fixcountry.prg
	* t_rcountry=" "  				4-21-15 changed to below && a temp varriable to pass the country name to the fixcountry.prg
	t_rcountry=c_srcountry  		&& a temp varriable to pass the country name to the fixcountry.prg
	
	do h:\report\fix\fixcountry		&& validate country and leave the country in position to assign tax
	c_scountry=m_country
	t_country=m_country				&& needed for fixstate
	*messagebox("after the fix country m_country"+M_country)
	*messagebox("after the fix country t_country"+T_country)
	*messagebox("after the fix country c_scountry"+c_scountry)
	* check to see if we have a good state code 
	T_state=c_sstate			&& temporary varriable to pass to the fixstate.prg 
	*m_state=space(2)			&& set a blank return varriable to rec from fixstate.prg
	m_state=c_sstate			&& set a blank return varriable to rec from fixstate.prg
	t_zip=c_szipcode
	
	DO h:\report\fix\FIXSTATE	&& fix state and leave the zipcode file in position to calculate tax
	C_sSTATE=M_STATE
	C_sCOUNTRY=M_COUNTRY 		&& RESET THIS VARRIABLE INCASE WE CHANGED IT IN THE FIX STATE PROGRAM
 	*messagebox("after the fix state c_scountry"+c_scountry)
    * over write the data in the cms file for a ship to address
    *09-23-02 moved the below greeting code to final because it is possible for some one to send a gift message to their own billing address and therefore should be moved to final
*!*	    if ! isnull(alltrim(c_greeting1))   
*!*	    	if Len(alltrim(c_greeting1))>0
*!*	    		select gxr
*!*	    		append blank
*!*	    		replace order with cms.order
*!*	    		replace bcustnum with cms.custnum
*!*	    		replace scustnum with m_custnum
*!*	    		replace odr_date with cms.odr_date
*!*	    		replace note1 with c_greeting1
*!*	    		replace note2 with c_greeting2
*!*	    		replace cms->shiptype with "G"
*!*	    	else 
*!*	    		replace cms->shiptype with "S"
*!*	    	endif
*!*	    else
*!*	    	replace cms->shiptype with "S"
*!*	    endif
	select cms						&& 10-10-04 added because we could be at the eof of the state file when we write to cms and if so we will miss the write
    replace cms->shiptype with "S"	&& 09-23-02 moved from loop above to its own line now that the code has been commented out 
    replace cms->ntaxrate with country->ntaxr
    do case	&& 04-15-2003 changed from individual if statements for us and canada to this case statement to deal with a Cal bill to and and UK ship to address
    	case m_country="001"
    		replace cms->staxrate with state->taxrate
    		replace cms->ctaxrate with county->ctaxr
    		replace cms->itaxrate with zip->itaxr
  
    	case m_country="034"							&& 5-15-01 had to split this out from the above becuse we don't look for a county in canada and therefore it is possible to inherit a rate from a previous order's county
			replace cms.ntaxrate with country.ntaxr
			replace cms.staxrate with state.taxrate
			replace cms.ctaxrate with 0					&& 3-5-05  this need to be hard coded because if the bill to address is a taxable state in the USA and the ship to is canada we need to over write the old code 
			replace cms.itaxrate with 0
			
		OTHERWISE
			replace cms.ntaxrate with country.ntaxr
			replace cms.staxrate with 0
			replace cms.ctaxrate with 0 
			replace cms.itaxrate with 0
	endcase
    replace cms->taxship with state->taxship
    replace cms->shipnum with m_custnum
 
    * add the shipto customer into data base
    select cust
    N_cust_rec = n_cust_rec +1 			&& keep in memory a count on new records added
	append blank
	replace cust->custnum with m_custnum
	
	replace cust->custtype with "S"
	replace cust->altnum with c_sALTNUM
	replace cust.title with proper(c_stitle) 
	replace cust->lastname with Proper(c_SLASTNAME)
	replace cust->FIRSTNAME with proper(c_SFIRSTNAMe)
	replace cust->COMPANY with proper(c_SCOMPANY)
	replace cust->email with c_email
	replace cust->ADDR with proper(c_SADDRESS1)
	replace cust->addr2 with proper(c_SADDRESS2)
	replace cust->city with proper(c_SCITY)
	replace cust->county with c_county		&& initilized from fixstate and we use c_county instead of c_scounty because we use this as a universal varriable, c_county set to " " at the top of this program
	replace cust->state with upper(c_SSTATE)
	replace cust->zipcode with c_SZIPCODE
	replace cust->country with c_scountry
	if cust.country<>"001" and cust.country<>"034" 
		replace cust.comment with "Itl Phone->"+c_sphone
	endif
	m_phone = c_sphone
	do h:\report\fix\fixphone			&& go standarize the phone number format
	replace cust->phone with P_phone

	m_phone = c_sphone2
	do h:\report\fix\fixphone			&& go standarize the phone number format to the mom format
	replace cust->phone2 with p_phone
	if at("@",c_semail)>0
		replace cust.email with c_semail
	ENDIF
	IF VARTYPE(c_comment)<>"U" AND c_COMMENT<>SPACE(40)				&& 10-12-16 this was in tomom_custb but not here, we create c_comment in fix phone when the phone number is too long
		do case 
			case cust.comment=space(40)
				replace cust.comment with c_comment
			case cust.comment<>space(40) .and. cust.comment2=space(40)
				replace cust.comment2 with cust.comment
				replace cust.comment with c_comment
			case len(alltrim(cust.comment))<len(alltrim(cust.comment2))
				replace cust.comment with alltrim(cust.comment)+c_comment
			otherwise
		    	replace cust->comment2 with alltrim(cust.comment2)+c_COMMENt
		   endcase
	ENDIF
	c_comment=""					&& 10-12-16 now that we put this in the record I am blanking it out to keep this from bleeding over to the next order, I can't be sure all calling program initialize this variable 
	
	replace cust.shiplist WITH c_scust_shiplist
	replace cust->ctype with upper(c_ctype)	
	replace cust->odr_date with d_ORDER_DATE
	replace cust->entrydate with date()
	replace cust->sales_id with upper(SALES_ID)
	replace cust->belongnum with n_billto
	if cust.ctype2 = " "
		REPLACE CUST->CTYPE2 WITH upper(SUBSTR(c_CTYPE2,1,2))
	endif
	if cust.ctype3 = "  "
		replace cust->ctype3 with UPPER(substr(C_CTYPE3,1,3))
	endif	
endif && shipto


