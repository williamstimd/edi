* piv_mom5.prg
* 3-28-98   UPDATES THE ITEMS FILE WITH EACH PRODUCT
* 4-09-98   changed the price->cl_key comparision with f->source key to field size match
* 4-13-98   changed the price look up routine to check all issues before finding the right price
* 12-30-99  changed the program to work with the new windows mom 
* 01-12-00	more needs to be done with this program for composits and breakouts.
*			needed is a varriable used to recycle this program if an item is a breakout or a composit
* 03-10-00 	updated the program to calculate cms.next_pay
* 03-19-00	updated for adaptec added the rec_id logic
* 04-01-00 	Adp needs to have po go on automatic back order
* 07-07-00  had to make changes to make breakouts work again 
* 05-15-02 	added the ability to use a character for the record id rather than just a numberic c_nowon vs n_nowon
* 05-28-02 	items with a discount on the line item are not checked against mom price and therefore should be marked as replace items->pricechang with .t.
* 06-03-02 jc	added code to trap in rec_id ad code by product
* 08-30-02 jc 	the system was not respecting the l_useprices flag. ( asp wanted us to use prices) to fix the problem I moved the 
*				replace it_unlist command to below the momprice function and after the reassignment of prices from the L_useprices check.
* 09-06-02 jc 	additional work on breakout items to support delayed shipping days and the same items repeating on a breakout
* 12-21-02 jc 	added code to deal with drop ship orders to make them equivalent to filled items 
* 06-05-03 jc 	repaired an oversight on breakout the see note next to code for this date 
* 09-03-03 JC 	made change to allow for differences in rounding of an item price without thowing an error
* 09-08-03 jc 	changed the way we keep the running tabulation for nontaxable merchandise in cms.vntm  
* 04-03-04 jc   adding value of merchandise by product tax class, the variables are initialized in tomom_cms.prg and the value will conversion to tax rates will happen in final  
* 05-06-04 jc	added unlock function to sequence to eliminate the freeze conditons
* 04-25-06 jc 	added items to the market point import for rec_id
* 12-10-06 JC 	added buyers part number
* 11-15-07 JC 	changed the use of N_price to add the following calculaiton including discount (n_price-(n_price*(n_discount/100)))
* 08-25-08 jC	needed to break the case statments for the national tax class and state tax class into two do case because they were not mutually exclusive and you could have one national class and a state ( one rule would fire but not both when it was in one do case statment ) 
* 07-09-10 JC	changed code on bottom to tally up accessories 
* 07-16-10 jc 	added code to deal with tax on product tax class by state 
* 08-25-12 jc 	added code to add the value of the items on hold into cms.vitemholds
* 08-19-13 JC 	change to deal with LSI Nytro and Sync products - need vp approval to ship
* 10-20-15 JC 	had to change the price comparison in if abs(N_price-fprice)> = 0.01 from if abs(N_price-fprice)> = 0.00 
* 12-09-15 JC 	exclude price checks on COUPON , DISCOUNT AND DONATION
* 12-10-15 PA 	addes exclusion to PLEDGE as well before recompile.
* 09-26-17 JC 	added flag to n_nontax to make this work for national tax as well as state tax 
*
*	   	c_item =a.product01
*       n_quant =a.quantity01
*       n_price=a.price01
*       n_discount=a.discount01
*       *n_nowon =a.refid1
* 		comeagain && set to the orig item was a breakout
*		fprice = the mom file price (including discount calculation) 
*		fdiscount= the mom file discount 
*		n_shipdays = number of days to await shipment
*		d_waitdate = date until we ship this item
*		
@20,10 say "loading the products ordered into the items file "
*messagebox("loading the products ordered into the items file ")
?c_item
needed=.t.
more=.f.
breakoutprice =.f. 				&& 5-11-10 added to not check prices if this price comes from a fixed price in breakout 
comeagain="none"
N_orignquant=n_quant			&& 1-25-07 added so we dont have to look at tomom.qty
	*messagebox("nowon var #2 -> "+str(n_nowon))
do while needed 
*!*		messagebox(c_item) 
*!*		if l_useprices
*!*			messagebox("use prices")
*!*		else 	
*!*			messagebox("dont use prices")
*!*		endif
*!*		messagebox("breakout price" +str(n_price,10,2))
*!*		messagebox(comeagain)
	select sequence	&& going to find the next id for items from the sequence file
	seek "ITEMS"
	if ! found()
		select login
		replace problems with "No ITEMS sequence"
		MESSAGEBOX("PROGRAM ABORTED NO ITEMS IN SEQUENCE FILE")
		quit
		
	else
		try=0
		DONE=.F.
		MitemId = 0 
		do while ! done .and. try<10		&& 10 trys to get a lock on the sequence file
			if rlock()
				replace next_val with next_val+1
				n_cMS=next_val
				MITEMID = SEQUENCE.NEXT_VAL && 5-15-01 added this to the system because it was not using the sequence number 
				done=.t.
				unlock
			endif
			try=try+1
		enddo
	endif	&& found the item in the sequence file
	
	select items
	n_item_rec = n_item_rec +1			&& keep running total of all the items added to the system
	GO BOTTOM
	*MITEMID=ITEMS.ITEM_ID	&& store the last item_id used in the items file (no order set on items when opened) removed 5-15-01 
	append blank
	replace order with cms->order
	REPLACE ITEMS.ITEM_ID WITH MITEMID 		&& 5-15-01 changed because it was not using the sequence number 
	replace item with upper(c_item)
	replace items->advert with upper(c_source_key)
*!*		replace items->It_unlist with n_price		&& 8-30-02 MOVED LOWER BECAUSE WE WERE NOT REPECTING THE L_USEPRICES FLAG
*!*		
*!*		if N_discount>0
*!*			replace items->discount with n_discount
*!*			replace items->pricechang with .t.
*!*		endif
	
	if n_nowon>0.and.comeagain="none"	&& for adaptec we need to keep in a seperate database all the pivital item ids
		select rec_id					&& this rule will file only if we have set the nowon varriable in the inital conversion program
		append blank
		replace order with cms.order
		replace item with upper(c_item)
		replace mom_itemid with items.item_id
		replace piv_rec_id with N_nowon
		if c_client="Adaptec"				&& 06-03-02 added to the code 
			replace rec_id.mktproject with orderlineitem.mktproject
		ENDIF
		if m_code="MPI"
			replace rec_id.unit_meas with c_unit_measure
			replace rec_id.prod_qual with c_prod_qual
		endif
		if m_code="NRI" AND c_source_key = "EDI_COSTC" 
			REPLACE REC_ID.COSTCO_NUM WITH c_IN_ITEM
		ENDIF
		select items
	endif
	if c_nowon<>" ".and.comeagain="none"	&& for adaptec we need to keep in a seperate database all the pivital item ids
		select rec_id						&& this rule will file only if we have set the nowon varriable in the inital conversion program
		append blank
		replace order with cms.order
		replace item with upper(c_item)
		replace mom_itemid with items.item_id
		replace rem_rec_id with c_nowon
		if c_client="Adaptec"			&& 06-03-02 added to the code to trap ad code by product 
			replace rec_id.mktproject with orderlineitem.mktproject
		ENDIF
		select items
	endif
	select stock
	seek upper(substr(c_item,1,20))
	if found()
		fprice=stock->price1
		fdiscount=0
	    select price
	    do while price->number=stock->number.and.!eof()
	        * this section looks for price changes in the system based on the
	        * mom pricing matrix.  Much more needs to be done here this is only
	        * a start looking at source codes only.
	        * Match# tells us if anyone of the parameters in the price file does
	        * not match with our order.  Hit tells us if any one is a hit
	        * then we ask if there are any hits with everything matching
	        match1 =.T.
	        match2 =.T.
	        match3 =.T.
	        match4 =.T.
	        match5 =.T.
	        match6 =.T.
	        match7 =.t.
	        match8 =.t.
	        match9 =.t.
	        hit =.f.
	        if price->cl_key<>space(9)
	            if price->cl_key=upper(substr(c_source_key,1,9))
	                hit =.t.
	            else
	                match1 =.f.
	            endif
	        endif
	        if price->qty>0
	            if price->qty<=n_quant
	                hit =.t.
	            else
	                match2 =.f.
	            endif
	        endif
	        if price->catcode<>space(2)
	            if price->catcode=upper(substr(c_catalog,1,2))
	                hit =.t.
	            else
	                match3 =.f.
	            endif
	        endif
	        if price->ctype<>space(1)
	            if price->ctype=upper(substr(c_ctype,1,1))
	                hit =.t.
	            else
	                match4 =.f.
	            endif
	        endif
	        if price->ctype2<>space(2)
	            *	this filed is converted from the piv_mom file to a mom standard
	            *	so we need to look at the cust file 
	            if price->ctype2=cust->ctype2
	                hit =.t.
	            else
	                match5 =.f.
	            endif
	        endif
	        if price->ctype3<>space(3)
	            if price->ctype3=cust.ctype3
	                hit =.t.
	            else
	                match6 =.f.
	            endif
	        endif
	        if price->date_start<>ctod("00/00/00")
	            if price->date_start<=date()
	                hit =.t.
	            else
	                match7 =.f.
	            endif
	        endif
	        if price->date_end<>ctod("00/00/00")
	            if price->date_end>=date()
	                hit =.t.
	            else
	                match8 =.f.
	            endif
	        endif
	        if price->custnum>0
	            if price->custnum=cust.custnum
	                hit =.t.
	            else
	                match9 =.f.
	            endif
	        endif
	        if hit.and.match1.and.match2.and.match3.and.match4.and.match5.and.match6.and.match7.and.match8.and.match9
	            if price->discount=0
	                fprice =price->price
	            else
	                fprice =stock->price1-(stock->price1*(price->discount/100))
	                fdiscount=price.discount
	            endif
	        endif
	        skip
	    enddo		&& cycle thru the price file for the target item

		SELECT items && 4-20-12 added to make sure we are dont try to do a write when we are at eof of the price file 
		
	    if l_useprices					&& we are instructed from the calling program to use the prices given to us by the calling program 
	    	if abs(N_price-fprice)>= 0.01 and cms.cl_key<>"CORP" AND CMS.CATCODE <>"XX" and not L_priceapproved and not breakoutprice AND NOT INLIST(c_item,'DISOUNT','COUPON','DONATION','PLEDGE') && 12-09-15 EXCLUDE FOR THESE ITEMS 7-6-07 Added l_priceapproved to deal with one off price excepitons 10-20-15 changed from below  
	    		** if abs(N_price-fprice)>= 0.01 and cms.cl_key<>"CORP" AND CMS.CATCODE <>"XX" and not L_priceapproved and not breakoutprice
	    		** if abs(N_price-fprice)> = 0.00 and cms.cl_key<>"CORP" AND CMS.CATCODE <>"XX" 	and not L_priceapproved and not breakoutprice 
	    		&& if abs(N_price-fprice)>=0.01 and cms.cl_key<>"CORP" AND CMS.CATCODE <>"XX"  && 4-17-07 added cat code  2-4-2004 added corp so we don't check prices on orders with this ad code 
	    		&& if N_price<>fprice 	&& 09-03-03 removed in favor of the above line of code that allows for rounding differences 
	        	c_error_code="I"
	        	do h:\report\tomom2\tomom_Log_errors
	        	select stock 
	        endif     
			&& decied to lock in all prices from web we whe are told to use prices 
*!*		    	if N_price<>fprice 			&& 09-03-03 make this change regardless if it is a rounding error because this 
*!*		    								&& may be a case where mom wants to make the price = 44.95 with a 35% discount = 29.2175 vs the price we get which is 29.22
*!*		    								&& by making this pricechang flag = true we insure mom will not auto recalculate the price to be 29.22 + 35% discount 
*!*		    		replace items->pricechang with .t.
*!*		    	endif
	    	
	    	replace items->pricechang with .t.
	    	
			
	    else							&& we are to use the MOM price instead of the price from the orig source
	    	if fdiscount>0				&& use the stock file price plus a discount
	    		n_discount=fdiscount
	    		n_price=stock->price1
	    	else 
	    		n_price=fprice			&& use the mom price (no discount involved) 
	    	endif
	    endif
	
	    Select items					&& 8-30-02 moved the following 6 lines from above to respect the l_useprices flag 
	    								&& above we have reset N_price based on the l_useprices flag now we need to fill in items 
	    								&& with the correct price.  if l_useprices we have the original data, if not we have the mom price
	    
	    replace items->It_unlist with n_price	&& 
	
		if N_discount>0
			replace items->discount with n_discount
			replace items->pricechang with .t.
		endif
		
	    select stock
		
		up_stock =.f.
	    do while .not. Up_stock
	    
	        if rlock()
	        	if UPPER(c_client)="ADAPTEC" or upper(c_client)="ROXIO" 
			       	if stock->units>=N_quant.and.cms->paymethod="CC".and.(cust->country="001".or.cust.country="034") 	&& 4-01-00 ADDED FOR ADAPTEC  (changed to include Canada 04-07-00)
		     			replace stock->commited with stock->commited+N_quant					&& To make sure purchase orders
		     																					&& foreign orders 
		                replace stock->units with stock->units-N_quant							&& are forced into backorder
		                select items															&& 
		                replace items->quanto with N_quant
		                replace items->quantf with N_quant
		                replace items->quantb with 0
		                replace items->item_state with "CM"
		                select cms
		                replace cms->nfill with cms->nfill+N_quant
		                * replace cms->ord_total with cms->ord_total+(N_price*n_quant)11-15-07 out in favor of the line below
		                replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
		                *replace cms->next_pay with cms.next_pay+(n_price*n_quant)		&& 3-20-00 moved to tomom_final.prg
		                replace cms->nall with cms->nall+N_quant
		       
		            else
		            	replace stock->bounits with stock->bounits+N_quant
		                select items 
		                replace items->quanto with N_quant
		                replace items->quantf with 0
		                replace items->quantb with N_quant
		                replace items->item_state with "BO"
		                select cms 
		                replace cms->nbor with cms->nbor+N_quant
		                *replace cms->ord_total with cms->ord_total+(N_price*N_quant)  11-15-07 out in favor of the line below
		                replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
		                * replace cms->vbor with cms->vbor+(N_price*N_quant)		&& this should include tax but since we are not to the 
		                															&& shipto record we will store how much of this is non taxable
		                replace cms->vbor with cms->vbor+((n_price-(n_price*(n_discount/100)))*N_quant)
		                	
						if stock.staxclass<> " "
							classcheck="tax_class"+alltrim(stock.staxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
							if stock.nontax or state.&classcheck	
								N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
							endif
						else
			            	if stock.nontax 
		                		* 11-07 out in favor of the line below N_nontax_bo = N_nontax_bo+(n_price*n_quant)
		                		N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
		                	endif
		                endif
		          
		                replace cms->nall with cms->nall+N_quant
		                if cms->multiship
		                	replace cms.order_st2 with "CA"
		                else
		                	replace cms->order_st2 with "BO"		&& the default is set when we populated the cms file 
		                endif										&& and since any item on the order will make this a bo
		                	 										&& we change this here (assuming it is not a multiship then it is a CA)
		               
		            endif
		        ELSE
		        	IF upper(c_client)="PRISMIQ" 
		        		IF TOMOM.FULFILLED<>"N"
		        			replace stock->commited with stock->commited+N_quant					&& To make sure purchase orders
		     																						&& foreign orders 
		                	replace stock->units with stock->units-N_quant							&& are forced into backorder
		                	select items															&& 
		                	replace items->quanto with N_quant
		                	replace items->quantf with N_quant
		                	replace items->quantb with 0
		               	 	replace items->item_state with "CM"
		                	select cms
		                	replace cms->nfill with cms->nfill+N_quant
		                	*11-15-07 out in favor of the line below
		                	* replace cms->ord_total with cms->ord_total+(N_price*n_quant)
		                	replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
		                	*replace cms->next_pay with cms.next_pay+(n_price*n_quant)		&& 3-20-00 moved to tomom_final.prg
		                	replace cms->nall with cms->nall+N_quant

		           		 else
			            	replace stock->bounits with stock->bounits+N_quant
			                select items 
			                replace items->quanto with N_quant
			                replace items->quantf with 0
			                replace items->quantb with N_quant
			                replace items->item_state with "BO"
			                select cms 
			                replace cms->nbor with cms->nbor+N_quant
			                * 11-15-07 out in favor of the lines below 
			                *replace cms->ord_total with cms->ord_total+(N_price*N_quant)
			                * replace cms->vbor with cms->vbor+(N_price*N_quant)		&& this should include tax but since we are not to the 
			                														&& shipto record we will store how much of this is non taxable
			                replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)

			                replace cms->vbor with cms->vbor+((n_price-(n_price*(n_discount/100)))*N_quant)	&& this should include tax but since we are not to the 
			                
			                if stock.staxclass<> " "
								classcheck="tax_class"+alltrim(stock.staxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
								if stock.nontax or state.&classcheck	
									N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
								endif
							else
			            		if stock.nontax 
		                			* 11-07 out in favor of the line below N_nontax_bo = N_nontax_bo+(n_price*n_quant)
		                			N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
		                		endif
		                	endif
		                	
			                replace cms->nall with cms->nall+N_quant
			                if cms->multiship
			                	replace cms.order_st2 with "CA"
			                else
			                	replace cms->order_st2 with "BO"		&& the default is set when we populated the cms file 
			                endif										&& and since any item on the order will make this a bo
			          	ENDIF	 										&& we change this here (assuming it is not a multiship then it is a CA)
			               
		            ELSE
		        	&& all of the other accounts other than Primic, roxio and adaptic 
		        	
			        	do case
			        		case stock.drop
			                	select items
			                	replace items->quanto with N_quant
			                	replace items->quantf with N_quant
			                	replace items->quantb with 0
			                	replace items->item_state with "ND"
			                	replace items.drop with .t.
			                	replace items.picked with .t.
			                	select cms
			                	replace cms->nfill with cms->nfill+N_quant
			                	replace cms.dfill with cms.dfill+N_quant
			                	*11-15-07  out in favor of the line below replace cms->ord_total with cms->ord_total+(N_price*n_quant)
			                	replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
			                	replace cms->nall with cms->nall+N_quant
			                	replace cms.dall with cms.dall+n_quant
			                	n_nontax_bod = 0						&& 4-4-04 since this program currently assums all drop ship products are in stock we don't need to keep track of the merchadise total the of nontax drop ship back ordera 
			    
			        		case stock->units>=N_quant					&& added the drop ship stuff 12/02
			        			&& out 12/02 if stock->units>=N_quant or stock.drop  	&& (why was this here ?).and.cms->paymethod="CC".and.cust->country="001"
			     				replace stock->commited with stock->commited+N_quant
			                	replace stock->units with stock->units-N_quant
			                	select items
			                	replace items->quanto with N_quant
			                	replace items->quantf with N_quant
			                	replace items->quantb with 0
			                	replace items->item_state with "CM"
			                	select cms
			                	replace cms->nfill with cms->nfill+N_quant
			                	*11-15-07 out in favor of the line below replace cms->ord_total with cms->ord_total+(N_price*n_quant)
			                	replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
			                	*replace cms->next_pay with cms.next_pay+(n_price*n_quant)		&& 3-20-00 moved to tomom_final.prg
			                	replace cms->nall with cms->nall+N_quant
			       			
							otherwise  && back ordered items 
			            		replace stock->bounits with stock->bounits+N_quant
			               	 	select items 
			                	replace items->quanto with N_quant
			                	replace items->quantf with 0
			                	replace items->quantb with N_quant
			                	replace items->item_state with "BO"
			                	select cms 
			                	replace cms->nbor with cms->nbor+N_quant
			                	* 11-15-07 out in favor of the lines below 
			                	* replace cms->ord_total with cms->ord_total+(N_price*N_quant)
			                	* replace cms->vbor with cms->vbor+(N_price*N_quant)	&& this should include tax but since we are not to the 
			                															&& shipto record we will store how much of this is non taxable
			                	replace cms->ord_total with cms->ord_total+((n_price-(n_price*(n_discount/100)))*N_quant)
			               
			                	replace cms->vbor with cms->vbor +((n_price-(n_price*(n_discount/100)))*N_quant)		&& this should include tax but since we are not to the 
			                																						&& shipto record we will store how much of this is non taxable
			                	if stock.staxclass<> " "
									classcheck="tax_class"+alltrim(stock.staxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
									if stock.nontax or state.&classcheck	
										N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
									endif
								else
			            			if stock.nontax 
		                				* 11-07 out in favor of the line below N_nontax_bo = N_nontax_bo+(n_price*n_quant)
		                				N_nontax_bo = N_nontax_bo+((n_price-(n_price*(n_discount/100)))*N_quant)
		                			endif
		                		endif
			                	
			                	replace cms->nall with cms->nall+N_quant
			                	if cms->multiship
			                		replace cms.order_st2 with "CA"
			                	else
			                		replace cms->order_st2 with "BO"		&& the default is set when we populated the cms file 
			                	endif										&& and since any item on the order will make this a bo
			                	 											&& we change this here (assuming it is not a multiship then it is a CA)
			            endcase	
					ENDIF
					unlock && 05-06-04 added 
				ENDIF
*!*09-08-03 move to lines below to insure we get this tablulation even if the item we are adding 
*!*		            N_value = N_value + (N_price*N_quant) 			&& keep a running total of the total merch 

				 do case	&& 08-25-08 this can not be a case statement
				*04-03-04 	jc - added initialization of varriables that will hold value of merchandise by product tax class
				*				 the value will be assigned in the tomom_items.prg and the conversion to tax rates will happen in final  
				*11-15-07   JC - in the lines below we changed the calculaiton from (N_price*n_quant) to ((n_price-(n_price*(n_discount/100)))*N_quant)
					case stock.ntaxclass = "A"
						n_ntaxclass_A = n_ntaxclass_A +((n_price-(n_price*(n_discount/100)))*N_quant)		&& running total of National class "A" merch for this order
						n_nbo_class_a = n_nbo_class_a +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "A" merch on back order for this order
						if stock.drop and items.quantb>0
							n_nbodclass_a = n_nbodclass_a +(n_price*items.quantb) 	&& running total of National class "A" drop ship merch on back order for this order
						endif 
									
					case stock.ntaxclass = "B"
						n_ntaxclass_B = n_ntaxclass_B +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of national class "B" merch for this order
						n_nbo_class_B = n_nbo_class_B +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of national class "B" merch on back order for this order
						if stock.drop and items.quantb>0
							n_nbodclass_b = n_nbodclass_b +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "B" drop ship merch on back order for this order
						endif  
					
					case stock.ntaxclass = "C"
						n_ntaxclass_C = n_ntaxclass_C +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of National class "C" merch for this order
						n_nbo_class_C = n_nbo_class_C +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "C" merch on back order for this order
						if stock.drop and items.quantb>0
							n_nbodclass_C = n_nbodclass_C +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "C" drop ship merch on back order for this order
						endif 

					case stock.ntaxclass = "D"
						n_ntaxclass_D = n_ntaxclass_D +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of national class "D" merch for this order
						n_nbo_class_D = n_nbo_class_D +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of national class "D" merch on back order for this order
						if stock.drop and items.quantb>0
							n_nbodclass_D = n_nbodclass_D +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "D" drop ship merch on back order for this order
						endif 
					
					case stock.ntaxclass = "E"
						n_ntaxclass_E = n_ntaxclass_E +((n_price-(n_price*(n_discount/100)))*N_quant)		&& running total of National class "E" merch for this order
						n_nbo_class_E = n_nbo_class_E +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "E" merch on back order for this order
						if stock.drop and items.quantb>0
							n_nbodclass_E = n_nbodclass_E +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "E" drop ship merch on back order for this order
						endif 
				endcase	
				do case
					case stock.staxclass = "A"
						n_staxclass_A = n_staxclass_A +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of State class "A" merch for this order
						n_sbo_class_a = n_sbo_class_a +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of State class "A" merch on back order for this order
						if stock.drop and items.quantb>0
							n_sbodclass_a = n_sbodclass_a +(n_price*items.quantb) 	&& running total of National class "A" drop ship merch on back order for this order
						endif 
					case upper(stock.staxclass) = "B"
						n_staxclass_B = n_staxclass_B +((n_price-(n_price*(n_discount/100)))*N_quant)		&& running total of State class "B" merch for this order
						n_sbo_class_B = n_sbo_class_B +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of State class "B" merch on back order for this order
						if stock.drop and items.quantb>0
							n_sbodclass_B = n_sbodclass_B +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "B" drop ship merch on back order for this order
						endif 									
					case stock.staxclass = "C"
						n_staxclass_C = n_staxclass_C +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of State class "C" merch for this order
						n_sbo_class_C = n_sbo_class_C +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of State class "C" merch on back order for this order
						if stock.drop and items.quantb>0
							n_sbodclass_C = n_sbodclass_C +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "C" drop ship merch on back order for this order
						endif 
					case stock.staxclass = "D"
						n_staxclass_D = n_staxclass_D +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of State class "D" merch for this order
						n_sbo_class_D = n_sbo_class_D +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of State class "D" merch on back or
						if stock.drop and items.quantb>0
							n_sbodclass_D = n_sbodclass_D +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "D" drop ship merch on back order for this order
						endif 
					case stock.staxclass = "E"
						n_staxclass_E = n_staxclass_E +((n_price-(n_price*(n_discount/100)))*N_quant) 		&& running total of State class "E" merch for this order
						n_sbo_class_E = n_sbo_class_E +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of State class "E" merch on back order for this order
						if stock.drop and items.quantb>0
							n_sbodclass_E = n_sbodclass_E +((n_price-(n_price*(n_discount/100)))*items.quantb) 	&& running total of National class "E" drop ship merch on back order for this order
						endif 
				endcase
	            up_stock =.t.
	        endif		&& if we get a lock on the stock file to update it with decrement of committed stock related to this order
	        if items->quanto=0
	        	c_error_code="4"
	            do H:\report\tomom2\tomom_Log_errors
	            select stock
	        endif
	    enddo			&& multiple try to get a lock on the stock file to decrement units available for sale 
	    select items 
	    replace items->it_uncost with stock->uncost
	    replace items->drop with stock->drop
	    replace items->OVERSIZED with stock->oversize
	    replace items->OVERSIZE2 with stock->oversize2
	    replace items->box with 1
	 						
		if stock.staxclass<> " "
			classcheck="tax_class"+alltrim(stock.staxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
			if stock.nontax or state.&classcheck	
				replace items.nontax with .t.
			else
	    		replace items.nontax with .f.
	   		endif
	   	else
	   		replace items.nontax with stock.nontax
		endif
		
		if stock.ntaxclass<> " "
			classcheck="tax_class"+alltrim(stock.ntaxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
			if stock.nontax or state.&classcheck	
				replace items.nontax with .t.
			else
	    		replace items.nontax with .f.
	   		endif
	   	else
	   		replace items.nontax with stock.nontax
		endif

	    replace items->nonproduct with stock->nonproduct
	    replace box->calc_weigh with box->calc_weigh + (n_quant*stock.unitweight)
	    if box->value+(n_quant*n_price)<99999
	   		replace box->value with box->value+(n_quant*n_price)
	   	else
	   		replace box->value with box->value+1
	   	endif 
		&& moved below with new calulation for value of the merchindise 
*!*		    if stock->nontax			&& revised 02-12-00 for the new field in cms.vntm (value of non tax merch)
*!*		    							&& later we need to look at mom related to tax ship Y/N
*!*		    	replace cms.vntm with cms.vntm+(n_quant*n_price)
*!*		    endif
	    	
	else	
		select items 
	    * here we have an item not in our inventory file
	    * no it_uncost will be added to this items file because no data exists
	    replace items->quanto with n_quant
	    replace items->quantb with N_quant
	    replace ITEMS->box with 1
	    replace it_unlist WITH n_price 		&& 12-09-15 added because it will save time when fixing an order we don't have a stock.number to match the order
	    replace cms->nbor with cms->nbor+N_quant
	    replace cms->vbor with cms->vbor + ((n_price-(n_price*(n_discount/100)))*N_quant)
	    replace cms->nall with cms->nall+N_quant   
	    c_error_code="i"		&& go log the error 
	    do h:\report\tomom2\tomom_Log_errors
	    select stock 
	    replace items->pricechang with .t.
	endif	&& find the target item in the stock file 
	
	if stock.serial
	&&MESSAGEBOX('Serialized: '+ALLTRIM(stock.number))
		ser_runnum = 0					&& we need to run this as many times as we have units 
		IF LEN(ALLTRIM(c_serial)) > 0
			importserial=.t.
			do h:\report\tomom2\tomom_serial
		ELSE
			importserial=.f.
			&& we are going assign one that presumably already in mom
			&& jc AND pA 12-3-08 modified to loop only when we are assigning not when we are adding a new serial as in above
			do while ser_runnum<n_quant
				do h:\report\tomom2\tomom_serial
				ser_runnum=ser_runnum+1
			enddo
		endif
	endif
	select items
	replace items->sales_id with upper(c_sales_id)
	replace items->catcode with upper(c_catalog)
	
	N_value = N_value + quanto*(it_unlist-(it_unlist*(discount/100))) 				&& 09-08-03 keep a running total of the total merch 
	
	if stock.staxclass<> " "
		classcheck="tax_class"+alltrim(stock.staxclass)		&& 07-16-10 a routine to look at stock class and the bill to ship taxclass ( this will be problem if the ship to address is a different state and the product class is billed differently ) 							
		if stock.nontax or state.&classcheck	
			*9-30-10 this is now done in final 
			*replace cms.vntm with cms.vntm + items.quanto*(items.it_unlist-(items.it_unlist*(items.discount/100)))	
		endif
	else
		if stock.nontax or upper(c_taxexempt)="YES"	
			* 05-09-11 added tax exempt customers to this list 
			* 11-07 out in favor of the line below N_nontax_bo = N_nontax_bo+(n_price*n_quant)
		    replace cms.vntm with cms.vntm + quanto*(it_unlist-(it_unlist*(discount/100)))	
		    replace items.nontax with .t.
		    replace items.n_nontax WITH .t. && 9-26-17 added for national tax 
		    
		endif
	endif
	
	if comeagain<>"none"	&& routine for entry of an component of a break out item 
		select breakout
		goto (n_recno)		&& get us back to the one we were working on, breakout has moved as a result of being linked with stock 
		select items
		if breakout.ship_days >0
			replace items.it_sdate with date()+breakout.ship_days 
			replace items.int_ext with .t.
			replace cms.vitemholds WITH cms.vitemholds + items.quanto*(items.it_unlist-(items.it_unlist*items.discount/100))
		endif
		if breakout.wait_date>date()
			replace items.it_sdate with breakout.wait_date
			replace cms.vitemholds WITH cms.vitemholds + items.quanto*(items.it_unlist-(items.it_unlist*items.discount/100))
		endif
		replace items.pricechang with l_useprices	&& tells mom this is a non standard price - changed to use prices because it is more reliable 
	endif
	if stock.break_out					&& we found an item that is a breakout item
		comeagain= upper(c_item)		&& this fires only on the orig item ( breakout subparts will not fire this rule)
		needed=.t.						&& we store the orig item so we know if the next item in the breakout
		*messagebox("rule fired for breakout item")
	else 								&& file also needs to entered on this order
		needed=.f.
	endif
	*messagebox(comeagain)
	if comeagain<>"none"
		*messagebox("rule2 fired for break out item comeagain="+comeagain)
		select breakout
		if alltrim(comeagain)+" "<>UPPER(alltrim(c_item))+" "				&& 8-10-07 ADDED UPPER JUST IN CASE WE DID NOT DO IT IN THE CLALING PROGRAM 7-17-00  we are not on the orig item, c_item is a break out item and has already been entered, and this rule will help us find the next items to enter
			* if alltrim(comeagain)<>alltrim(c_item)	6-04-03 code replaced with above, the system equated comeagain = 	2012500*2012300 = c_item of 2012500    
			*messagebox("rule3 fired for break out item")
			*locate for alltrim(breakout.prod)=alltrim(comeagain).and.alltrim(breakout.inv)=alltrim(c_item) && find the last item imported into mom 9-7-02 removed this code in favor of a record number seheem this one fell apart when the same item was repeated like in the case of monthly payments
			goto (n_recno) && this is the last item we have imported
			if not eof()
				skip
			endif																							&& and skip to the next record 
		endif																								&& this item should be either the next breakout item or eof()
		 
			** this is the first item in the list of 
		**if alltrim(breakout.prod)=alltrim(comeagain)														05-15-01 had to change to the statemant below because the system needs this double check to work with all trims statment MMBRL=MMBRL-DWN otherwise
		if alltrim(breakout.prod)=alltrim(comeagain) AND ALLTRIM(COMEAGAIN)=ALLTRIM(BREAKOUT.PROD) AND ! EOF()	&& the breakout record matches the stored value of the orig item
			*messagebox("the breakout.prod = "+breakout.prod + " and the comeagain is "+comeagain)
			N_recno = recno()
			c_item=breakout.inv							&& and we store the details of the item to be entered
			n_quant=breakout.q * N_orignquant			&& we are looking into the break out table and muliplying the quantity ordered times  
														&& the number of number of units that go in to the break out item. Once we have this 
														&& data we loop through this routine again to add the break out items on our MOM order
			*n_quant=breakout.q * tomom.qty				&& 1-26-07 removed in favor of the line above
			*messagebox("there is an assignment of the varriable")
			if breakout.s_price
				l_useprices=.t.
				n_price=breakout.price
				breakoutprice =.t.
			else
				l_useprices=.f.
				breakoutprice =.f.
			endif
			if breakout.s_disct
				n_discount=breakout.discount
			endif
			needed=.t.					&& we need to run this loop again
			skip						&& we skip to the next record to see if we need to run the next item
		else							&& as well (so when we loop again this rule will fire
			needed=.f.
		endif
	endif	
enddo && while not needed for breakouts and composite products 

&& CHECK TO SEE IF WE WANT TO INVOICE THIS ITEM/ORDER

if items.item="RETURN" or c_fulfilled="A"
	IF QUANTO=QUANTF
		C_FULFILLED="A"				&& TRIGGER TO MARK THE ORDER TO INVOCIE
		*?C_FULFILLED
		*WAIT
		select sequence	&& going to find the next id for items from the sequence file
		seek "INVTRANS"
		if ! found()
			select login
			replace problems with "No INVTRANS sequence"
			MESSAGEBOX("PROGRAM ABORTED NO ITEMS IN SEQUENCE FILE")
			quit
		
		else
			try=0
			DONE=.F.
			MTRANSId = 0 
			do while ! done .and. try<10		&& 10 trys to get a lock on the sequence file
				if rlock()
					replace next_val with next_val+1
					n_cMS=next_val
					MTRANSID = SEQUENCE.NEXT_VAL && 5-15-01 added this to the system because it was not using the sequence number 
					done=.t.
					unlock
				endif
				try=try+1
			enddo
		endif	&& found the item in the sequence file
		&& create invtrans record 
		SELECT INVTRANS
		APPEND BLANK
		REPLACE TRANS_ID WITH MTRANSID
		REPLACE NUMBER WITH STOCK.NUMBER
		REPLACE INVENT_ID WITH inventor.invent_id
		replace trans_date with date()
		replace transtype with "S"
		replace quantity with -items.quanto
		replace unit_cost with inventor.unit_cost
		replace total_amt with quantity*unit_cost
		replace userid with c_oper_id
		replace newinvtot with inventor.units-items.quanto
		replace notation with "Invoiced via import - order#"+str(items.order,8,0)
		&& REMOVE THE ITMEMS INVOICED FROM THE INVENTOR FILE 
		select inventor
		if items.item=inventor.number
			replace inventor.units with inventor.units-items.quanto
		else
			select login
			replace problems with "no sync between items and inventor"
			MESSAGEBOX("PROGRAM ABORTED NO ITEMS IN SEQUENCE FILE")
			quit
		endif
		&& REMOVE THE ITEMS INVOICED FROM STOCK FILE 
		select stock
		if items.item=stock.number
			replace stock.commited with stock.commited - items.quanto
		else
			select login
			replace problems with "no sync between items and inventor"
			MESSAGEBOX("PROGRAM ABORTED NO ITEMS IN SEQUENCE FILE")
			quit
		endif
		
		select items
		REPLACE INPART WITH "A"
		REPLACE QUANTF WITH 0
		REPLACE QUANTS WITH QUANTO
		REPLACE PICKED WITH .T.
		REPLACE IT_SDATE WITH DATE()
		REPLACE ITEM_STATE WITH "SH"
		REPLACE CUSTOMINFO WITH "Invoiced upon import"
		
		REPLACE CMS.NFILL WITH 0

	ELSE
		C_FULFILLED="B" && BACK ORDRED ITEMS AND CAN'T BE INVOICED
	ENDIF

ENDIF 
&& 07-18-10 ADDED FOR PRESTO 
IF c_client="PRE"
	do case	 
 		case stock.assoc="MAIN"
   			N_MAIN=N_MAIN + ITEMS.QUANTO
   		case stock.assoc="ACCEH"
   			** A heavy accessory on this order
   			N_MAIN=N_MAIN + ITEMS.QUANTO
   		CASE INLIST(C_ITEM,"HPC8766WN","HPC9363WN")
   			N_ACCESs = n_ACCESs + ITEMS.QUANTO
        		
   	endcase
ENDIF