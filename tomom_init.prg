* set up all the varriables we will need for this program
* 03-19-99 added comments and the n_nontax_bo varriable 
* 05-15-02 added c_nowon
* 11-09-02 added titel for adaptec import 
* 04-25-06 added new varriables for c_saltnum, c_unit_measu and c_prod_qual


set century on 
set safety off
IF VARTYPE(gctable)="U"
	public 	gcTable			&& when a file is open using getfile()
endif
public 	m_phone			&& used in program to standarize phone numbers to mom format
public 	p_phone			&& the return value from fixphone
public  m_state			&& used with fixstate
public	t_state			&& used with fixstate
public  t_zip			&& used with fixstate.prg
public 	t_country		&& temp varriable to be passed to the fixcountry.prg (presumably a mom country code)
public 	m_country		&& corrected country from the fixcountry program
public 	t_rcountry		&& temp varriable to be passed to the fixcountry.prg (presumably a upper case country name) 
public 	bill
public	N_error_to_log	&& used to keep track of errors in log_errors.prg
public 	c_error_code	&& used to pass errors to tomom_log_errors.prg 
public 	m_lastord		&& assigned in tomom_custb as the last order number for a customer and entered into cms.dbf in tomom_cust.prg
public 	m_custnum		&& system assigned final mom customer number
public	n_custnum		&& never used in import, added to program only as a compatible field with MOM IEM.dbf file ( might be used in the future if we want to import history in a add on product) 
public	C_altnum		&& added to the cust.dbf file as cust.altnum
public 	c_saltnum		&& added to teh cust.dbf rile as the cust.altnum for the ship to address
public  R_TAX			&& assigned in the tomom_cms.prg to keep a running total of the tax 
public  R_Taxm			&& assigned in the tomom_cms.prg to keep a running total of taxable merchandise
public 	R_notaxM		&& assigned in the tomom_cms.prg to keep a running total of non taxable merchandise ( varriable no longer used now we keep this in cms.vntm 
public 	c_title			&& 11-09-02 added for adaptec import 	
public	c_lastname
public	c_firstname
public	c_company
public	c_address1
public	c_address2
public	c_city
public	c_state
public 	c_county
public	c_country		&& expecting a valid country code used in fixcountry
public	c_rcountry		&& expecting a country name (fix country converts to upper case for search)
public	c_zipcode
public	c_phone
public	c_phone2
public	c_comment 
PUBLIC  c_comment2
public	c_ctype
public	c_ctype2
public	c_ctype3
public	c_taxexempt
public	c_prospect 		&& loaded into the cms and dbf files in the tomom_cust#.prg programs
public	c_cardtype		&& loaded into the cms and dbf files in the tomom_cust#.prg programs
public	c_cardnum		&& loaded into the cms and dbf files in the tomom_cust#.prg programs
public	c_expires		&& loaded into the cms and dbf files in the tomom_cust#.prg programs
public	c_approval		&& Credit Card Approval Number going to cms.approval
public	c_source_key 	&& GOES TO CMS CL_KEY
public	c_catalog		&& passed to cms
public	c_sales_id 		&& passed to cms.sales_id
public	c_oper_id 		&& passed to cms.userid
public	d_order_date	&& CMS FILE ODR_DATE
Public 	d_pay_date		&& stores the date a payment or credit card settlement
public	c_odr_num		&& passed to cms.alt_order
public	c_paymethod		&& passed to the cust.dbf and cms.dbf file in the tomom_cust#.prg programs)
public	c_greeting1
public	c_greeting2
public	n_promocred
public	l_shipto		&& the varriable that tells us if we have a unique ship to address (assigned in tomom_import.prg)
public	n_billto		&& the varriable that hold the customer number for the bill to address (assigned in tomom_import.prg)
public 	c_stitle
public	c_sfirstname	&& added to the shipto record in tomom_cust3
public 	c_slastname		&& added to the shipto record in tomom_cust3
public	c_scompany		&& added to the shipto record in tomom_cust3
public	c_saddress1		&& added to the shipto record in tomom_cust3
public	c_saddress2		&& added to the shipto record in tomom_cust3
public	c_scity			&& added to the shipto record in tomom_cust3
public 	c_sstate		&& added to the shipto record in tomom_cust3
public	c_szipcode		&& added to the shipto record in tomom_cust3
public 	c_scounty		&& added to the shipto record in tomom_cust3
public	c_scountry		&& added to the shipto record in tomom_cust3  expecting a valid country code used in fixcountry
public  c_srcountry		&& added to the shipto record in tomom_cust3  expecting a country name (fix country converts to upper case for search)
public	c_sphone		&& added to the shipto record in tomom_cust3
public	c_sphone2		&& added to the shipto record in tomom_cust3
public	c_semail		&& added to the shipto record in tomom_cust3
public	c_reference		&& customer po or bill to number 
public	c_sreference	&& ship to reference number if different than the bill to reference/ customer PO number
public	c_shipvia		&& must be valid mom ship codes 
public	n_paid			
public	N_product
public 	c_item
public	n_quant
public	d_holddate		
public	n_price
public 	n_nowon			&& stores the items line number if we need it ( if other than blank the system will create a datbase)
public  c_nowon			&& stores the items lien number if we need it (in character form) 
public	n_discount
public	n_shipping		&& to cms file shipping
public  n_tax			&& tax to cms file (the amount in this varriable will be imported, in tomom_final.prg we double check the amount and if it is out over .05 we put the order on hold 
public	c_email			&& the bill to email 
public	c_internetid	&& used to populate the cms.internetid field
public  l_useprices		&& tells import to if the price in the file should be used or use the mom price
public 	L_priceapproved && tells tomom_items not to bother checking the price (set up for a discount in ALI that varries and is not supported in MOM) 
public 	L_useshipamt	&& tells the import to use the shipping amount or not (not useed yet ) 
Public 	N_cust_rec		&& running total of the number of customer records (reported to inlog)
Public 	N_order_rec		&& running total of the number of orders imported (reported to the inlog)
Public 	N_item_rec		&& running total of the number of items imported (reported to the inlog) 
public  N_value			&& total value of merchandise on orders imported 
public  N_nontax_bo 	&& initialized as part of tomom_items to store the amount of each order that is non taxable an on back order
						&& then used in final to calculate the next_pay varriable 
public 	N_nontax_bod	&& value of drop ship back orders 
public  N_terms			&& set to 30 days here and used to populate cms.duedays and cust.duedays

IF VARTYPE(C_CLIENT)="U"
	public  c_client		&& set in main program (M_code and c_code are already set as public varriables in importok.mem which is called before this program 
	c_client ="   "
ENDIF

IF VARTYPE(c_client_code)="U"
	public  c_client_code	&& used in tomom.contact to identify the client_code 
	c_client = "Unknown"
ENDIF

PUBLIC  C_CARDAPP		&& credit card approval number if paid 
public 	unfinished		&& used to pass by strip_char.prg a string where we want to strip out all not numberic char
public 	finished 		&& used pased by strip_char.prg a string with all the non numberics striped out
public 	l_multiship		&& used to pass the option to split ship an order when one item is on back order
public 	C_NOTE1			&& to be used to fill in the notes.dbf with line 1 
PUBLIC 	c_note2			&& to be used to fill in the notes.dbf with line 2 
public 	c_note3			&& to be used to fill in the notes.dbf with line 3
public 	d_paid_date		&& holds the date an order is shown to have been paid 
public 	N_recno			&& used in Items for breakout items 
public 	m_contact		&& Used in Making a Contact Entry (contains the contact in sequence) 
public 	n_cms 			&& used in tomom.cms and tomom.contact to hold the order number

public  n_ntaxclass_a	&& keep track of non taxable merch by product class
public  n_nbo_class_a	&& keep track of backordered merch by product class
public  n_nbodclass_a	&& keep track of backordered drop ship merch by product class

public  n_staxclass_a	&& keep track of non taxable merch by product class
public  n_sbo_class_a	&& keep track of backordered merch by product class
public  n_sbodclass_a	&& keep track of backordered drop ship merch by product class

public  n_ntaxclass_b	&& keep track of non taxable merch by product class
public  n_nbo_class_b	&& keep track of backordered merch by product class
public  n_nbodclass_b	&& keep track of backordered drop ship merch by product class

public  n_staxclass_b	&& keep track of non taxable merch by product class
public  n_sbo_class_b	&& keep track of backordered merch by product class
public  n_sbodclass_b	&& keep track of backordered drop ship merch by product class

public  n_ntaxclass_c	&& keep track of non taxable merch by product class
public  n_nbo_class_c	&& keep track of backordered merch by product class
public  n_nbodclass_c	&& keep track of backordered drop ship merch by product class

public  n_staxclass_c	&& keep track of non taxable merch by product class
public  n_sbo_class_c	&& keep track of backordered merch by product class
public  n_sbodclass_c	&& keep track of backordered drop ship merch by product class

public  n_ntaxclass_d	&& keep track of non taxable merch by product class
public  n_nbo_class_d	&& keep track of backordered merch by product class
public  n_nbodclass_d	&& keep track of backordered drop ship merch by product class

public  n_staxclass_d	&& keep track of non taxable merch by product class
public  n_sbo_class_d	&& keep track of backordered merch by product class
public  n_sbodclass_d	&& keep track of backordered drop ship merch by product class

public  n_ntaxclass_e	&& keep track of non taxable merch by product class
public  n_nbo_class_e	&& keep track of backordered merch by product class
public  n_nbodclass_e	&& keep track of backordered drop ship merch by product class

public  n_staxclass_e	&& keep track of non taxable merch by product class
public  n_sbo_class_e	&& keep track of backordered merch by product class
public  n_sbodclass_e	&& keep track of backordered drop ship merch by product class
public 	tsid_cdkey		&& Note for storing TSID and CDKEY for Roxio
public 	auth_net_msg	&& Note for storing Authorize.Net Error Message for Roxio
public	importserial	&& If you have a serial number online that you want to add to serial, set this to T in orders loop
public 	c_serial		&& Serial Number to be imported
public  target_ord		&& target order number 
Public  c_thirdparty	&& Third party shipper number	 04-25-06
Public 	c_unit_measure  && to be used with market point
Public  c_prod_qual 	&& to be used wIth rec_id on Market point
public 	n_page			&& used with market point's packing skip program 
public  c_shipvia_orig	&& used with Market Point's packing slip program 
public  c_blind_shipment&& to pass shipment types to the packing slip program for Market Point
public 	C_line_desc1	&& to pass product line descriptions to the packing slip program for Market Point 
public 	C_line_desc2	&& to pass product line descriptions to the packing slip program for Market Point 
public 	c_location_code && to pass location code to the packing slip program for Market Point 
public  c_item_NO		&& to pass item sku (without location code appended)  to the packing slip program for Market Point 
Public 	N_qty_order		&& to pass item quantity ordered to the packing slip program for Market Point 
public 	n_qty_ship		&& to pass item quantity shipped to the packing slip program for Market Point 
public  l_signature		&& to tell cms.notyetused that this a package that needs a cusotmer signature
PUBLIC  c_IN_ITEM		&& Buyer's item number
PUBLIC  C_FULFILLED		&& TRIGGER TO TELL US TO INVOICE  7-15-07
public 	c_ordernote1	&& to be used with the mom 5.0 IEM layout
public 	c_ordernote2	&& to be used with the mom 5.0 IEM layout
public 	c_ordernote3	&& to be used with the mom 5.0 IEM layout
public 	c_ordernote4	&& to be used with the mom 5.0 IEM layout
public 	c_ordernote5	&& to be used with the mom 5.0 IEM layout
public 	c_special_instruction	&& to be used with error code R 
public 	c_ship_instructions	&& to be used with error code r 
public 	c_error_mess	&& error message to go into telemark
public 	c_bt_custnum	&& mom ship to customer number provided in the inbound import file
public  c_st_custnum 	&& mom ship to customer number provided in the inbound import file 
PUBLIC  c_custom_info	&& adds data to items.custominfo


PUBLIC pos0				&& used in conjunction with EDI imports 
PUBLIC pos1
PUBLIC pos2
PUBLIC pos3
PUBLIC pos4
PUBLIC pos5
PUBLIC pos6
PUBLIC pos7
PUBLIC pos8
PUBLIC pos9
PUBLIC pos10
PUBLIC pos11
PUBLIC pos12
PUBLIC pos13
PUBLIC pos14
PUBLIC pos15
PUBLIC pos16
PUBLIC pos17
PUBLIC pos18
PUBLIC pos19
PUBLIC pos20
public pos21 
PUBLIC pos22
PUBLIC pos23
PUBLIC pos24
PUBLIC pos25
PUBLIC FRAUD_ID && THIS IS THE FRAUD ID FROM TOMOM
PUBLIC FRAUD_MESSAGE && THIS IS THE FRAUD MESSAGE FROM TOMOM

IF VARTYPE(send_to)="U"		&&08-25/16 added 
	public send_to			&& These variables are for sending email errors
	public send_from
	public send_subject
	public send_cc
	public send_bcc
	public send_message
	PUBLIC send_attachment
ENDIF

public c_rma_number

PUBLIC encrypted
PUBLIC decrypted
PUBLIC C_TAR_cardnum
PUBLIC C_sql_STATE
IF VARTYPE(c_program_name)="U"
	public C_program_name
	c_program_name="TOMOM Program"
endif
IF VARTYPE(C_USERNAME)="U"
	public c_username
	public c_machine
	public c_currentpath
	public c_executable
	c_username = alltrim(substr(sys(0),at("#",sys(0))+1,len(sys(0))))  	&& capture the user name
	c_machine = alltrim(substr(sys(0),1,at("#",sys(0))-1))				&& capture the workstation 
	c_currentpath = sys(5)+sys(2003)									&& capture the current path 
	c_executable = SYS(16)	
ENDIF

PUBLIC c_momuser
public n_tar_order		&& 04-20-09 added to use with encryption / decryption 
public l_encrypt		&& used to determine if we are going to encrypt credit cards
public c_password
public l_skip_mod10		&& a flag used in tomom_cms to tell us if we want to do a mod10 skip
public N_main			&& used to count the number of base units on a wellcore order to see if we need to do a split ship ( also used with Presto) 
public n_subscrip		&& used to count the number of welcore subscriptions on the order  
PUBLIC n_ACCESS			&& USED TO COUNT THE NUMBER OF ACCESSORIES ON AN ORDER STOCK.ASSOC="ACCES" FOR PRESTO AND ANY OTHER CLIENT
**PA - 11/15/2010	To listen to the custid.optin
PUBLIC c_cust_shiplist
PUBLIC c_scust_shiplist

l_encrypt = .f.			&& default so tomom_cms does not try to encrypt orders 
c_momuser="OLS"

m_phone=space(10)
p_phone=space(10)
m_state=space(10)
t_state=space(10)
t_zip=space(10)
t_country=space(10)
t_rcountry = " "
m_country=space(10)
N_error_to_log=0
c_error_code=space(10)
n_custnum=0
C_altnum=space(10)	
c_title = space(3) 	
c_lastname=space(10)
c_firstname=space(10)
c_company=space(10)
c_address1=space(10)
c_address2=space(10)
c_city=space(10)
c_state=space(10)
c_county=space(10)
c_country=space(10)		
c_rcountry=space(10)
c_zipcode=space(10)
c_phone=space(10)
c_phone2=space(10)
c_comment=space(10)
c_comment2=space(10)
c_ctype=space(1)
c_ctype2=space(2)
c_ctype3=space(4)
c_taxexempt=space(1)
c_prospect=space(1)
c_cardtype=space(2)
c_cardnum=space(10)
c_approval=space(10)
c_expires=space(5)
c_source_key=space(10) 
c_catalog=space(2)
c_sales_id=space(2)
c_oper_id=space(2)
d_order_date=date()
d_pay_date=date()
c_odr_num=space(10)
c_paymethod=space(2)
c_greeting1=space(10)
c_greeting2=space(10)
n_promocred=0
l_shipto=.f.
c_saltnum = space(10)
c_stitle = space(3) 
c_sfirstname=space(10)
c_slastname=space(10)
c_scompany=space(10)
c_saddress1=space(10)
c_saddress2=space(10)
c_scity=space(10)
c_sstate=space(2)
c_szipcode=space(10)
c_scounty=space(3)
c_scountry=space(3)
c_sphone=space(10)
c_sphone2=space(14)
c_semail=space(10)
c_reference=space(10)
c_sreference=space(10)
c_shipvia=space(10)
n_paid=0
c_item=" "				&& 10-26-06 change to initilize to " " instead of 0 
n_quant=0
d_holddate=date()
n_price=0
N_nowon=0				&& sets item record id if we need it (if other than 0 the system will add or create a database)
c_nowon="   "
n_discount=0
n_shipping=0
n_tax=0
c_email=space(10)
c_internetid=space(10)
l_useprices=.t.			&& has to be set to true 3-19-00 for now no other option is available
L_priceapproved =.f.	&& set up 7-6-07 to deal with a discount amount not supported in MOM 
l_useshipamt=.t.		&& has to be set to true 3-19-00 for now no other option is available
N_cust_rec= 0
N_order_rec=0
n_item_rec=0
N_value=0
N_nontax_bo =0 
N_nontax_bod=0 
N_terms = 30
C_CARDAPP = "        "
l_multiship = .f.
C_NOTE1 = " "		 
c_note2	= " "		 
c_note3	= " "
d_paid_date = date()
n_ntaxclass_A = 0 	&& Initialize the running total of National class "A" merch for an order
n_nbo_class_A = 0 	&& Initialize the running total of National class "A" merch on back order for an order
n_nbodclass_A = 0 	&& Initialize the running total of National class "A" drop ship merch on back order for an order

n_staxclass_A = 0 	&& Initialize the running total of State class "A" merch for an order
n_Sbo_class_A = 0 	&& Initialize the running total of state class "A" merch on back order for an order
n_sbodclass_A = 0 	&& Initialize the running total of State class "A" drop ship merch on back order for an order

n_ntaxclass_B = 0 	&& Initialize the running total of National class "B" merch for an order
n_nbo_class_B = 0 	&& Initialize the running total of National class "B" merch on back order for an order
n_nbodclass_B = 0 	&& Initialize the running total of National class "B" drop ship merch on back order for an order

n_staxclass_B = 0 	&& Initialize the running total of State class "B" merch for an order
n_Sbo_class_B = 0 	&& Initialize the running total of state class "B" merch on back order for an order
n_Sbodclass_B = 0 	&& Initialize the running total of state class "B" drop ship merch on back order for an order

n_ntaxclass_C = 0 	&& Initialize the running total of National class "C" merch for an order
n_nbo_class_C = 0 	&& Initialize the running total of National class "C" merch on back order for an order
n_nbodclass_C = 0 	&& Initialize the running total of National class "C" drop ship merch on back order for an order

n_staxclass_C = 0 	&& Initialize the running total of State class "C" merch for an order
n_Sbo_class_C = 0 	&& Initialize the running total of state class "C" merch on back order for an order
n_Sbodclass_C = 0 	&& Initialize the running total of state class "C" drop ship merch on back order for an order

n_ntaxclass_D = 0 	&& Initialize the running total of National class "D" merch for an order
n_nbo_class_D = 0 	&& Initialize the running total of National class "D" merch on back order for an order
n_nbodclass_D = 0 	&& Initialize the running total of National class "D" drop ship merch on back order for an order

n_staxclass_D = 0 	&& Initialize the running total of State class "d" merch for an order
n_Sbo_class_D = 0 	&& Initialize the running total of state class "D" merch on back order for an order
n_Sbodclass_D = 0 	&& Initialize the running total of state class "D" drop ship merch on back order for an order

n_ntaxclass_E = 0 	&& Initialize the running total of National class "E" merch for an order
n_nbo_class_E = 0 	&& Initialize the running total of National class "E" merch on back order for an order
n_nbodclass_E = 0 	&& Initialize the running total of National class "E" drop ship merch on back order for an order


n_staxclass_E = 0 	&& Initialize the running total of State class "E" merch for an order
n_Sbo_class_E = 0 	&& Initialize the running total of state class "E" merch on back order for an order
n_Sbodclass_E = 0 	&& Initialize the running total of state class "E" drop ship merch on back order for an order

tsid_cdkey = ""		&& Initialize the Roxio TSID and CDKEY Data
auth_net_msg = "" 	&& Initialize Authorize.Net Error Message
importserial = .f.	&& Initialize checkbit to see if we need to import serial
c_serial = "" 		&& Initialize c_serial if serial number is available
c_thirdparty =" " 	&& initialize thrird party billing number to blank
target_ord = "0"
c_unit_measure = " "
c_prod_qual = " "
FRAUD_ID = 0 && NO FRAUD IF SET TO "0"
FRAUD_MESSAGE = " " && REASON FOR FRAUD FLAG
c_rma_number = "  " && holds the rma number that we get for an 940 edi from Market Point
l_signature	=.f.	&& tells cms.notyetused 
c_IN_ITEM = " "		&& buyers item number
C_FULFILLED =" "	&& TRIGGER TO TELL US TO INVOICE
c_ordernote1=" "	&& to be used with the mom 5.0 IEM layout
c_ordernote2=" "	&& to be used with the mom 5.0 IEM layout
c_ordernote3=" "	&& to be used with the mom 5.0 IEM layout
c_ordernote4=" "	&& to be used with the mom 5.0 IEM layout
c_ordernote5=" "	&& to be used with the mom 5.0 IEM layout
c_special_instruction = " "  && to be used with error code R 
c_error_mess = "" 	&& special data about an error that will go into telemark
c_bt_custnum =""	&& mom ship to customer number provided in the inbound import file
c_st_custnum =""	&& mom ship to customer number provided in the inbound import file 
n_billto = 0 
c_password=""	&& 3-19-10 added for cc decryption
l_skip_mod10 =.f.	&& 3-18-10 added for new version with cc encrypt
N_ACCESS = 0 
N_MAIN = 0 
**PA - 11/15/2010	To listen to the custid.optin
c_cust_shiplist = "IN" && IN is the default, OUT if opted out
c_scust_shiplist = "IN" && IN is the default, OUT if opted out
c_custom_info=""						&& will be used for the data we get from the SDQ segment