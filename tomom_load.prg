* 	This program extracts the data from the source file and loads it as a 
*	memory varriable.  The goal of this program is to capture all the data into
*	a standarized format for the mom import.
*  
*	The program will call a series of routines designed to import this data into 
*	into mom windows.  The idea is that the subroutines will not have to change
*	for new data fromats just this program.   
* 
* 	12-25-99 JC	FROMAT FOR EXTACTING FROM TOMOM.DBF
*   02-12-00 jc more work on program and initial testing 
*   02-14-00 jc more work added carrier and zone tables to program 
* 	02-17-00 PA & JC additional debugging on fix programs, custb, custs.prg 
* 	02-19-00 jc added enhancements to inlog.dbf and err_log.dbf in this program
* 	09-10-00 jc updated while working on c1tv added the do final and do zone as well as a routine to strip out spaces in credi card numbers
*
*	(1) tomom			inbound data file
*	(2) inlog			import log with data and number of orders
*	(3)	ERR_LOG			individual order error log
*	(4)					available for future use
*	(5)					available for future use
*			*** below loaded by tomom_open.prg ***
*	(6)	cust			with index on subst(zipcode,1,5)+lastname 	
*	(7) cms
*	(8)	items
*			     |--->price  
*	(11)stock----|
*	             |--->breakout
*
*	(12)serial
* 	(13)g:\programs\country
*
*              |---->county (15)
*	(16)zip----|
*              |---->state (14)
*	(17)box
*	(18)sequence
*  
*   (20)carrier ---->zones (19)
*
*	Note to programer : steps to follow when converting this to use with 
*						a new data source
*		step 1			watch for data type compatibility by checking the 
*						data types
*		Step 2			All these fields should have a value or have them set to blanks 
*						watch that each loop either sets a new value or a blank. 
*						(except prospect which is required)
*		step 3 			check if this is just a customer order or a prospect c_prospect "Y" or "N"
*
*		Other points :
* 						- in this program you can open up 5 tables (select 6-15 are reserved for mom tables) 
*						- in the linked programs we clean up phone numbers, addresses, states and country codes
*						- the programer must however standarize data on paymethods, ship methods, credit card types
*						  in this module 
*						
*
if file("h:\mom\enf\importok.mem")
	restore from h:\mom\enf\importok		&& pulls in the latest advise if an import is okay to run
else
	goahead=.f.
endif
if goahead
	set delete on
	set safety ON
	CLOSE DATABASES
	do tomom_init
	cd c:\mom\nvm							&& make change here for any new company
	do H:\REPORT\TOMOM2\tomom_open			&& opens all the files we need in mom
	select 2
	use H:\REPORT\TOMOM2\inlog 				&& where we log data on this import
	append blank
	replace date_in with date()
	replace time_in with time()
	select 3
	use H:\REPORT\TOMOM2\err_log				&& this is where we any error we find during the import
	*zap 
	SELECT 1
	cd h:\report\tomom2
	use tomom
	do while ! eof()
		if a.continued<>"Y"	&& in the tomom table layout we are limited to just 5 items on an order
							&& when there is more we make the next record a continuation (a.continued ="Y"
							&& when this happens we don't load the customer info or the cms, we just want to 
							&& add the additonal items on the order
			n_order = 0
			N_error_to_log=0	&& way to see if an error has been loged on this order
			n_custnum=a.custnum	&& not used in the import loaded here just for compatiblilty with the MOM iem.dbf file 
			C_altnum=a.altnum	&& pulled into the cust.altnum field
			c_lastname=proper(a.lastname)
			c_firstname=proper(a.firstname)
			c_company=proper(a.company)
			c_address1=proper(a.address1)
			c_address2=proper(a.address2)
			c_city=proper(city)
			c_country=a.country 
			c_rcountry=upper(substr(a.rcountry,1,15))
			c_state=upper(a.state)
			c_zipcode=a.zipcode
			c_phone=a.phone		&& TOMOM_CUSTB.prg will send off to fix phone	
			c_phone2=a.phone2
			c_email=a.email
			c_comment = a.comment
			c_ctype=a.ctype1  && or O
			c_ctype2=a.ctype2
			c_ctype3= "  "
			c_taxtxempt=upper(a.taxexempt)  	&& import program assumes "  " to be no 
			c_prospect = "N"
			c_cardtype=upper(a.cardtype)
			c_cardnum=a.cardnum
			c_expires=a.expires				&& EXPECTING "01/01"
			c_source_key = upper(a.source_key)
			c_catalog=upper(a.catalog)
			c_sales_id ="OL"
			c_oper_id = "OL"
			d_order_date=ctod(a.order_date)
			c_odr_num=str(a.odr_num)
			c_paymethod=a.paymethod
			c_greeting1=a.greeting1
			c_greeting2=a.greeting2
			n_promocred=a.promocred
			c_sfirstname=proper(a.sfirstname)
			c_slastname=proper(a.slastname)
			c_scompany=proper(a.scompany)
			c_saddress1=proper(a.saddress1)
			c_saddress2=proper(a.saddress2)
			c_scity=proper(a.scity)
			c_sstate=upper(a.sstate)
			c_szipcode=a.szipcode
			c_scountry=a.scountry
			c_srcountry=a.rcountry
			c_sphone=a.sphone			&& tomom_custs.prg will send off to fix phone
			c_sphone2 =a.sphone2
			c_semail=a.semail
			c_reference=a.reference
			C_SHIPVIA=UPPER(A.SHIPVIA)
			* in this module we do all the necessary standarization of import data for items like this 
	*!*			do case
	*!*		        CASE UPPER(a.shipvia)="UPS GROUND"
	*!*		            replace cms->shiplist with "UPS"
	*!*		            if a.freight<>6.95.or.a.state="AK".OR. F->STATE="HI".OR. F->STATE="PR"
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->shipvia)="APO"
	*!*		            replace cms->shiplist with "FC"
	*!*		            if a.freight<>6.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="3 DAY FED X"
	*!*		            replace cms->shiplist with "FE3"
	*!*		            if a.freight<>9.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="FED X PATCH"
	*!*		            replace cms->shiplist with "FE3"
	*!*		            if a.freight<>9.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="FED X UPGRA"
	*!*		            replace cms->shiplist with "FE3"
	*!*		            if a.freight<>14.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="2 DAY FED X"
	*!*		            replace cms->shiplist with "FE2"
	*!*		            if a.freight<>12.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="2 DAY FEDX"
	*!*		            replace cms->shiplist with "FE2"
	*!*		            if a.freight<>12.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="1 DAY FED X"
	*!*		            replace cms->shiplist with "FEP"
	*!*		            if a.freight<>16.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="FED-X SAT"
	*!*		            replace cms->shiplist with "FEX"
	*!*		            if a.freight<>26.95
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="INTL  CANAD"
	*!*		            replace cms->shiplist with "FEC"
	*!*		            if a.freight<>24.50
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="FEDX  CANAD"
	*!*		            replace cms->shiplist with "FEC"
	*!*		            if a.freight<>24.50
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="INTL PRIORI"
	*!*		            replace cms->shiplist with "FEI"
	*!*		            if a.freight<>35.00
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        CASE UPPER(F->SHIPVIA)="AIR MAIL"
	*!*		            replace cms->shiplist with "AC"
	*!*		            if a.freight<>15.00
	*!*		                replace a.errorc with rtrim(a.errorc)+"K"
	*!*		            endif
	*!*		        OTHERWISE
	*!*		            replace cms->shiplist with "FC"
	*!*		            replace a.errorc with rtrim(a.errorc)+"K"
	*!*		    ENDCASE
			c_shipvia=upper(shipvia)
			n_paid=a.paid						&& for now must not be paid and error message should be placed below if paid
			c_internetid=a.internetid
			d_holddate=a.holddate
			c_useshipamt=a.useshipamt			&& for now if not this will should error out below 
			n_shipping=a.shipping				&& for now we must have the correct shipping amount
					
			* go load mom with this customer record Presumed to be a prospect 
			*messagebox("run tomom_custb")
			do tomom_custb
			
			* here we put a do loop to get all the items on the order
			if c_prospect <> "Y"				&& we don't have a prospect only
				*messagebox("run tomom_cms")
				do TOMOM_CMS	&& go load the cms file with basic order data and update the billto customer file with order data
			endif								&& we want to get this done before we move to the customer record for shiping
		endif && not a.continued	
		** the above stuff is only done when this is an origional order if it is a continuation of a previous record we want to skip the above
		** this next section is 
		** the below approach is designed for a flat file format ( see myp_tomom for a relational file approach) 
	   	if A.product01<>space(10)
	        c_item =a.product01 		&& this varriable is trimed to 20 characters later in the program for compatibility with mom data structures
	        n_quant =a.quantity01
	        if upper(a.useprices)="Y".or.upper(a.useprices)="X"
	        	l_useprices=.t.			&& this means we will import your prices and not change them to the mom based prices (exceptions will be noted in the error file) 
	        else
	        	l_useprices=.f.			&& you want us to use the MOM based pricing (current prices at the time of the import instead of the priceing in the file)
	        endif
	        n_price=a.price01
	        *n_price =a.unitprice1	&& layout for piv mom
	        n_discount=a.discount01
	        *n_nowon =a.refid1
	        *messagebox("run tomom_items")
	        do h:\report\tomom\tomom_items
	    endif
	    if A.product02<>space(10)
	        c_item =a.product02
	        n_quant =a.quantity02
	        if upper(a.useprices)="Y".or.upper(a.useprices)="X"
	        	l_useprices=.t.
	        else
	        	l_useprices=.f.
	        endif
	        n_price=a.price02
	        *n_price =a.unitprice2
	        n_discount=a.discount02
	        *n_nowon =a.refid2
	        do h:\report\tomom\tomom_items
	    endif
	    if A.product03<>space(10)
	        c_item =a.product03
	        n_quant =a.quantity03
	        if upper(a.useprices)="Y".or.upper(a.useprices)="X"
	        	l_useprices=.t.
	        else
	        	l_useprices=.f.
	        endif
	        *n_price =a.unitprice3 
	        n_price=a.price03
	        n_discount=a.discount03
	        *n_nowon =a.refid3
	        do h:\report\tomom\tomom_items
	    endif
	    if A.product04<>space(10)
	        c_item =a.product04
	        n_quant =a.quantity04
	        if upper(a.useprices)="Y".or.upper(a.useprices)="X"
	        	l_useprices=.t.
	        else
	        	l_useprices=.f.
	        endif
	        n_price=a.price04
	        *n_price =a.unitprice4
	        n_discount=a.discount04
	        *n_nowon =a.refid4
	        do h:\report\tomom\tomom_items
	    endif
	    if A.product05<>space(10)
	        c_item =a.product05
	        n_quant =a.quantity05
	        if upper(a.useprices)="Y".or.upper(a.useprices)="X"
	        	l_useprices=.t.
	        else
	        	l_useprices=.f.
	        endif
	        n_price=a.price05
	        *n_price =a.unitprice5
	        n_discount=a.discount05
	        *n_nowon =a.refid5
	        do h:\report\tomom\tomom_items
	    endif 
	    select tomom
	    skip
		if a.continued<>"Y"		&& we only want to add the ship to customer record as the last thing
								&& we do on this order so we move to the next record first to insure
								&& this is not a continued record. (no worry we have the ship to 
								&& data from the previous record loaded into memory)  
			DO TOMOM_CUSTs		&& go add the a customer record for shipto if needed 
			*messagebox("check over shipto")
			do tomom_zone		&& adds the zone and c_table data into cms
		
			do tomom_final		&& adds in the cms.next_pay and the tax on cms.vbor
			IF N_error_to_log>0	&& ORDER WITH ERRORS SHOULD BE PUT ON HOLD
	        	replace cms->PERM_HOLD WITH .T.
	        	replace cms->order_st2 with "PE"
	        	replace err_log.mom_cust with n_custnum
		        replace err_log.mom_order with n_order
	    	endif
	    	IF CMS->NALL=0		&& ORDERS WITH NO ITEMS ARE GIVEN A MOM SETTING OF EXTRA2=" P"
	    		REPLACE CMS->EXTRA2 WITH " P"
	    	ENDIF
		endif
		select tomom
	enddo
	select inlog							&& update the inlog file with data on this import
	replace cust_rec  with n_cust_rec
	replace order_rec with n_order_rec
	replace items_rec with n_item_rec
	replace value with n_value
else
	select 2
	use H:\REPORT\TOMOM2\inlog shared				&& where we log data on restriction on the import 
	append blank
	replace filename with "enf_tomom"
	replace date_in with date()
	replace time_in with time()
	replace problems with "import set to off"
endif	

	
