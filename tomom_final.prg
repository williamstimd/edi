* tomom_final.prg  this program adds the final data to the cms file.
* here we calculate how much the next payment is for this order
* it is possible that the item on back order is nontaxable and as 
* a result we will all the tax now. cms.Vbor should include the prorated amount of tax.
* we need to add this now and then calculate the next pay amount 
* 03-24-00 	jc	added this program to series of program (called from main program)
* 03-26-00 	jc	added select cms 
* 04-01-00 	jc	final pass before going live on h:\mom
* 08-21-01 	jc	moved select cms to top because the write was not completed unless cms was selected
* NOW WE NEED TO CALCULATE THE TAX FOR ORDERS THAT ARE IMPORTED WITHOUT TAX DATA
* 09-23-02 	jc	moved the code for creating a greeting to this module from tomom_custs 
* 11/11/02 	jc	changed the way we date payments into the journal 
* 01/11/03 	jc	changed again the way we date payments into the journal
* 08/12/2003 jc	changed for paid orders to use the variable c_oper_id as the USERID in Journal
* 09-03-2003 jc	changed the code to respect the c_taxexempt public varriable 
* 09-08-2003 jc changed the way we keep the running tabulation for nontaxable merchandise in cms.vntm  
* 03-23-2005 jc updated with code to add data to the odrmemo.dbf table 
*messagebox("cms tax test final1="+str(cms.tax))

if len(alltrim(c_ordm_notes))>0 or len(alltrim(c_ordm_fulfill))>0 or len(alltrim(c_ordm_desc1))>0 or len(alltrim(c_ordm_desc2))>0 or len(alltrim(c_ordm_desc3))>0 or len(alltrim(c_ordm_desc4))>0 or len(alltrim(c_ordm_desc5))>0 or len(alltrim(c_ordm_desc6))>0
	select ordmemo
	if cms.order<>ordmemo.order 		&& we have not yet had a reason to create a ordmemo for this order 
		append blank
		replace order with cms.order
	endif
	replace ordmemo.notes with alltrim(ordmemo.notes)+" "+ c_ordm_notes			&& loaded in final for the ordmemo.dbf file 
	replace ordmemo.fulfill with alltrim(ordmemo.fulfill)+" "+ c_ordm_notes		&& loaded in final for the ordmemo.dbf file 	
	replace ordmemo.desc1 with c_ordm_desc1 
	replace ordmemo.desc2 with c_ordm_desc2 
	replace ordmemo.desc3 with c_ordm_desc3 
	replace ordmemo.desc4 with c_ordm_desc4
	replace ordmemo.desc5 with c_ordm_desc5  
	replace ordmemo.desc6 with c_ordm_desc6 
endif

if ! isnull(alltrim(c_greeting1)) 			&& 09-23-02 moved to this module from tomom_custb because it is concievable that 
	if Len(alltrim(c_greeting1))>0			&&			a customer may send a gift to his / her own billing address
		select gxr							&& 			even if it simply was "don't open until xmas"	
		append blank
		replace order with cms.order
		replace bcustnum with cms.custnum
		if cms.shipnum >0
			replace scustnum with cms.shipnum
		else
			replace scustnum with cms.custnum
		endif
		replace odr_date with cms.odr_date
		replace note1 with c_greeting1
		replace note2 with c_greeting2
		replace note3 with c_greeting3
		replace note4 with c_greeting4
		replace note5 with c_greeting5
		replace note6 with c_greeting6
		replace cms->shiptype with "G"
	endif
endif

select cms
IF upper(c_taxexempt)<>"YES"		&& we need to add any sales tax on shipping based on the ship to location 
	IF country.taxship															&& the calculation for when we need to tax shipping in addtion to all the taxable merchandise
		REPLACE CMS.TAX WITH cms.tax+(cms.shipping*country.ntaxr/100)
		replace cms.ord_total with cms.ord_total + (cms.shipping*country.ntaxr/100)
		if cms.nfill>0
			REPLACE CMS.R_TAX WITH cms.R_tax+(cms.shipping*country.ntaxr/100)
		ELSE 
			REPLACE CMS.Nr_TAX WITH cms.Nr_tax+(cms.shipping*country.ntaxr/100)
		ENDIF
		*?cms.shipping*(country.ntaxr/100)
	endif
	IF state.taxship															&& the calculation for when we need to tax shipping in addtion to all the taxable merchandise
		REPLACE CMS.TAX WITH cms.tax+(cms.shipping*state.taxrate/100)
		replace cms.ord_total with cms.ord_total +(cms.shipping*state.taxrate/100)
		if cms.nfill>0
			REPLACE CMS.R_TAX WITH cms.R_tax+(cms.shipping*state.taxrate/100)
		ELSE 
			REPLACE CMS.Nr_TAX WITH cms.Nr_tax+(cms.shipping*state.taxrate/100)
		ENDIF
		*?cms.shipping*(state.taxrate/100)
	endif
	IF county.taxship															&& the calculation for when we need to tax shipping in addtion to all the taxable merchandise
		REPLACE CMS.TAX WITH cms.tax+(cms.shipping*county.ctaxr/100)	
		replace cms.ord_total with cms.ord_total + (cms.shipping*county.ctaxr/100)
		if cms.nfill>0
			REPLACE CMS.R_TAX WITH cms.R_tax+(cms.shipping*county.ntaxr/100)
		ELSE 
			REPLACE CMS.Nr_TAX WITH cms.Nr_tax+(cms.shipping*county.ntaxr/100)
		ENDIF
		*?cms.shipping*(county.ctaxr/100)
	endif
	IF zip.taxship															&& the calculation for when we need to tax shipping in addtion to all the taxable merchandise
		REPLACE CMS.TAX WITH cms.tax+(cms.shipping*zip.itaxr/100)
		replace cms.ord_total with cms.ord_total + (cms.shipping*zip.itaxr/100)
		if cms.nfill>0
			REPLACE CMS.R_TAX WITH cms.R_tax+(cms.shipping*zip.itaxr/100)
		ELSE 
			REPLACE CMS.Nr_TAX WITH cms.Nr_tax+(cms.shipping*zip.itaxr/100)
		ENDIF
		*?cms.shipping*(zip.itaxr/100)
	endif
endif 	

* This next section deals with orders that have prepaid
if N_paid>0
	
	replace cms.checkamoun with N_paid
	replace cms.approval with c_approval
	replace cms.check with "G"
	replace cms.vcount with 1
	*3-16-07 removed in favor of the case statement below -- replace cms.order_st2 with "IN"
	* replace opevent.op_state with "IN" 
	do case												&& 3-16-07 added case statment to set this order state in the case of back orders
			case cms.nbor=0 
				replace cms.order_st2 with "IN"
				replace opevent.op_state with "IN" 
			case cms.nbor>0 and cms.nfill=0 
				replace cms.order_st2 with "BO"
				replace opevent.op_state with "BO" 
			case cms.nbor>0 and cms.nfill>0 and cms.multiship
				replace cms.order_st2 with "IN"
				replace opevent.op_state with "IN" 
			case cms.nbor>0 and cms.nfill>0 and not cms.multiship
				replace cms.order_st2 with "BO"
				replace opevent.op_state with "BO" 
			otherwise
				replace cms.order_st2 with "IN"				&& prior to 3-16-07 this was the settting for all orders
				replace opevent.op_state with "IN" 
	endcase
	replace cms.acc_state with "AP"
	replace cms.overpay with n_paid	- cms.tb_ship-cms.tb_merch-cms.tb_tax		&& added 10/17/00 for paid orders so if 
																				&& the order does not ship it will show 
																				&& on the ar as a pre-paid order 
																				&& we have to deduct the total billed items
																				&& incase this order is also filled in advance
	select sequence
	locate for file="JOURNAL"
	IF ! EOF()
		TRY = 0
		DONE = .F.
		DO WHILE ! DONE .AND. TRY<10  
			IF RLOCK() 
				REPLACE NEXT_VAL WITH NEXT_VAL+1
				DONE = .T.
			ENDIF
		ENDDO
	ELSE
		MESSAGEBOX("PROGRAM ABORTED NO JOURNAL IN SEQUENCE FILE")
		QUIT
	ENDIF
	SELECT JOURNAL
	APPEND BLANK
	if c_PAYMETHOD ="CK" && ADDED 8-6-02 because we noticed mom uses the checknum fields in the journal instead of cms for checks with order
		** 11/21/02 changed to date() instead of cms.odr_date because it was possible we could import an order for a date that is already closed from an reporting standpoint 
		REPLACE ORDER WITH CMS.ORDER,INPART WITH "A", ACCT WITH "PI", PAYMETHOD WITH CMS.PAYMETHOD, CARDTYPE WITH CMS.CARDTYPE, ODR_DATE WITH date(), NOTATION WITH "CK REC AT CORP #:"+ALLTRIM(c_reference),CHECKNUM WITH c_reference, AMOUNT WITH N_PAID, USERID WITH c_oper_id ,TRANS_ID WITH SEQUENCE.NEXT_VAL
	else
		** 11/21/02 changed to date() instead of cms.odr_date because it was possible we could import an order for a date that is already closed from an reporting standpoint 
		** 01/11/03 changed again to use the actual pay date when we have it because balancing has become annother problem 
		if d_pay_date<>date()	
			REPLACE ORDER WITH CMS.ORDER,INPART WITH "A", ACCT WITH "PY", PAYMETHOD WITH CMS.PAYMETHOD, CARDTYPE WITH CMS.CARDTYPE, ODR_DATE WITH d_pay_date, NOTATION WITH "CR.INCARD: "+CMS.APPROVAL, AMOUNT WITH N_PAID, USERID WITH c_oper_id,TRANS_ID WITH SEQUENCE.NEXT_VAL
		else
			REPLACE ORDER WITH CMS.ORDER,INPART WITH "A", ACCT WITH "PY", PAYMETHOD WITH CMS.PAYMETHOD, CARDTYPE WITH CMS.CARDTYPE, ODR_DATE WITH date(), NOTATION WITH "CR.INCARD: "+CMS.APPROVAL, AMOUNT WITH N_PAID, USERID WITH c_oper_id,TRANS_ID WITH SEQUENCE.NEXT_VAL
		endif
	endif
ENDIF
select cms		

replace cms.next_pay with cms.ord_total-cms.vbor-cms.vdsb-CMS.CHECKAMOUN

if l_multiship = .t.								&& 06-07-02 added this option to set multship from the data file 
	if cms->nbor>0.and.cms->nfill>0
    	replace cms->multiship with .t.
	endif
else
	replace cms.multiship with .f.
endif
if cms->nfill>0
    replace cms->picked with .t.
endif


* TAX ISSUES 
** below we look at the tax given to us from the import,  if this differ by more than .05 then we put the order on 
** hold and keep the amount from the import.  If someone goes to look at the order inside of mom, Mail Order Manger will
** recalulate the tax an show the amount calulated by MOM

if L_tax_quote and abs(cms->tax-n_tax)>.05	&& our inbound file had a tax amount and it is different than the mom tax calculation and therefore we will kick an error 
	c_error_code="L"
    do tomom_Log_errors
    select cms
    REPLACE CMS->TAXMODIFY WITH .T.
    *messagebox("shiptax order #"+str(cms.order,8)+" momtax:"+str((cms->ord_total-cms->tax)*(cms->ntaxrate+cms->staxrate+cms->ctaxrate+cms->itaxrate)/100,8,3)+" pivtax:"+str(cms->tax,8,3))
endif
    * put order on hold with errors unless it its a tax issue because that is
   
*messagebox("cms tax test final2="+str(cms.tax))

if len(alltrim(c_thirdparty)) > 0
	select third_party
	append blank
	replace third_party.client with m_code
	replace third_party.order with cms.order
	replace third_party.inpart with "A"
	replace third_party.carrier with cms.shiplist
	replace third_party.account with c_thirdparty
	replace third_party.user with "OLS"
	replace third_party.date with date()
	replace third_party.time with time()
endif