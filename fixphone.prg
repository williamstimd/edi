* phone.prg
* 2-19-98 designed to convert all kinds of phone number into the mom format of
*         (XXX) XXX-XXXX   
* 01-03-00 updated with two new formats
* 03-19-00 updated with new formats and organized by lenght 
* varriables
*			m_phone = inbound number
*			p_phone = fixed number                                                   
M_phone = strtran(strtran(m_phone, " ",""),"+","")  && 10-25-07 strip out "+" and spaces from a phone number, this will eliminate a series of options below but I will keep them in for now just to keep the format check in place 
do case
    case M_phone=space(14)																		&& phone = '         '
        p_phone =space(14)
    case  substr(M_phone,1,1)="(".and.substr(M_phone,5,2)=") ".and.substr(M_phone,10,1)="-"		&& phone =(xxx) xxx-xxxx
        p_phone =M_phone
    case substr(M_phone,1,1)="(".and.substr(M_phone,5,2)=")-".and.substr(M_phone,10,1)="-"		&& phone =(xxx)-xxx-xxxx
        p_phone =substr(M_phone,1,5)+" "+substr(M_phone,7)
    case substr(M_phone,1,1)="(".and.substr(M_phone,5,1)=")".and.substr(M_phone,6,1)<>" "		&& phone =(xxx)xxx?????
    	if len(alltrim(substr(m_phone,6)))=7															&& phone =(xxx)xxxxxxx
		p_phone = substr(m_phone,1,5)+" "+substr(m_phone,6,3)+"-"+substr(m_phone,9)
	endif
	IF substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,1)=")".AND.SUBSTR(M_phone,9,1)="-"	&& phone =(xxx)xxx-xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,6)
	ENDIF
    case len(rtrim(M_phone))=8																	&& phone =????????
        p_phone ="(000) "+substr(M_phone,1)
    case len(rtrim(M_phone))=9																	&& phone =?????????
        p_phone ="(000) "+substr(M_phone,1)
    case len(rtrim(M_phone))=10																	&& phone =xxxxxxxxxx
        p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,4,3)+"-"+substr(M_phone,7)
    case len(rtrim(M_phone))=11	
    	do case 
    		case substr(M_phone,4,1)="-"														&& phone =xxx-xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,4,1)=" "														&& phone =xxx xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,4,1)="."														&& phone =xxx.xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,4,1)="/"														&& phone =xxx/xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,4,1)="_"														&& phone =xxx_xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,4,1)=")"														&& phone =xxx)xxxxxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,1,1)="("														&& phone =(xxxxxxxxxx
        		p_phone =substr(M_phone,1,4)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,7,1)="."														&& phone =xxxxxx.xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,4,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,7,1)="-"														&& phone =xxxxxx-xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,4,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,7,1)="_"														&& phone =xxxxxx_xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,4,3)+"-"+substr(m_phone,8)
        	case substr(M_phone,7,1)="/"														&& phone =xxxxxx/xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,4,3)+"-"+substr(m_phone,8)
    		case substr(M_phone,4,1)=" ".and.substr(M_phone,8,1)=" "							&& phone =xxx xxx xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,8)	
        	otherwise
       			p_phone ="("+substr(M_phone,1,3)+")"+substr(M_phone,4,4)+"-"+substr(M_phone,8) 	&& phone=xxxxxxxxxxx - too long to be a us Number
       	ENDCASE
    case len(rtrim(M_phone))=12																	&& phone =????????????
    	do case
    		case substr(M_phone,4,1)="-".and.substr(M_phone,8,1)="-"							&& phone =xxx-xxx-xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5)
    		case substr(M_phone,4,1)=" ".and.substr(M_phone,8,1)=" "							&& phone =xxx xxx xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,9)
    		case substr(M_phone,4,1)=".".and.substr(M_phone,8,1)="."							&& phone =xxx.xxx.xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,9)
    		case substr(M_phone,4,1)=" ".and.substr(M_phone,8,1)="-"							&& phone =xxx xxx-xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5)
    		case substr(M_phone,4,1)="-".and.substr(M_phone,8,1)="."							&& phone =xxx-xxx.xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,9)
    		case substr(M_phone,4,1)="/".and.substr(M_phone,8,1)="-"							&& phone =xxx/xxx-xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5)								
    		case  substr(M_phone,4,1)="/".and.substr(M_phone,8,1)=" "							&& phone =xxx/xxx xxxx
        		p_phone ="("+substr(M_phone,1,3)+") "+substr(M_phone,5,3)+"-"+substr(m_phone,9)
    		case SUBSTR(M_phone,4,1)="/".AND.SUBSTR(M_phone,8,2)="/"							&& phone =xxx/xxx/xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)="/".AND.SUBSTR(M_phone,8,2)="."							&& phone =xxx/xxx.xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=".".AND.SUBSTR(M_phone,8,2)="/"							&& phone =xxx.xxx/xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=".".AND.SUBSTR(M_phone,8,2)=" "							&& phone =xxx.xxx xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=".".AND.SUBSTR(M_phone,8,2)="-"							&& phone =xxx.xxx-xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=")".AND.SUBSTR(M_phone,8,2)="-"							&& phone =xxx)xxx-xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=")".AND.SUBSTR(M_phone,8,2)=" "							&& phone =xxx)xxx xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,1)=")".AND.SUBSTR(M_phone,8,2)=" "							&& phone =(xxxxxx xxxx
        		p_phone =SUBSTR(M_phone,1,4)+") "+SUBSTR(M_phone,5,3)+"-"+SUBSTR(M_phone,9)
        	case SUBSTR(M_phone,4,2)=") "														&& phone =xxx) xxxxxxx needed for adaptec
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,6,3)+"-"+SUBSTR(M_phone,9)
        	otherwise
        		p_phone ="("+substr(M_phone,1,3)+")"+substr(M_phone,4,4)+"-"+substr(M_phone,8) && phone xxxxxxxxxxxx
        endcase
    case len(rtrim(M_phone))=13
    	do case
    		case SUBSTR(M_phone,4,1)="/ ".AND.SUBSTR(M_phone,9,2)="/"							&& phone =xxx/ xxx/xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,6,3)+"-"+SUBSTR(M_phone,10)
        	case SUBSTR(M_phone,4,1)=". ".AND.SUBSTR(M_phone,9,2)="."							&& phone =xxx. xxx.xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,6,3)+"-"+SUBSTR(M_phone,10)
        	case SUBSTR(M_phone,4,1)="  ".AND.SUBSTR(M_phone,9,2)="."							&& phone =xxx  xxx.xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,6,3)+"-"+SUBSTR(M_phone,10)
        	case SUBSTR(M_phone,4,1)="  ".AND.SUBSTR(M_phone,9,2)=" "							&& phone =xxx  xxx xxxx
        		p_phone ="("+SUBSTR(M_phone,1,3)+") "+SUBSTR(M_phone,6,3)+"-"+SUBSTR(M_phone,10)
        	case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,1)=")".AND.SUBSTR(M_phone,9,1)="-"&& phone =(xxx)xxx-xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,6)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,1)=")".AND.SUBSTR(M_phone,9,1)=" "&& phone =(xxx)xxx xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,6,3)+"-"+substr(m_phone,10)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,1)=")".AND.SUBSTR(M_phone,9,1)="."&& phone =(xxx)xxx.xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,6,3)+"-"+substr(m_phone,10)
			otherwise
        		p_phone =M_phone
    	endcase
    case len(rtrim(M_phone))=14																	&& phone =??????????????
    	do case																
    		case substr(M_phone,1,2)="1-".or.substr(M_phone,1,2)="1.".or.substr(M_phone,1,2)="1 "&& phone =1-????????????
    			if substr(M_phone,6,1)=" ".and.substr(m_phone,10,1)=" "							&& phone =1-xxx xxx xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7,3)+"-"+substr(m_phone,11)
    			endif
    			if substr(M_phone,6,1)=" ".and.substr(m_phone,10,1)="-"							&& phone =1-xxx xxx-xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(M_phone,7)
    			endif
    			if substr(M_phone,6,1)="-".and.substr(m_phone,10,1)="-"							&& phone =1-xxx-xxx-xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7)
    			endif
    			if substr(M_phone,6,1)="-".and.substr(m_phone,10,1)=" "							&& phone =1-xxx-xxx xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7,3)+"-"+substr(m_phone,11)
    			endif
    			if substr(M_phone,6,1)=".".and.substr(m_phone,10,1)="."							&& phone =1.xxx.xxx.xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7,3)+"-"+substr(m_phone,11)
    			endif
    			if substr(M_phone,6,1)=".".and.substr(m_phone,10,1)=" "							&& phone =1.xxx.xxx xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7,3)+"-"+substr(m_phone,11)
    			endif
    			if substr(M_phone,6,1)=" ".and.substr(m_phone,10,1)="."							&& phone =1.xxx xxx.xxxx
    				p_phone ="("+substr(M_phone,3,3)+") "+substr(m_phone,7,3)+"-"+substr(m_phone,11)
    			endif
    		case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=").".AND.SUBSTR(M_phone,9,1)="."&& phone =(xxx).xxx.xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=")-".AND.SUBSTR(M_phone,9,1)="."&& phone =(xxx)-xxx.xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=") ".AND.SUBSTR(M_phone,9,1)=" "&& phone =(xxx) xxx xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=") ".AND.SUBSTR(M_phone,9,1)="_"&& phone =(xxx) xxx_xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=")_".AND.SUBSTR(M_phone,9,1)="_"&& phone =(xxx)_xxx_xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
			case substr(m_phone,1,1)="(".and.SUBSTR(M_phone,5,2)=")_".AND.SUBSTR(M_phone,9,1)="_"&& phone =(xxx)_xxx_xxxx
				p_phone =SUBSTR(M_phone,1,5)+" "+SUBSTR(M_phone,7,3)+"-"+substr(m_phone,11)
    		otherwise
        		p_phone =M_phone
    	endcase
    otherwise
        p_phone =M_phone
endcase              
IF LEN(ALLTRIM(P_PHONE))>14 AND VARTYPE(c_comment)="C"			&& after we have fixed this phone number it is too long to fit into MOM so we are going to put the raw one in MOM 
	c_comment = m_phone		&& we are dumping the raw phone number in this field
endif