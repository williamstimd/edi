**	EDI_810.prg
**		This program creates a file
**		 to be sent to the trading partner with invoice information.  
**
**
** 12-18-06	JC	The resulting files are sent via FTP to Sterling Informaiton Broker.  
**				our Mailbox Id : 	SJMINFTO 	- Data Slot outbound
**									SJMINFTI	- Data Slot inbound
**									SJMINFTR	- Report Slot
**				Password			4088482
**				Communications Id :	12			- 12=our phone number is our id 
**												  ZZ= our name is our id
**			prod Communicaitons Id	4088482282	- What Patrik set up with Sterling as our account
**									4088483525	- is what we use with GSX
**
** 				IP address for FTP 	209.95.224.133 default ports 20 and 21 data needs to be sent
**									Ascii (not binary) and anything over 512 bytes needs to be 
**									streamed.
** 
**				web address : 		sciftp.commerce.stercomm.com
**				
**  Note that you must initally log in the outbound slot and perform a change Directory command
**	to access any other slots.  Also be sure to enter the slots in all caps

**	Technical support hotline number for your mailbox is 877-432-4300.  

** 	in this program we use 
**							Segment Terminator 	-  a line feed ( Market point we use ~"
**							element seperator	-  * (Jc Penny uses a bell character)
**							subelement seperator-  > on the ISA line 

**	12-24-06	JC 	moved ST segment inside the order loop
**					created variable N_Inv_run_count to us in the GE segment	

**	12-26-06	JC	added code to deal with times when we dont have any orders to process and to move the send funciton indide the loop for good orders 
**	01-02-06 	JC 	added code to only run the routine if cms.userid="CCO" and invoice.authcode="  " instead of just authocode="  "
** 	04-16-07	JC	changed location of CDI
**  04-24-07 	JC  fixed bug that assumse the work station is mapped to M for ro-gilroy-cdi-1
** 	05-20-07	JC 	modify to allow auto run at 2 pm each day
**  07-31-08 	jC 	modified for generic use for new EDI clients
**  08-24-08 	JC 	we need to get the 9 digit wallgreens ap vendor number suppleid by accounts payable see REF segement
**  10-31-08 	JC  approved by OSH 
** 	12-16-08 	JC  work on approval for Walgreens 
**  01-02-12	SA&JC replace ftp command for chilkat version 
* 	04-28-13	JC 	added code to update edi_cms.in_810 and the id to 7814395515 for Maykah's Target.com domestic account 
**	05-15-15 	JC  program to support store invoices for Basis 

** 	next we should tie in the control number from the 850 file into eid_cms so we can enter the 810 into the edi_log
**  10-17-16	TW  modified for automation - still need to check for SCAC, BOL, and shipping method 
**  11-07-16    TW  sent first files via automated method for NRI and WWI
**  12-08-16    TW  added workorder procedure with pdfoutput
**
set deleted on
set safety off
clear
close databases
cd h:\report\company
@1,1 say " Program create an EDI 810 Sears report based on invoice transactions "


ON ERROR DO H:\REPORT\ADVANCED_ERR_HANDLER\ERR_HANDLE_CALL.PRG WITH ;
  ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
  
public C_program_name 
public c_username
public c_machine
public c_currentpath
public c_executable
PUBLIC c_compile_date
public c_exe_size
PUBLIC c_home
PUBLIC cc_att  
 
cc_att='edi@rushorder.com' 
c_program_name = 	"edi_810_sears.exe"								&&<-------------Enter program name here!
c_home = 			"h:\report\edi"									&&<-------------Enter program home directory here!
c_username = alltrim(substr(sys(0),at("#",sys(0))+1,len(sys(0))))  	&& capture the user name
c_machine = alltrim(substr(sys(0),1,at("#",sys(0))-1))				&& capture the workstation 
c_currentpath = sys(5)+sys(2003)									&& capture the current path 
c_executable = SYS(16,0)	


public ftp_name
public ftp_login
public ftp_pword
public ftp_path
public ftp_send
public ftp_rename 
public ftp_bye
public ftp_filename
public ftp_script

	**Chilkat Email 
	PUBLIC send_to
	PUBLIC send_from
	PUBLIC send_message
	PUBLIC send_cc
	PUBLIC send_bcc
	PUBLIC send_subject
	PUBLIC send_npriority

	*Chilkat FTP
	PUBLIC loftp
	PUBLIC lnsuccess
	PUBLIC c_filename
	PUBLIC c_filedir

IF vartype(n_target_order)="U"	&& This will let us know if we have already created these variables
	PUBLIC n_target_order
	public c_target_inpart
endif

STRTOFILE(TTOC(DATETIME())+' PROGRAM START - '+c_program_name+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)	


cd h:\report\company

SELECT 1
use H:\mom\profile alias company shared
set order to code 
*do form h:\report\company\company to c_code
LOCATE FOR code="ABL"
if FOUND()
*IF c_code="yes"
	public m_dataloc
	*m_dataloc="\\ro-gilroy-ad-1\win_mom"+company.dataloc
	m_dataloc=alltriM(company.drive)+company.dataloc
	*messagebox(m_dataloc)
	M_CLIENT=PROPER(COMPANY.NAME)
	c_target_client = company.code
else
	quit
endif


select 101
if used("master_prg_run_log")
	select master_prg_run_Log
else
	select 101
	use H:\mom\master_prg_run_log shared
endif
append blank
replace prg_name with c_program_name
replace client with company.code
replace date with date()
replace time with time()
replace comments with "started program"
*!*	REPLACE FUTURE2 WITH STR(N_LAST_TRANS_ID)							&& this is our start point for the this run ( the last record we looked at for the last report 
*!*	replace future3 with str(n_last_trans_id) 							&& put this in for now in case we don't find any new record for this report



public c_filename 
public c_tempname 
public N_total_ref_needed
public N_total_run_count 
public runline

select 288
use h:\report\edi\edi_log shared										&& this is the table where we will record in and out bound edi documents 
if ! file("h:\report\edi\edi_log.cdx")
	index on filename tag filename OF h:\report\edi\edi_log.cdx
	index on control_No tag control_no OF h:\report\edi\edi_log.cdx
	index on ponum tag ponum OF h:\report\edi\edi_log.cdx
ENDIF
**set order to control_no
set order to mom_order
set filter to edi_log.recv_id=substr(company.edi_id,1,15)				&& we will be setting a relationship to this child table and we want to limit it just to this client's inbound files 


*IF VAL(ISA)<1000000000 and company.edi_id<>"          " && we are expecting all of these companies to have edi data in the company profile
														&& this data is entered by the EDI programmer when we first set the client up for EDI trading
														&& and we need a unique control number at least 10 digits long 
	CD (m_dataloc)	
	select 31
	if file("edi_ucc128.dbf")			
		use edi_ucc128 shared						&& our location for our label data 
	else
		use h:\report\edi\master_edi_ucc128 shared
		copy stru to edi_ucc128
		use edi_ucc128
	endif
	if not file("edi_ucc128.cdx")
		index on ordernum tag ordernum of edi_ucc128.cdx
		index on alltrim(ordernum)+number tag orderitem of edi_ucc128.cdx
	endif
	set order to orderitem
	
	SELECT 2
	USE BOX SHARED
	SET ORDER TO ORDER
	
	select 3 
	use carrier shared
	set order to ca_code
	
	
	select 50					&& each client will have its own table in the client directory to hold customer specific trading data
	if file("EDI_cms.dbf")
		use edi_cms shared		&& each trading partner will have its own record in this table 
	else
		use h:\report\edi\master_edi_cms shared
		copy stru to edi_cms 
		use EDI_cms
		append blank
		*messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("edi_CMS.cdx")
		index on order tag order OF edi_cms.cdx
		index on custnum tag custnum OF edi_cms.cdx
	endif
	REPLACE id WITH RECNO() FOR id=0
	set order to order
	*LOCATE FOR ORDER=1336 && JUST FOR BBB FISRT ORDER
	 
	select 4
	use cms shared
	set order to order
	set relation to shiplist into carrier 
	set relation to order into edi_cms additive
	
	select 6
	use h:\report\audit\adp_co shared
	set order to country

	select 7
	use cust shared  &&exclusive &&
	*set order to tag title of cust.cdx
	*index on title tag title of cust.cdx
	set order to custnum
	*set relation to altnum into edi_cust 
	set relation to country into adp_co additive
	
	select 8
	use stock shared
	set order to number
	
	select 9
	if file("REC_ID.dbf")
		use REC_ID shared		&& each trading partner will have its own record in this table 
	else
		use H:\report\edi_rec_id_master shared
		copy stru to REC_ID 
		use REC_ID
		append blank
		*messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("REC_ID.cdx")
		index on mom_itemid tag mom_itemid OF rec_id.cdx
	endif
	*use h:\mom\nap\rec_id.dbf shared					&&  costco sends to us the line item along with a record_id
	set order to mom_itemid 							&& 	we need to store this and match it back up with the item
														&&  sold when we send back the tacking number file.  
	select 10
	use items shared
	set order to inpart
	set relation to item into stock additive
	set relation to item_id into rec_id additive
	set relation to alltriM(company.code)+alltrim(str(order,10,0))+inpart+item into edi_ucc128 additive
	
	select 11
	use invoice shared
	set relation to order into cms additive
	set relation to custnum into cust additive
	set relation to order into edi_log additive
	SET relation to str(order,10,0)+inpart into box additive
	set relation to str(order,10,0)+inpart into items additive
	N_Inv_run_count = 0					&& number of invocies sent
	*locate for cms.userid="CCO" and invoice.authcode="   "
	
	*locate for invoice.order=n_target_order and invoice.inpart=c_target_inpart
	N_target_order =  0
	c_Inpart = "A"
	*@2,10 say " Enter the MOM order number  :" get n_target_order Picture "@99999"
	*@3,10 say " Enter the invoice part letter " get c_inpart picture "@!"
	*read
	
	*locate for order=n_target_order and inpart=c_inpart
	LOCATE FOR ALLTRIM(cms.cl_key)="EDI_SEARS" AND edi_cms.sh_997<>"          " AND edi_cms.in_810="          " 	and LEN(ALLTRIM(INVOICE.AUTHCODE))>0 AND AT("-",INVOICE.AUTHCODE)=0 AND INVOICE.AMOUNT<>0 AND ALLTRIM(edi_cms.company)<>"NRI_NETSUITE"
	
	DO WHILE NOT EOF()
		select 102
		use h:\report\edi\edi_vals shared
		GO TOP
		replace edi_vals.isa with alltrim(str(val(edi_vals.isa)+1))
		replace edi_vals.gs with alltrim(str(val(edi_vals.gs)+1))
		replace edi_vals.st with alltrim(str(val(edi_vals.st)+1))
		n_CONTROL_ISA = val(edi_vals.isa)
		n_CONTROL_GS = val(EDI_VALS.GS)
		n_CONTROL_ST = val(EDI_VALS.ST)

		C_FILENAME="m:\sterling\outbox\"+alltrim(company.edi_id)+"\EDI_810_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+".001"
		c_tempname = ALLTRIM(company.drive)+alltrim(company.dataloc)+"\lmm\edi_810_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+".txt"
		c_shortname="EDI_810_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+".001"
		do case
			CASE substr(edi_cms.interch_id,1,15)="611125"  && Sears
				runline = "ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*"+edi_cms.qual+"*6111250001*"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*U*00401*"+alltrim(str(n_control_ISA,10,0))+"*0*P*>"
				*messagebox("set up for test not production in the Interchange segment, and not set to send via FTP")
				* produciton mode runline = 'ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*"+edi_cms.qual+"*"+substr(edi_cms.interch_id,1,15)+"*"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*U*00401*"+alltrim(c_control_isa)+"*0*P*>'
				strtofile(runline+chr(13),c_tempname,.f.)		
				runline = "GS*IN*"+alltrim(company.edi_id)+"*6111250001*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(str(n_control_gs,10,0))+"*X*004010"  && normal
				*runline = "GS*SH*4088482282*COSTCOEWHSE*20"+substr(dtoc(date()),7,2)+substr(dtoc(date()),1,2)+substr(dtoc(date()),4,2)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(edi_vals.gs)+"*X*004010"
				strtofile(runline+chr(13),c_tempname,.t.)
			
		ENDCASE

																
		@2,1 SAY "Now processing customer record #" +str(cms.order,8,0)
		* 2-19-09 we are now putting a id into authcode when we do the 856 so we want something in authcode but just not a "-" because that is what we will put in when we do the 810 if INVOICE.AUTHCODE="   " AND AT("-",INVOICE.AUTHCODE)=0 AND INVOICE.AMOUNT<>0 && we have an record we have not previously sent, and either it is a customer record we have sent before or is eligible to set up 
		*if INVOICE.AUTHCODE<>"   " AND AT("-",INVOICE.AUTHCODE)=0 AND INVOICE.AMOUNT<>0 && we have an record with a asn sent but not a 810 yet (-) we have not previously sent, and either it is a customer record we have sent before or is
				
			runline = "ST*810*"+alltrim(str(n_control_st,10,0))
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count =1
			IF INVOICE.AMOUNT>0
				N_Inv_run_count = N_Inv_run_count +1
				** Beginning Segment for Invocie for a debit Invoice
				DO CASE
					CASE edi_cms.interch_id="611125"  && Sears
						runline =  "BIG*"+DTOS(invoice.INV_DATE)+"*"+ALLTRIM(STR(INVOICE.ORDER,10,0))+ALLTRIM(INVOICE.INPART)+"1*"+ALLTRIM(EDI_CMS.PO_DATE)+"*"+ALLTRIM(EDI_CMS.PO_NUM)	&& Beginning Segment for Ship Notice 
				ENDCASE 
			ELSE
				** Beginning setment for Invoice for a credit memo
				runline =  "BIG*"+DTOS(invoice.INV_DATE)+"*"+ALLTRIM(STR(INVOICE.ORDER,10,0))+ALLTRIM(INVOICE.INPART)+"*"+ALLTRIM(EDI_CMS.PO_DATE)+"*"+	+ALLTRIM(EDI_CMS.PO_NUM)+"***CR"	&& Beginning Segment for Ship Notice 
			ENDIF
			strtofile(runline+chr(13),c_tempname,.t.)	
			N_total_run_count = N_total_run_count + 1
			
			DO CASE
				CASE edi_cms.company="SEARS"
					runline =  "REF*IA*0000"+alltrim(EDI_CMS.VENdOR) && Reference Identification
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
				
					
					runline =  "REF*DP*"+alltrim(EDI_CMS.REF_DP) && Reference Identification
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
					
				
			ENDcase
			
			
			****** ----- Order level detail --------********
			DO CASE
				CASE EDI_CMS.COMPANY="SEARS"

					runline =  "N1*BY**92*"+alltrim(EDI_CMS.buyer) && Reference Identification
					strtofile(runline+chr(13),c_tempname,.t.)	
					N_total_run_count = N_total_run_count + 1
					
					runline =  "N1*RI*PARTNERSX ROBOTICS*92*"		&& Name Remit to plus a 9 digit ap code
					strtofile(runline+chr(13),c_tempname,.t.)												&& 
					N_total_run_count = N_total_run_count + 1
			
				
			ENDcase
			
			
			DO CASE
				CASE LEN(ALLTRIM(edi_cms.terms))>0 									&& 3-26-16 ADDED 
					runline ="ITD*"+STRTRAN(ALLTRIM(EDI_CMS.TERMS),"~","")
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
			ENDCASE 		
			
			
			DO CASE
				CASE INLIST(edi_cms.interch_id,"611125") AND edi_cms.company="SEARS"
					runline =  "PID*S**VI*FL"												&& Product/Item Description												&& Hierarchical Level
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
			
			ENDCASE
			

			n_hl = 0																				&& varriable to keep track of the hierarchical Levels 
			n_ISS_NUM = 0 
			select items
			
			****** ----- line level detail --------********
			do while items.order = invoice.order and items.inpart=invoice.inpart and not eof()
				N_HL= N_HL+1				&& 12-16-08 Moved from just below skip because it was adding one extra at the end of the loop
				DO CASE
					
					CASE INLIST(edi_cms.interch_id,"611125") AND EDI_CMS.COMPANY="SEARS" &&& SEARS 
						n_ISS_NUM = n_ISS_NUM + items.quantp+items.quants
						* runline =  "IT1**"+alltrim(str(items.quantp+items.quants))+"*"+ALLTRIM(edi_log.qty_code)+"*"+ALLTRIM(STR(ITEMS.IT_UNLIST,10,2))+"**UP*"++ALLTRIM(STOCK.UPCCODE)										&& Baseline Item Data (Invoice)
						runline =  "IT1**"+alltrim(str(items.quantp+items.quants))+"*"+ALLTRIM(rec_id.un_measure)+"*"+ALLTRIM(STR(ITEMS.IT_UNLIST,10,2))+"**IN*"+ALLTRIM(REC_ID.COSTCO_NUM)+"*UP*"+ALLTRIM(STOCK.UPCCODE)								
						strtofile(runline+chr(13),c_tempname,.t.)
						N_total_run_count = N_total_run_count + 1
					
				ENDCASE 
			

				SELECT ITEMS
				SKIP
				
			ENDDO

			
			RUNLINE =  "TDS*"+alltriM(strtran(STR((INVOICE.AMOUNT-invoice.ntax-invoice.stax-invoice.ctax-invoice.itax),10,2),".",""))																	&& Total Monetary Value Summary
			strtofile(runline+chr(13),c_tempname,.t.)	
			N_total_run_count = N_total_run_count + 1
			

			
			runline =  "SE*"+alltrim(str((N_total_run_count+1),5,0))+"*"+alltrim(edi_vals.st)	
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
					
			replace invoice.authcode with ALLTRIm(invoice.authcode)+"-"+RIGHT(alltrim(edi_vals.gs),3)
			
			runline ="GE*1*"+alltrim(edi_vals.gs)
			strtofile(runline+chr(13),c_tempname,.t.) 
								
			runline ="IEA*1*"+alltrim(edi_vals.isa)
			strtofile(runline+chr(13),c_tempname,.t.)   
		  	
		  	 
		  	*Messagebox("Please note the 810 document control number "+alltrim(str(n_control_st,10,0)))
		  
			*modify command (c_tempname)
			*modify command (c_filename)
			rename (c_tempname) to (c_filename)

			DO CASE 
				CASE company.edi_id="8888513711"
	
					select master_prg_run_log
					&&New CHILKAT FTP method
					*c_filename="EDI_810_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+".001"
					C_FILENAME="M:\sterling\outbox\"+alltrim(company.edi_id)+"\EDI_810_"+alltrim(company.code)+alltrim(str(invoice.order,10,0))+alltrim(invoice.inpart)+".001"
					
					c_filedir="M:\users\sterling\outbox\"+alltrim(company.edi_id)+"\"
					
					l_goodtogo =.t.
					IF l_goodtogo
						*messagebox("control number ="+alltrim(str(n_control_gs,10,0)))
						DO h:\report\edi\ftp_Sterling2.prg	
						
						SELECT edi_cms
						
						REPLACE EDI_CMS.IN_810 WITH alltrim(edi_vals.gs)
						REPLACE EDI_CMS.IN_810_D WITH DATE()
						replace edi_log.in810_date with date()
						replace edi_log.in810_file with c_shortname 
					ELSE 
						*MESSAGEBOX(" File not sent") 
					ENDIF 
				
				
			ENDCASE 
		

		DO workorder
	
		SELECT invoice
		CONTINUE
	ENDDO 
*ELSE
	*MESSAGEBOX("OUR SEQUENCE FILE IS CORRUPT OR THIS CLIENT IS NOT SET UP FOR EDI< THIS PROGRAM WILL TERMINATE")
	
*ENDIF

STRTOFILE(TTOC(DATETIME())+' PROGRAM COMPLETE - '+c_program_name+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)	


PROCEDURE workorder

public wo_no
public client_account
public received_by
public date_received
public client_contact
public rush_order_requester
public project_title_name
public project_assigned_to
public Client_P_O
Public date_due
public date_completed
public project_management
public file_maintenance
public customer_service
public programming
public web_programming
public warehouse_spec_project
public reporting
public blank_catagory
public blank_catagory_units
public rush_charges
public instructions
public service
public time_units
public instructions1
public service1
public time_units1
public instructions2
public service2
public time_units2
public instructions3
public service3
public time_units3
public instructions4
public service4
public time_units4
public instructions5
public service5
public time_units5
public instructions6
public service6
public time_units6
public instructions7
public service7
public time_units7
public instructions8
public service8
public time_units8
public instructions9
public service9
public time_units9
public instructions10
public service10
public time_units10
public instructions11
public service11
public time_units11
public instructions12
public service12
public time_units12
public instructions13
public service13
public time_units13
public instructions14
public service14
public time_units14
public instructions15
public service15
public time_units15
PUBLIC SPECIAL_BILLING_INSTRUCTIONS

client_account 		= 	M_CLIENT
received_by			=	"Contract"
date_received		=	"Standing"
client_contact		=	" "
rush_order_requester=	"Standing"
project_title_name	=	"EDI for Sears"
project_assigned_to	=	"IT"
Client_P_O			=	"Standing"
date_due			=	dtoc(Date())
date_completed		=	dtoc(date())
project_management	=	0	
file_maintenance	=	0
customer_service	=	0
programming			=	0
web_programming		=	0	
warehouse_spec_project=	0
reporting			=	""
blank_catagory		=	"Order Management"
blank_catagory_units=	0.15
rush_charges		=	" "
instructions		=	"Process EDI documents for order :"+STR(invoice.order,10,0) 
service				=	"Odr mgmt"
time_units			=	.15
instructions1		=	"- Create and send Advance shipping notifications for above orders (856) "
service1			=	" "
time_units1			=	" "
instructions2		=	"- Match up returning acknowledge documents from customer (997)"
service2			=	" "
time_units2			=	" "
instructions3		=	"- Create and send electronic invoices for above orders (810)" 
service3			=	" "
time_units3			=	" "
instructions4		=	"- Match up returning acknowledge documents from customer (997) "
service4			=	" "
time_units4			=	" "
instructions5		=	" "
service5			=	" "
time_units5			=	" "
instructions6		=	" "
service6			=	" "
time_units6			=	" "
instructions7		=	" "
service7			=	" "
time_units7			=	" "
instructions8		=	" "
service8			=	" "
time_units8			=	" "
instructions9		=	" "
service9			=	" "
time_units9			=	" "
instructions10		=	" "
service10			=	" "
time_units10		=	" "
instructions11		=	" "
service11			=	" "
time_units11		=	" "
instructions12		=	" "
service12			=	" "
time_units12		=	" "
instructions13		=	" "
service13			=	" "
time_units13		=	" "
instructions14		=	" "
service14			=	" "
time_units14		=	" "
instructions15		=	" "
service15			=	" "
time_units15		=	" "
special_billing_instructions = c_program_name
C_LINE 				= 	"INSTRUCTIONS"	
C_LINE_nUM 			=   " "		&& TO HOLD THE COMMAND THAT WILL POPULATE THE INSTRUCTION SETS
N_RUN_num			= 0 

SELECT 103
use h:\report\work_order\work_order shared
REPLACE WO_NUMBER WITH WO_NUMBER +1 FOR RECNO()>0
GO TOP
wo_no = WO_NUMBER
*report form H:\report\work_order\work_order to print PROMPT NOCONSOLE 
*REPORT FORM h:\report\edi\test1 TO PRINTER prompt
DO h:\report\edi\pdfoutput

select 104
USE 'm:\'+alltrim(company.code)+'\reports\workorder' SHARED
APPEND BLANK 
replace workorder.wo_number WITH wo_no
replace workorder.by WITH received_by
replace workorder.ord_admin WITH blank_catagory_units
replace workorder.date WITH DATE()
COPY TO 'm:\'+alltrim(company.code)+'\reports\workorder'+ALLTRIM(STR(MONTH(DATE()))) XL5 FOR VAL(substr(DTOS(workorder.date),5,2))=VAL(STR(MONTH(DATE())))
