* program assumes we have two public varriables comming in 
*	t_country=mom formated country code
*	m_country=an uppercase country name country name 
* the file h:\report\fix\country is open and there is no national tax considered
* the final positon of the country file is important as we are going to pull the 
* tax from this field

&& 10-14-15 ADDED CHECK FOR ADP_CO OPEN 

select country
validhit=.f.		&& presume the untested data is bad

if t_country<>space(3).and.len(rtrim(t_country))=3  && we have a three digit country code 
	*set order to code
	TRY 
		SET ORDER TO CODE		&& WE USE THE MOM VERION FOR CLIENTS WHO HAVE NATIONAL TAX RATES
	CATCH
		SET ORDER TO COUNTRY && THE FIXCOUNTRY VERSION OF COUNTRY HAS AS THE INDEX COUNTRY 
	ENDTRY
	
**This was causing me some issues when the CODE index existed but was not in the TAG1 possition
**Using the above code which does the same exact thing but doesnt care what number the tag is assigned.
*!*		IF tag(1)="CODE"			&& THE MOM VERSION OF COUNTRY HAS AS THE INDEX CODE 
*!*			SET ORDER TO CODE		&& WE USE THE MOM VERION FOR CLIENTS WHO HAVE NATIONAL TAX RATES
*!*		ELSE
*!*			SET ORDER TO COUNTRY && THE FIXCOUNTRY VERSION OF COUNTRY HAS AS THE INDEX COUNTRY 
*!*		ENDIF

	seek alltrim(t_country)
	if found()
		validhit=.t.
		m_country=country.country
	endif
	*MESSAGEBOX("RULE1 FIRED")
ENDIF

** 4-21-15 ADDED TO BETTER GET COUNTRY WHEN WE HAVE AN ISO2 CODE

IF ! validhit AND LEN(ALLTRIM(t_country))=2
	SELECT 888
	IF ALIAS()<>"ADP_CO"					&& 10-14-15 ADDED CHECK 
		USE h:\report\audit\adp_co SHARED			
	ELSE
		*MESSAGEBOX("DO NOTHING ALREADY OPEN ")
	ENDIF

	LOCATE FOR iso2=ALLTRIM(UPPER(t_country)) 
	IF FOUND()
		validhit=.t.
		m_country=adp_co.country
	ENDIF 
	select country
	
	*MESSAGEBOX(T_RCOUNTRY)
ENDIF 

IF ! validhit AND LEN(ALLTRIM(t_rcountry))=2
	SELECT 888
	IF ALIAS()<>"ADP_CO"					&& 10-14-15 ADDED CHECK 
		USE h:\report\audit\adp_co SHARED			
	ELSE
		*MESSAGEBOX("DO NOTHING ALREADY OPEN ")
	ENDIF
	LOCATE FOR iso2=ALLTRIM(UPPER(t_rcountry)) 
	IF FOUND()
		validhit=.t.
		m_country=adp_co.country
	ENDIF 
	select country
	
ENDIF 

*** END OF CHANGE 4-21-15 
* If we don't find a valid country we will search on the country name to see if we can find a match 

if ! validhit.and.t_rcountry<>" "
	*MESSAGEBOX("RULE2 FIRED")
	GOT_INDEX = .F.
	FOR nCount = 1 to 100
		if !empty(tag(ncount))		&& checks tag in index
			IF tag(nCount)="NAME"
				GOT_INDEX = .T.
			ENDIF
		else
			exit
		endif
	endfor
	IF NOT GOT_INDEX 
		INDEX ON NAME TO TEMP
	ELSE
		set order to tag name				&& 6-26-08 IT SEEMS THAT NOT ALL COMPANIES HAVE THIS INDEX
	ENDIF
	*MESSAGEBOX(T_RCOUNTRY)
	seek PROPER(substr(t_rcountry,1,15))
	if found()
		m_country=country.country
		validhit=.t.
	else
	 	seek upper(subst(t_rcountry,1,10))
		if found()
			m_country=country.country
			validhit=.t.
		else
			seek upper(substr(t_rcountry,1,5))
			if found()
				m_country=country.country
				validhit=.t.
			else
				validhit=.f.
				m_country="999"				&& 04-21-15 CHANGED TO THIS 
				*m_country="173"			&& 04-20-15 REMOVED 
			endif
		endif
	endif
endif
* record error if we don't get a match in the above two tries at finding a match
if ! validhit
	c_error_code="C"
	*MESSAGEBOX("no find")
	*do tomom_log_errors
ELSE
	SELECT country 
	LOCATE FOR country = m_country  && need to have this pointed to the correct country in case we picked it up from adp_co
endif
			