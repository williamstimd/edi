* fixstate makes sure we have a valid state.  Some records are giving us Ohio, Texas, N.Y. ect.  
* This program attempts to standarize these to the 2 digit codes in the MOM stock file
*
*	03-25-00 	jc	The first case statement excluded a run against the zip file when we had 
*					a condition meet.  Moved the balance of the rules outside of the case statement
*					so the system would return the county data 
*					Also changed the case statement to initilize t_state to the corrected code so 
*					the other rules will fire with that data 
*	06-14-01	JC	made a change to only report zipcode/state mismatch when the state is not an APO or AE address
*	11-11-2003 	jc changed the fixstate program to work better when we have both a good country code and a good zip but a bad state
*	06-5-07		JC two quick changes for when we don't have any state code
* This program assume we have the following PUBLIC varriables in memory
*		t_state = target state
*		M_STATE = our return state
*		M_country = a valid mom country code
*		t_zip = a zipcode from the source file

badstate=0

IF T_COUNTRY="001"
	M_COUNTRY=SPACE(3)
	*MESSAGEbox("stop1")
	* this has to be a case statement because we need to look up the zipcode in order to get the county
	do case
		case upper(t_state)="N.M."
			t_state="NM"
			m_state="NM"
			M_COUNTRY=t_country
		case upper(t_state)="D.C."
			t_state="DC"
			m_state="DC"
			M_COUNTRY=t_country
		case upper(t_state)="N.Y."
			t_state="NY"
			m_state="NY"
			M_COUNTRY=t_country
		case upper(t_state)="NYC"
			t_state="NY"
			m_state="NY"
			M_COUNTRY=t_country
		case upper(t_state)="N.J."
			t_state="NJ"
			m_state="NJ"
			M_COUNTRY=t_country
		case upper(t_state)="P.A."
			t_state="PA"
			m_state="PA"
			M_COUNTRY=t_country
		case upper(t_state)="ARIZONA"		&& STATES WHERE THE FIRST TWO CHARACTERS WILL SHOW THE WRONG STATE
			t_state="AZ"
			m_state="AZ"
			M_COUNTRY=t_country
		case upper(t_state)="NEW YORK"		&& STATES WHERE THE FIRST TWO CHARACTERS WILL SHOW THE WRONG STATE
			t_state="NY"
			m_state="NY"
			M_COUNTRY=t_country
		case upper(t_state)="NEW JERSEY"
			t_state="NJ"
			m_state="NJ"
			M_COUNTRY=t_country
		case upper(t_state)="NEW MEXICO"
			t_STATE="NM"
			m_STATE="NM"
			M_COUNTRY=t_country
		case upper(t_state)="NEW H"
			t_STATE="NH"
			m_STATE="NH"
			M_COUNTRY=t_country
		case upper(t_state)="NEVADA"
			t_STATE="NV"
			m_STATE="NV"
			M_COUNTRY=t_country
		case upper(t_state)="MONTANA"
			t_STATE="MT"
			m_STATE="MT"
			M_COUNTRY=t_country
		case upper(t_state)="CON"
			t_STATE="CT"
			m_STATE="CT"
			M_COUNTRY=t_country
		case upper(t_state)="MIN"
			t_STATE="MN"
			m_STATE="MN"
			M_COUNTRY=t_country
		case upper(t_state)="MIS"
			m_STATE="MS"
			M_COUNTRY=t_country
			t_STATE="MS"
		case upper(t_state)="MON"
			m_state="MT"
			M_COUNTRY=t_country
			t_state="MT"
		case upper(t_state)="VIR"
			m_STATE="VA"
			M_COUNTRY=t_country
			t_STATE="VA"
		case upper(t_state)="MAN"
			m_STATE="MB"
			m_country="034"
			t_STATE="MB"
			t_country="034"
		case upper(T_state)="NEW BRUN"
			m_STATE="NB"
			m_country="034"
			t_STATE="NB"
			t_country="034"
		case upper(t_state)="NEWFOUN"
			t_STATE="NF"
			m_STATE="NF"
			t_country="034"
			m_country="034"
		case upper(t_state)="BC"
			t_STATE="NF"
			m_STATE="NF"
			t_country="034"
			m_country="034"
		case upper(t_state)="NSW"	&& 5-19-08 added to the rules to deal with Australian 
			t_STATE="NSW"
			m_STATE="NSW"
			t_country="012"
			m_country="012"
		
	endcase
	*MESSAGEbox("stop 2")
	SELECT ZIP
	SEEK "001"+SUBSTR(T_ZIP,1,5)
	
	if not eof() and t_state="  "		&& 6-5-07 in the case we dont have a state code and we have a zip code match, pick up the state code from the zipcode
		t_state=zip.state
		m_STATE=zip.state
		c_error_code="s"
		do H:\report\tomom2\tomom_log_errors
		select state
	endif
	*MESSAGEbox("stop 3")
	*MESSAGEbox("T_STATE->"+T_STATE+" ZIP->STATE"+zip.state)
	c_county=county.county		&& because soft seek is on we have the nearest zipcode for the county file
								&& on the web we have the record before it fails, here we have the record after it fails 
	do case						&& 11-11-03 changed to a case statement to add the case below. 
		case SUBSTR(T_ZIP,1,5)=substr(zip.zipcode,1,5) and upper(alltrim(c_city))=upper(alltrim(zip.city)) and SUBSTR(T_STATE,1,2)<>substr(ZIP.STATE,1,2)	
			M_STATE=ZIP.STATE						&& We have a good match on city name and zip but a incompatible state and therefore we can take the state from Zip 
			m_country=zip.country
		case SUBSTR(T_STATE,1,2)=substr(ZIP.STATE,1,2) 
			M_STATE=ZIP.STATE						&& WE HAVE A VALID 2 DIGIT STATE CODE
			m_country=zip.country
			*MESSAGEbox("stop 4")
			*MESSAGEbox(m_state)
		otherwise 
			*MESSAGEbox("stop 5")
			*MESSAGEbox(upper(ALLTRIM(T_STATE))+" state.name->"+upper(ALLTRIM(STATE.NAME)))
			select state
			IF upper(ALLTRIM(T_STATE))=upper(ALLTRIM(STATE.NAME))and state.name<>"  "  && THE STATE NAME MATCHES THE ZIP FILE
																						&& 11-30-04 changed to deal with a end of file problem with no state name 
				M_STATE=STATE.STATE
				m_country=state.country
				*MESSAGEbox(m_state)
			ELSE
				SELECT STATE						&& lets see if the target state in canada
				LOCATE FOR substr(STATE.STATE,1,2)=upper(SUBSTR(t_STATE,1,2))
				*browse
				*MESSAGEbox(upper(ALLTRIM(T_STATE))+" state.name->"+upper(ALLTRIM(STATE.NAME)))
				IF ! EOF()
					IF STATE.COUNTRY="034" and m_country="001"
						M_country=STATE.COUNTRY		&& change the country to canada
						select country 
						locate for country="034"	&& need to leave the country file in position for tax  	
						c_error_code="c"
						do H:\report\tomom2\tomom_log_errors
						select state
						M_STATE=STATE.STATE
					ELSE 
						if m_state <> "AP" or m_state <> "AE"	&& 6-14-01 check to make sure this is not an APO ar AE address
							M_STATE=STATE.STATE			&& we have a good state but it is not consistant with the zipcode
							c_error_code="3"
							m_country=t_country
							do H:\report\tomom2\tomom_log_errors
							*messagebox("log error")
						endif
					ENDIF
				ELSE
					LOCATE FOR SUBSTR(STATE.NAME,1,10)=SUBSTR(RTRIM(t_STATE),1,10) and t_state<>"  " && CHECK TO SEE IF THIS IS THE STATE SPELLED OUT
					* LOCATE FOR SUBSTR(STATE.NAME,1,10)=SUBSTR(RTRIM(t_STATE),1,10) changed in favor of the line above because the search with otherwise stop at the top of the file
					IF ! EOF()
						M_STATE=STATE.STATE
						IF STATE.COUNTRY="034" and m_country="001"	&& MATCH WITH A CANADIAN PROVINCE CHANGE THE COUNTRY CODE
							M_COUNTRY=STATE.COUNTRY
							select country 
							locate for country="034"	&& need to leave the country file in position for tax  	
							c_error_code="c"
							do H:\report\tomom2\tomom_log_errors
							select state
						ENDIF
					ELSE
						c_error_code="S"
						do H:\report\tomom2\tomom_log_errors
						select state
					ENDIF
				endif
			ENDIF
	ENDcase
ENDIF		&& country =001

IF T_COUNTRY="034"
	M_COUNTRY=SPACE(3)
	SELECT STATE
	LOCATE FOR SUBSTR(LTRIM(T_STATE),1,2)=substr(STATE.STATE,1,2)
	IF ! EOF().AND.STATE.COUNTRY="034"
		M_STATE=T_STATE	
		m_country=t_country							&& GOOD MATCH AND WE CAN USE THIS DATA IN THE PILIOT FILE
	ELSE
		LOCATE FOR UPPER(SUBSTR(LTRIM(T_STATE),1,10))=UPPER(SUBSTR(STATE.NAME,1,10))
		IF ! EOF().AND.STATE.COUNTRY="034"
			M_STATE=STATE.STATE						&& CUSTOMER SPELLED OUT THE STATE
			m_country=t_country
		ELSE
			DO CASE									&& WE WILL HAVE TO ASSIGN A STATE CODE BASED ON THE ZIP CODE
				CASE UPPER(SUBSTR(T_ZIP,1,1))="A"
					M_STATE="NF"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="B"
					M_STATE="NS"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="C"
					M_STATE="PE"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="E"
					M_STATE="NB"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="G"
					M_STATE="PQ"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="H"
					M_STATE="PQ"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="I"
					M_STATE="PQ"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="J"
					M_STATE="PQ"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="K"
					M_STATE="ON"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="L"
					M_STATE="ON"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="M"
					M_STATE="ON"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="N"
					M_STATE="ON"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="O"
					M_STATE="ON"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="P"
					M_STATE="ON"
					
				CASE UPPER(SUBSTR(T_ZIP,1,1))="R"
					M_STATE="MB"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="S"
					M_STATE="SK"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="T"
					M_STATE="AB"
					
				CASE UPPER(SUBSTR(T_ZIP,1,1))="V"
					M_STATE="BC"
				
				CASE UPPER(SUBSTR(T_ZIP,1,1))="X"			&& 5-22-13 NEED TO CHECK IF IT IS NT OR NU
					*M_STATE="NT"
				CASE UPPER(SUBSTR(T_ZIP,1,1))="Y"
					M_STATE="YT"
				OTHERWISE
					c_error_code="B"
					do H:\report\tomom2\tomom_log_errors
			ENDCASE
			m_country="034"
		endif  && look for state.dbf match 			
	endif	
ENDIF
if M_country="001" and isalpha(c_zipcode)
	*messagebox("we have an error that has been noted above with an error 3 code ") 	
		
endif 
*MESSAGEbox("last stop")
*MESSAGEbox(m_state)