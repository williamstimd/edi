* tomom_serial.prg puts a blank serial into the serial number file serial.dbf
*
*
* 10-4-00 jc	problems have shown up in Nuvomedia.  Thie program does not always set up a serial number
* 				record.  see change below 
select serial

if importserial = .t.
	append blank
	
	replace item with items.item
	replace status with "T"
	replace order with items.order
	replace item_id with items.item_id
	replace serial with c_serial
	
ELSE
	IF INLIST(ALLTRIM(UPPER(company.code)),'TLI','TLX','GIR') && Just do not want to break soemthing else.  The seek below does NOT work.
		SET FILTER TO serial.item=items.item AND serial.status='A' AND serial.order=0
		GO top
*!*			if ! found() 	&& 10-4-00  was if eof() but it with SET NEAR ON this may not be working 
*!*				append blank
*!*				replace item with items.item
*!*			ENDIF
		
		replace status with "T"
		replace order with items.order
		replace item_id with items.item_id
	else
		seek (items.item)+"A"+"0"
		if ! found() 	&& 10-4-00  was if eof() but it with SET NEAR ON this may not be working 
			append blank
			replace item with items.item
		ENDIF
		
		replace status with "T"
		replace order with items.order
		replace item_id with items.item_id
	ENDIF
		
ENDIF
&&browse