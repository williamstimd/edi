* this routine will log into MOM any details related to a import error or reasons an order is put on hold


select ordmemo
if cms.order<>ordmemo.order 		&& we have not yet had a reason to create a ordmemo for this order 
	append blank
	replace order with cms.order
endif


replace ordmemo.notes with " - Order imported with the following error(s):"+ chr(13)+chr(10)

MEM_ERR=RTRIM(err_log.ERRORS)
done=.f.
do while ! done 
	CUR_ERR=SUBSTR(MEM_ERR,1,1)
	
    do case
            case CUR_ERR="1"
                replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - No bill to address"
            case CUR_ERR="2"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - No bill to city"
            case CUR_ERR="3"			&& assigned in fix state
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Zip code does not match state"
            case CUR_ERR="4"			&& assigned in tomom_items
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Quanity on an item missing"
            case CUR_ERR="5"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - No order number on the order"
            case CUR_ERR="6"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Catalog code does not match adcode"
            case CUR_ERR="A"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Missing source_key"
            case cur_err="B"			&& assigned in fixstate
            	 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid Province Abbreviation "
            case CUR_ERR="C"			&&fixstate
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - In valid country name"
            case CUR_ERR="c"			&& assigned in fix state
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Changed country to Canada"
            case CUR_ERR="D"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Check payment without check #"
            case CUR_ERR="d"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid payment method"
            case CUR_ERR="E"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Credit card number missing"
            case CUR_ERR="F"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid credit card type"
            case CUR_ERR="G"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid credit card number"
            case CUR_ERR="H"			&& assigned in tomom_cms
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Expired credit card"
            case CUR_ERR="I"			&& assigned in tomom_items
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Pricing exception on item"
            case CUR_ERR="J"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid product code"
            case CUR_ERR="K"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Shipping price exception"
            case CUR_ERR="L"			&& assigned in tomom_final
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Tax calculation error"
            case CUR_ERR="M"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - check order without check"
            case CUR_ERR="N"			&& assigned in adp_tomom_remedy
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - CRS required to review comment-"+alltrim(c.comment)
                * 10-24-00 take out -  replace A->comment with F->COMMENT
            case CUR_ERR="O"			&& assigned in adp_tomom_remedy
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Invalid Ship method"				
            case CUR_ERR="P"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Purchase order on hold"
            case CUR_ERR="Q"
                 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Test Credit Card Order"
            case cur_err="S"		&& assigned in fixstate
            	 replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - State not found in US & Canada State / Province "
            case cur_err="T"		&& assigned in tomom_custb
            	  replace ordmemo.notes with rtrim(ordmemo.notes) + chr(13)+chr(10)+" - Tax exempt customer on hold "
    endcase
    *messagebox("1 "+mem_err)
    MEM_ERR =substr(mem_err,2,10)
    if len(alltrim(mem_err))=0
    	done= .t.
    endif
enddo

