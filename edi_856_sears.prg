** 12-07-06 JC 	This program has been designed to create a 856 order shipment notice for 
**				costco related orders 
** 
**
** 12-18-06	JC	The resulting files are sent via FTP to Sterling Informaiton Broker.  
**				our Mailbox Id : 	SJMINFTO 	- Data Slot outbound
**									SJMINFTI	- Data Slot inbound
**									SJMINFTR	- Report Slot
**				Password			4088482
**				Communications Id :	12			- 12=our phone number is our id 
**												  ZZ= our name is our id
**			prod Communicaitons Id	4088482282	- What Patrik set up with Sterling as our account
**									4088483525	- is what we use with GSX
**
** 				IP address for FTP 	209.95.224.133 default ports 20 and 21 data needs to be sent
**									Ascii (not binary) and anything over 512 bytes needs to be 
**									streamed.
** 
**				web address : 		sciftp.commerce.stercomm.com
**				
**  Note that you must initally log in the outbound slot and perform a change Directory command
**	to access any other slots.  Also be sure to enter the slots in all caps

**	Technical support hotline number for your mailbox is 877-432-4300.  

** 	in this program we use 
**							Segment Terminator 	-  a line feed ( Market point we use ~"
**							element seperator	-  * (Jc Penny uses a bell character)
**							subelement seperator-  > on the ISA line 

**	12-24-06	JC 	moved ST segment inside the order loop
**					created variable N_Inv_run_count to us in the GE segment	

**	12-26-06	JC	added code to deal with times when we dont have any orders to process and to move the send funciton indide the loop for good orders 
**	01-02-06 	JC 	added code to only run the routine if cms.userid="CCO" and invoice.authcode="  " instead of just authocode="  "
** 	04-16-07	JC	changed location of CDI
**  04-24-07 	JC  fixed bug that assumse the work station is mapped to M for ro-gilroy-cdi-1
** 	05-20-07	JC 	modify to allow auto run at 2 pm each day
**  07-31-08 	jC 	modified for generic use for new EDI clients
**  11-07-08 	JC  review for OSH
**  05-05-09		jc 	USED FOR PRODUCTION ON COSTCO DROP SHIP 
**  09-14-09 	JC 	converted from costco to be used with Target
** 	09-20-09 	JC 	additional work based on feedback from Srish Kumar channel Intelligence 
**  09-24-09 	JC  Changes as per Srish Kumar
**  11-16-09 	JC  needed to deal with writing to invocie when we hit eof in items 
**  11-17-09 	JC  added code to keep us from working on a order without a edi_cms record
**	02-22-10 	JC	changed opening edi_ucc128 to shared from exclusive
**  04-16-10 	JC	modify the program to send the old part number 01-0001 if we are reporting activity on part num 01-0011 
**  04-21-10 	JC 	additional hard coding required to match with rec_id 
** 	04-22-10 	jc 	moved the creation of the file and the first two lines within the if statment for finding a shiped order
**					so we don't create any emply  files any longer
**					and switched to master_prg_run_Log if we did not have any orders to send
** 	05-10-10 	JC 	modify to work with new 856 via GSX
**  05-27-10 	JC  addtional changes 
**  05-28-10 	JC 	change the td5 segment
**  08-08-10	JC	copy target to use for Sears
**					we are planning on using the carton pack structure
**
**

SET resource off
SET SAFETY OFF
CLEAR ALL

ON ERROR DO H:\REPORT\ADVANCED_ERR_HANDLER\ERR_HANDLE_CALL.PRG WITH ;
  ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
	  
	public C_program_name 
	public c_username
	public c_machine
	public c_currentpath
	public c_executable
	PUBLIC c_compile_date
	public c_exe_size
	PUBLIC c_home
	
	C_program_name = 'Sears 856'	&&<-------------Enter program name here!
	c_home = 'h:\report\edi'			&&<-------------Enter program home directory here!
	c_username = alltrim(substr(sys(0),at("#",sys(0))+1,len(sys(0))))  	&& capture the user name
	c_machine = alltrim(substr(sys(0),1,at("#",sys(0))-1))				&& capture the workstation 
	c_currentpath = sys(5)+sys(2003)									&& capture the current path 
	c_executable = SYS(16,0)	
	c_executable = STRTRAN(UPPER(c_executable),'\\SILACCI.RUSHORDER.COM\PUBLIC\','H:\')
	c_executable = STRTRAN(UPPER(c_executable),'\\SILACCI-FS-01.SILACCI.RUSHORDER.COM\REPLICATION\','H:\')	
	
	&&New code to find file size and last modification
	c_exe_size=SizeArray(adir(SizeArray,c_executable)+1)
	c_exe_size=ALLTRIM(STR(c_exe_size))
	c_compile_date=	DTOC(SizeArray(adir(SizeArray,c_executable)+2))

	**COMMENT THIS IF STATEMENT IF YOU DONT CARE IF YOUR PROGRAM IS BEING RUN IN ITS HOME DIRECTORY
*!*		IF JUSTEXT(c_executable)='EXE' AND OCCURS(STRTRAN(UPPER(C_HOME),'\\SILACCI.RUSHORDER.COM\PUBLIC\','H:\'),UPPER(c_executable))=0
*!*			&&This program is not being run from its designated home directory.
*!*			c_sc_mess="You are running this program from a copy!"+CHR(10)
*!*			c_sc_mess=c_sc_mess+"You must run this program from the "+c_home+" directory!"+CHR(10)
*!*			c_sc_mess=c_sc_mess+"If you need help to make a short cut, please see IT Department."+CHR(10)
*!*			c_sc_mess=c_sc_mess+"Press OK to Quit this Application."+CHR(10)
*!*			MESSAGEBOX(c_sc_mess,0,'Y U NO MAKE SHORTCUT?!')
*!*			ERROR 'PRG Execution from a copy not in the Home directory.'	
*!*		ENDIF

	**New Text logfile code
STRTOFILE(TTOC(DATETIME())+' PROGRAM START - '+IIF(VARTYPE(c_program_name)='U','UNKNOWN',c_program_name)+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)
@1,1 say " Program create an EDI 856 report based on invoice transactions for Sears"

public ftp_name
public ftp_login
public ftp_pword
public ftp_path
public ftp_send
public ftp_rename 
public ftp_bye
public ftp_filename
public ftp_script


IF vartype(n_target_order)="U"	&& This will let us know if we have already created these variables
	PUBLIC n_target_order
	public c_target_inpart
endif


cd h:\report\company

SELECT 1
use H:\mom\profile alias company shared
set order to code 
locate for code="ABL"
*do form h:\report\company\company to c_code	5-13-09 removed to go faster 
if code="ABL"
	c_code="yes"
	public m_dataloc
	*m_dataloc="\\ro-gilroy-ad-1\win_mom"+company.dataloc
	m_dataloc=alltriM(company.drive)+company.dataloc
	*messagebox(m_dataloc)
	M_CLIENT=PROPER(COMPANY.NAME)
	c_target_client = company.code
else
	quit
endif


select 101
if used("master_prg_run_log")
	select master_prg_run_Log
else
	select 101
	use H:\mom\master_prg_run_log shared
endif
append blank
replace prg_name with c_program_name
replace client with company.code
replace date with date()
replace time with time()
replace comments with "started program"
*!*	REPLACE FUTURE2 WITH STR(N_LAST_TRANS_ID)							&& this is our start point for the this run ( the last record we looked at for the last report 
*!*	replace future3 with str(n_last_trans_id) 							&& put this in for now in case we don't find any new record for this report



public c_filename 
public c_tempname 
public N_total_ref_needed
public N_total_run_count 
public runline

select 288
use h:\report\edi\edi_log shared
if ! file("h:\report\edi\edi_log.cdx")
	index on filename tag filename OF h:\report\edi\edi_log.cdx
	index on control_No tag control_no OF h:\report\edi\edi_log.cdx
	index on ponum tag ponum OF h:\report\edi\edi_log.cdx
	index on mom_order tag mom_order of h:\report\edi\edi_log.cdx
ENDIF
set order to mom_order


IF VAL(ISA)>100000000 and !EMPTY(company.edi_id) && we are expecting all of these companies to have edi data in the company profile
														&& this data is entered by the EDI programmer when we first set the client up for EDI trading
														&& and we need a unique control number at least 10 digits long 
	CD (m_dataloc)	
	select 31
	if file("edi_ucc128.dbf")			
		use edi_ucc128 shared						&& our location for our label data with the serialized shipment container SSCC Label 
	else
		use h:\report\edi\master_edi_ucc128 shared
		copy stru to edi_ucc128
		use edi_ucc128
	endif
	if not file("edi_ucc128.cdx")
		index on ordernum tag ordernum of edi_ucc128.cdx
		index on alltrim(ordernum)+number tag orderitem of edi_ucc128.cdx
	endif
	set order to orderitem
	
	SELECT 2
	USE BOX SHARED
	SET ORDER TO ORDER
	SET FILTER TO LEN(ALLTRIM(TRACKINGNO))>0
	
	select 3 
	use carrier shared
	set order to ca_code
	
	
	select 50					&& each client will have its own table in the client directory to hold customer specific trading data
	if file("EDI_cms.dbf")
		use edi_cms shared		&& each trading partner will have its own record in this table 
	else
		use h:\report\edi\master_edi_cms shared
		copy stru to edi_cms 
		use EDI_cms
		append blank
		messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("edi_CMS.cdx")
		index on order tag order OF edi_cms.cdx
		index on custnum tag custnum OF edi_cms.cdx
	endif
	set order to order

	select 4
	use cms shared
	set order to order
	set relation to shiplist into carrier 
	set relation to order into edi_cms additive
	
	select 6
	use h:\report\audit\adp_co shared
	set order to country

	select 7
	use cust shared  &&exclusive &&
	*set order to tag title of cust.cdx
	*index on title tag title of cust.cdx
	set order to custnum
	*set relation to altnum into edi_cust 
	set relation to country into adp_co additive
	
	select 8
	use stock shared
	set order to number
	
	select 9
	if file("REC_ID.dbf")
		use REC_ID shared		&& each trading partner will have its own record in this table 
	else
		use H:\report\edi_rec_id_master shared
		copy stru to REC_ID 
		use REC_ID
		append blank
		messagebox("This cleint needs to be set up for EDI, please fill out the next page to begin")
		edit
	endif
	if ! file("REC_ID.cdx")
		index on mom_itemid tag mom_itemid OF rec_id.cdx
	endif
	*use h:\mom\nap\rec_id.dbf shared					&&  costco sends to us the line item along with a record_id
	set order to mom_itemid 							&& 	we need to store this and match it back up with the item
														&&  sold when we send back the tacking number file.  
	select 10
	use items shared
	set order to inpart
	set relation to item into stock ADDITIVE
	set relation to item_id into rec_id additive
	set relation to alltriM(company.code)+alltrim(str(order,10,0))+inpart+item into edi_ucc128 additive
	
	select 11
	use invoice shared
	set relation to order into cms additive
	set relation to custnum into cust additive
	set relation to order into edi_log additive
	SET relation to str(order,10,0)+inpart into box additive
	set relation to str(order,10,0)+inpart into items additive
	
	N_Inv_run_count = 0					&& number of invocies sent
	*locate for cms.userid="CCO" and invoice.authcode="   "
	
	*locate for invoice.order=n_target_order and invoice.inpart=c_target_inpart
	N_target_order =  0
	c_Inpart = "A"
	send_message=""

	SELECT invoice
	DO WHILE NOT EOF()
	locate for ALLTRIM(cms.cl_key)="EDI_SEARS" AND EMPTY(INVOICE.AUTHCODE) AND EMPTY(box.trackingno) and inpart="A"
		IF EMPTY(edi_cms.shipweight) OR EMPTY(edi_cms.cartons) OR EMPTY(edi_cms.scac) OR EMPTY(EDI_CMS.trans_meth)
			DO CASE
				CASE ALLTRIM(company.code)="ABL"
					&& THIS ONE IS NOT READY TO GO 
					send_to = "dkanemura@rushorder.com,abachmeyer@rushorder.com" 
					send_from = "orders@rushorder.com" 
					send_subject = ALLTRIM(company.code)+ " Load 856 Data Required"
					send_cc = "" 
					send_bcc = "twilliams@rushorder.com"
					send_message = send_message + "We can not process a Sears 856 because missing data for Order #"+str(invoice.order,8,0) +CHR(13)+CHR(10)	
			ENDCASE
		ELSE 	 
		
			if edi_cms.order<>invoice.order
				** 11-17-09 added when we don't have a link with edi_cms
				
				SELECT 103
				USE h:\Report\email\FILENUMBER SHARED

				GO TOP
				n_FILENUMBER = FILENUMBER.N_FILENUM && we store this value for now the send program increments it and we place the new value in at the end of this routine
		 
				send_to = "jchapman@rushorder.com" && This was for Testing
				send_from = "ediorders@rushorder.com" && This was for Testing
				send_subject = "EDI 856 File Error"
				send_cc = "pahlin@RUSHORDER.COM" && This was for Testing
				send_bcc = "jchapman@rushorder.com" && This was for Testing
				send_message = "We can not process an 856 because no edi_cms record for Order #"+str(invoice.order,8,0)
				do H:\Report\email_new\sendHTML	
				*do H:\Report\email\send		&&** put this back in if you wan to use this code
				select 103 
				replace n_filenum with n_FILENUMBER
				select invoice 
				replace invoice.authcode with "NO edi_cms"
				quit
				
			endif
			
			C_tempNAME="m:\sterling\outbox\8888513711\SENT\EDI_856_SEARS_"+upper(alltrim(company.code))+alltrim(str(invoice.order,10,0))+upper(alltrim(invoice.inpart))+".001"
			C_FILENAME="m:\sterling\outbox\8888513711\EDI_856_SEARS_"+upper(alltrim(company.code))+alltrim(str(invoice.order,10,0))+upper(alltrim(invoice.inpart))+".001"
			
			select 102
			use h:\report\edi\edi_vals shared
			GO TOP
			replace edi_vals.isa with alltrim(str(val(edi_vals.isa)+1))
			replace edi_vals.gs with alltrim(str(val(edi_vals.gs)+1))
			replace edi_vals.st with alltrim(str(val(edi_vals.st)+1))
			c_control_isa = edi_vals.isa
			c_control_gs = edi_vals.gs
			C_CONTROL_ST = EDI_VALS.ST
					
			
																	
			@4,10 SAY "Now processing customer record #" +str(cust.custnum,8,0)
			@5,10 say "                        order  #"+str(invoice.order,8,0)
			runline = "ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*08*6111250008     *"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*U*00401*"+alltrim(c_control_isa)+"*0*P*>"
			*runline = "ISA*00*          *00*          *12*"+substr(company.edi_id,1,15)+"*08*6111250008     *"+substr(dtos(date()),3)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*U*00401*"+alltrim(c_control_isa)+"*0*T*>"
			strtofile(runline+chr(13),c_tempname,.f.)					&& This line of code creates the txt version of the file
		
			if edi_cms.interch_id="6111250050" or edi_cms.interch_id="6111250008" && for Sears
				runline = "GS*SH*"+ALLTRIM(company.edi_id)+"*"+ALLTRIM(edi_cms.interch_id)+"*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(C_control_gs)+"*X*004010VICS"  && BBB
			else
				runline = "GS*SH*"+alltrim(company.edi_id)+"*6111250008*"+dtos(date())+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(c_control_gs)+"*X*004010"  && normal
				*runline = "GS*SH*4088482282*COSTCOEWHSE*20"+substr(dtoc(date()),7,2)+substr(dtoc(date()),1,2)+substr(dtoc(date()),4,2)+"*"+substr(time(),1,2)+substr(time(),4,2)+"*"+alltrim(edi_vals.gs)+"*X*004010"
			endif
			strtofile(runline+chr(13),c_tempname,.t.)
			
			N_Inv_run_count = N_Inv_run_count +1	
			runline = "ST*856*"+alltrim(C_control_st)
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count =1
			 
			runline =  "BSN*00*"+alltrim(str(invoice.trans_id,10,0))+"*"+DTOS(date())+"*"+strtran(time(),":","")+"*0004"		&& Beginning Segment for Ship Notice and we are going to do shipment,order,packagind,item as the hieirachial structure (No pack structure) 
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
				****** ----- shipment level detail --------********
				
			runline =  'HL*1**S'																					&& Hierarchical Level for the shipment
			*strtofile(runline+chr(13),c_tempname,.t.)	
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			n_ctns_in_shipmt = ALLTRIM(edi_cms.cartons)
			n_gross_weight = ALLTRIM(edi_cms.shipweight)
			
			
			runline = "TD1**"+alltrim(STR(n_ctns_in_shipmt))+"****G*"+Alltrim(STR(n_gross_weight))+"*LB"
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			do case
				case edi_cms.interch_id="611125"  and cms.shiplist="OTH" && Sears
					runline = "TD5**2*"+ALLTRIM(edi_cms.scac)			
			ENDCASE
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1	

			IF  cms.shiplist="OTH"
				runline = "REF*BM*"+ALLTRIM(BOX.TRACKINGNO)		&& CONTACT FOR EDI PROBLEMS
				strtofile(runline+chr(13),c_tempname,.t.)
				N_total_run_count = N_total_run_count + 1
			ENDIF
			
			runline =  "PER*IC*JIM CHAPMAN*EM*edi@rushorder.com"		&& CONTACT FOR EDI PROBLEMS
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			IF BOX.SHIP_DATE>CTOD("  /  /  ")
				runline =  "DTM*011*"+DTOS(BOX.SHIP_DATE)		&& item DETAIL (SHIPMENT) (ACCEPTED AND SHIPPED) 
			ELSE
				runline =  "DTM*011*"+DTOS(INVOICE.INV_DATE)	
			ENDIF 
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			
			IF CMS.SHIPNUM>0
				SELECT CUST
				SEEK CMS.SHIPNUM
			ENDIF
			runline =  "N1*ST**92*"+UPPER(ALLTRIM(CUST.ALTNUM))
			strtofile(runline+"~"+chr(13),c_tempname,.t.)											&& 
			N_total_run_count = N_total_run_count + 1
			
			runline =  "N1*SF*PARTNERXROBOTICS"
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			
			runline =  'HL*2*1*O'																					&& Hierarchical Level for the Order 
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			runline = "PRF*"+ALLTRIM(edi_cms.po_num)+"***"+ALLTRIM(edi_cms.po_date)  &&+"***DS"								
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
		
			runline =  "PID*S**VI*FL"				
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			runline = "TD1**"+alltrim(STR(n_ctns_in_shipmt))+"****G*"+Alltrim(STR(n_gross_weight))+"*LB"
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			runline = "REF*DP*"+alltrim(EDI_CMS.WAREHOUSE)
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
		
			runline = "REF*IA*"+alltrim(EDI_CMS.VENDOR)
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			runline = "N1*BY**92*"+alltrim(EDI_CMS.BUYER)
			*strtofile(runline+chr(13),c_tempname,.t.)
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			IF EDI_CMS.STORE<>"   "
				runline =  "N1*Z7**92*"+alltrim(EDI_CMS.STORE)
				*strtofile(runline+chr(13),c_tempname,.t.)
				strtofile(runline+chr(13),c_tempname,.t.)
				N_total_run_count = N_total_run_count + 1
			ENDIF
			
			n_hl = 2				&& varriable to keep track of the hierarchical Levels 
			n_hlp = 2			&& previous hierachial level (stays the same for all loops within items ) 
			select items
			****** ----- line level detail --------********
			do while items.order = invoice.order and items.inpart=invoice.inpart and not eof()
				IF inlist(edi_cms.interch_id,"61112500") && for Sears 
					n_hl = n_hl +1
					runline =  "HL*"+alltrim(str(n_hl,3,0))+"*"+alltrim(str((n_hlp),3,0))+"*I"																	&& Hierarchical Level - for the Tare / pallet
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
					
				
					DO CASE      	
						CASE STOCK.UPCCODE="   " AND STOCK.DISTSTOCK ="    "
							runline =  "LIN**IN*"+ALLtRIM(REC_ID.COSTCO_NUM)+"*VN*"+alltrim(ITEMS.ITEM)	&& item identificaiton								
						CASE STOCK.UPCCODE<>"   " AND STOCK.DISTSTOCK ="    "
							runline =  "LIN**IN*"+ALLtRIM(REC_ID.COSTCO_NUM)+"*UP*"+alltrim(stock.upccode)+"*VN*"+alltrim(ITEMS.ITEM)		&& item identificaiton								
						CASE STOCK.UPCCODE="   " AND STOCK.DISTSTOCK <>"    "
							runline =  "LIN**IN*"+ALLtRIM(REC_ID.COSTCO_NUM)+"*IZ*"+alltrim(stock.DISTSTOCK)+"*VN*"+alltrim(ITEMS.ITEM)					
						CASE STOCK.UPCCODE<>"   " AND STOCK.DISTSTOCK <>"    "
							runline =  "LIN**IN*"+ALLtRIM(REC_ID.COSTCO_NUM)+"*IZ*"+alltrim(stock.DISTSTOCK)+"*UP*"+alltrim(stock.upccode)+"*VN*"+alltrim(ITEMS.ITEM)		&& item identificaiton								
				
					ENDCASE
				
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
					
					runline =  "SN1**"+ALLTRIM(STR(ITEMS.QUANTP+ITEMS.QUANTS))+"*EA"		&& item DETAIL (SHIPMENT) (ACCEP											&& Hierarchical Level
					strtofile(runline+chr(13),c_tempname,.t.)
					N_total_run_count = N_total_run_count + 1
					
	*!*					runline =  "SLN*"+ALLTRIM(REC_ID.PO101)+"**A*"+ALLTRIM(STR(ITEMS.QUANTP+ITEMS.QUANTS))+"*"+alltrim(REC_ID.UN_MEASURE)+"****SK*"+alltrim(ITEMS.ITEM)		&& item DETAIL (SHIPMENT) (ACCEPTED AND SHIPPED) 												&& Hierarchical Level
	*!*					strtofile(runline+chr(13),c_tempname,.t.)
	*!*					N_total_run_count = N_total_run_count + 1

					n_hl = n_hl +1

				ENDIF
				select items
				REPLACE ITEMS.CUSTOMINFO WITH ALLTRIM(ITEMS.CUSTOMINFO)+"EDI 856 File sent on :"+chr(10)+dtoc(date())+chr(10)+"EDI"+dtos(date())+strtran(time(),":","")+".001"
				skip
			enddo										
		
				
			runline =  "SE*"+alltrim(str((N_total_run_count+1),5,0))+"*"+alltrim(edi_vals.st)	
			strtofile(runline+chr(13),c_tempname,.t.)
			N_total_run_count = N_total_run_count + 1
			
			runline ="GE*1*"+alltrim(edi_vals.gs)
			strtofile(runline+chr(13),c_tempname,.t.)
		
			runline ="IEA*1*"+alltrim(edi_vals.isa)
			strtofile(runline+chr(13),c_tempname,.t.)   
		  
			copy file (c_tempname) to (c_filename) && COPY FROM TEMP COPY CREATED IN TARGET\SENT AND PUT IN IN STERLING\OUTBOX
			select invoice
			replace invoice.authcode with alltrim(edi_vals.gs)
			replace edi_log.asn_date with date() 
			replace edi_log.asn_file with c_filename

			
			&& Update Master Program Run Log
			select master_prg_run_log
			replace comments with "Program Completed"
			

			
		ENDIF	
		SELECT invoice
		CONTINUE
	ENDDO 
	IF !EMPTY(send_message)
		do H:\Report\email_new\sendhtml		&&** put this back in if you want to use this code
 	ENDIF 	
ELSE
	MESSAGEBOX("OUR SEQUENCE FILE IS CORRUPT OR THIS CLIENT IS NOT SET UP FOR EDI< THIS PROGRAM WILL TERMINATE")
	CANCEL 
ENDIF

STRTOFILE(TTOC(DATETIME())+' PROGRAM COMPLETE - '+IIF(VARTYPE(c_program_name)='U','UNKNOWN',c_program_name)+CHR(13)+CHR(10),ADDBS(c_home)+'RUNLOG.txt',1)
