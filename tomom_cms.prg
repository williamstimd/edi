* This program is designed to import data directly into mom from the tomom_load program
* when we have data with order information 
* 02-12-00 more work on the windows version
* 03-19-00 more work on this version
* 04-01-00 put the spaces in the cardnumber for easy lookup in mom 
* 10-22-00 added a box_id when we add this record 
* 05-15-01 fixed a tax problem 
* 08-22-01 added code to strip out stray characters from a credit card number 
* 01-10-02 	jc - changed tomom_cms.prg to correct badly formated credit card expiration dates 
* 06-07-02 	jc - added option for multiship control from the import file 
* 11-12-02 	JC - Changed code to put on hold all orders with cardnum= 4242424242424242 as a test order 
* 09-03-03 	jc - changed code to respect the c_taxexempt flag
* 04-03-04 	jc - added initialization of varriables that will hold value of merchandise by product tax class
*				 the value will be assigned in the tomom_items.prg and the conversion to tax rates will happen in final  
* 05-06-2004 	jc	- added unlock function to sequence to eliminate the freeze conditons
* 02-17-2005 jc - removed from tomom_cms the code that rights the credit card number into cust
* 11-08-2006 jc - added to trap duplicate orders from coming into the system 
* 06-02-2010 JC - changed to do a cc check regarless of paymethod, First street orders for wellcore are IN but we want to keep the card number on the order as well
*				  this change imports the card number and makes the order paymethod still be IN 
* 08-04-2011 JC - added lock before doing the write to cust
* 01-23-2012 jc	- additional locks before writting to cust
* 12-01-2014 JC	- added check for ord_freq so we dont get a string overflow 

set safety off
set delete on
*04-03-04 	jc - added initialization of varriables that will hold value of merchandise by product tax class
*				 the value will be assigned in the tomom_items.prg and the conversion to tax rates will happen in final  
*10-16-08	JC 	- added code to support Pay Pal type of credit cards
*03-19-10	JC	- changes make related credit cards the new store version stating with wellcore
*07-01-14	jc 	- added code to deal with credit cards that come over with just the first digit.

n_ntaxclass_A = 0 	&& Initialize the running total of National class "A" merch for this order
n_nbo_class_A = 0 	&& Initialize the running total of National class "A" merch on back order for this order
n_nbodclass_A = 0 	&& Initialize the running total of National class "A" drop ship merch on back order for this order

n_staxclass_A = 0 	&& Initialize the running total of State class "A" merch for this order
n_Sbo_class_A = 0 	&& Initialize the running total of state class "A" merch on back order for this order
n_Sbodclass_A = 0 	&& Initialize the running total of state class "A" drop ship merch on back order for this order

n_ntaxclass_B = 0 	&& Initialize the running total of National class "B" merch for this order
n_nbo_class_B = 0 	&& Initialize the running total of National class "B" merch on back order for this order
n_nbodclass_B = 0 	&& Initialize the running total of National class "B" drop ship merch on back order for this order

n_staxclass_B = 0 	&& keep track of non taxable merch by product class
n_Sbo_class_B = 0 	&& Initialize the running total of state class "B" merch on back order for this order
n_Sbodclass_B = 0 	&& Initialize the running total of state class "B" drop ship merch on back order for this order

n_ntaxclass_C = 0 	&& keep track of non taxable merch by product class
n_nbo_class_C = 0 	&& Initialize the running total of National class "C" merch on back order for this order
n_nbodclass_C = 0 	&& Initialize the running total of National class "C" drop ship merch on back order for this order

n_staxclass_C = 0 	&& keep track of non taxable merch by product class
n_Sbo_class_C = 0 	&& Initialize the running total of state class "C" merch on back order for this order
n_Sbodclass_C = 0 	&& Initialize the running total of state class "C" drop ship merch on back order for this order

n_ntaxclass_D = 0 	&& keep track of non taxable merch by product class
n_nbo_class_D = 0 	&& Initialize the running total of National class D" merch on back order for this order
n_nbodclass_D = 0 	&& Initialize the running total of National class D" drop ship merch on back order for this order

n_staxclass_D = 0 	&& keep track of non taxable merch by product class
n_Sbo_class_D = 0 	&& Initialize the running total of state class "D" merch on back order for this order
n_Sbodclass_D = 0 	&& Initialize the running total of state class "D" drop ship merch on back order for this order

n_ntaxclass_E = 0 	&& keep track of non taxable merch by product class
n_nbo_class_E = 0 	&& Initialize the running total of National class "E" merch on back order for this order
n_nbodclass_E = 0 	&& Initialize the running total of National class "E" drop ship merch on back order for this order

n_staxclass_E = 0 	&& keep track of non taxable merch by product class
n_Sbo_class_E = 0 	&& Initialize the running total of state class "E" merch on back order for this order
n_Sbodclass_E = 0 	&& Initialize the running total of state class "E" drop ship merch on back order for this order

@19,10 say "starting the import of orders alt_number : "+c_odr_num

* we are ready to check if the customer is already in our data base
select sequence
seek "CMS"
if ! found()
	select inlog
	replace problems with "No cust sequence"
	MESSAGEBOX("PROGRAM ABORTED NO CMS IN SEQUENCE FILE")
	quit
else
	try=0
	DONE=.F.
	n_cms=0
	do while ! done .and. try<10		&& 10 trys to get a lock on the sequence file
		if rlock()
			replace next_val with next_val+1
			n_cMS=next_val
			done=.t.
			unlock
		endif
		try=try+1
	enddo
    select cust
    if lock()											&& 8-4-11 added lock to system for costco drop ships.  It seems someone is always in this order
    	replace cust.custtype with "O"					&& overwrite the custtype with the type for an order if we can get a lock on this record 
    	replace cust->orderrec with n_cms				&& we don't need to retry if not lock because it likley means someone is in that cust record now
    	IF cust.ord_freq<9000							&& 12-1-15 added the field lenght in mom is 4 so we don't want to run the risk of going over 
			replace cust->ord_freq with CUST->ORD_FREQ +1	&& and this data is not all that critical to store since it will get updated if anyone looks at this order in mom at some point
    	endif
    endif
    select cms
    n_order_rec = N_order_rec +1
    nORDER = N_CMS
    if alltrim(c_internetid)<>" " AND c_internetid<>"NRI"	&& 11-28-17 it is okay to have this number repeated for Neato Jitterbit imports 
   		seek alltrim(UPPER(c_internetid))					&& 3-20-07 added trim to insure we dont pick up the 80 character lenght from tomom.session
    	if found() and cms.internetid<>"      "				&& 11-08-06 added to trap duplicate orders from comming in to the system 
    		*internetid already exists in mom 
	    	c_error_code="g"
	    	DO H:\report\tomom2\TOMOM_LOG_ERRORS
	    	SELECT Cms
    	endif
    ENDIF
    
    if alltrim(c_odr_num)<>" " AND INLIST(ALLTRIM(UPPER(company.code)),'LBI') && Added a check for select companies to also include alt_order as a valid field to look for dupes.
   		SELECT cms
   		IF SEEK(alltrim(UPPER(c_odr_num)),"cms","alt_order")
   		&& seek alltrim(UPPER(c_odr_num))						&& 3-20-07 added trim to insure we dont pick up the 80 character lenght from tomom.session
    	&& if found() and cms.alt_order<>"      "			&& 11-08-06 added to trap duplicate orders from comming in to the system 
    		*internetid already exists in mom 
	    	c_error_code="g"
	    	DO H:\report\tomom2\TOMOM_LOG_ERRORS
	    	SELECT Cms
    	endif
    ENDIF
    
    if alltrim(c_odr_num)<>" " AND INLIST(ALLTRIM(UPPER(company.code)),'NRI') && Added a check for Net Suite order number alt_order as a valid field to look for dupes.
   		SELECT cms  && 12-11-17
   		
   		IF SEEK(alltrim(UPPER(c_odr_num)),"cms","alt_order")
   		
   		&& seek alltrim(UPPER(c_odr_num))						&& 3-20-07 added trim to insure we dont pick up the 80 character lenght from tomom.session
    	 	if found() and cms.alt_order<>"      "	AND ALLTRIM(c_odr_num) = ALLTRIM(cms.alt_order)			&& 11-29-17  added to make sure c_odr_num is not just the first part of the cms.alt_order
    			*internetid already exists in mom 
    			
	    		c_error_code="j"
	    		DO H:\report\tomom2\TOMOM_LOG_ERRORS
	    		SELECT Cms
	    	endif
    	endif
    ENDIF
    
    append blank
    *MESSAGEBOX("WE HAVE A NEW CMS RECORD"+str(N_cms,10,0))
    replace cms->order with N_CMS
    replace cms->custnum with m_custnum
    replace cms->cl_key with upper(C_source_key)
    replace cms->odr_date with D_order_date
    ** below moved from inside the cc payment case statement, now we will check and store the cc number even if the paymethod is ck or in 
    if len(alltrim(c_cardnum))>14
  		
  		IF LEN(ALLTRIM(c_cardnum))=1
  			&& 7-1-14 added for when we only get the first digit of the card number
			c_cardnum = ALLTRIM(c_cardnum)+"XXX-XXXX-XXXX-XXXX"
		ENDIF
		
    	&& strip out nonnumerics from card numbers  -08-01 added this new routine unless we find an XXXX in pos 6 though 10
		IF SUBSTR(ALLTRIM(c_cardnum),6,4) = "XXXX"
			l_encrypt=.f.
			l_skip_mod10 = .t.
		else
			newstring = ""
			oldstring = C_cardnum
			do while len(alltrim(oldstring))>0
				if val(substr(oldstring,1,1))>0 or substr(oldstring,1,1)="0"
					newstring = newstring + substr(oldstring,1,1)
				endif
				oldstring = substr(oldstring,2)
			enddo
			c_cardnum = newstring
			&& end routine to strip out stray chrs from credit card
		endif
		if c_cardnum= "4242424242424242"
			 *this is a test credit card number
            c_error_code="Q"
        	DO H:\report\tomom2\TOMOM_LOG_ERRORS
        	SELECT Cms
		ENDIF
		
        *replace cust->CARDNUM WITH C_cardnum		&& 2-17-04 removed for now to see impact of not keeping this number here
        *replace cms->CARDNUM WITH C_CARDNUM
        if l_encrypt		&& used to determine if we are going to encrypt credit cards
			n_TAR_ORDER = N_CMS
        	C_TAR_CARDNUM = C_CARDNUM
        	DO H:\REPORT\CHARGE_CARDS\CRYPTO\ENCRYPT
            do case										&& later we should check to see that we take all these cards 
                 case c_cardtype="PP"					&& 10-16-08 pay pal transaction 
                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
                    replace cms->cardtype with c_cardtype
                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
                    replace cms.cardnum with substr(c_cardnum,1,1)+"XXX XXXXXX "+substr(c_cardnum,11)
                case substr(C_cardnum,1,1)="3"
                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "AM"
                    replace cms->cardtype with "AM"
                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
                    replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
                case substr(C_cardnum,1,1)="4"
                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "V"
                    replace cms->cardtype with "V"
                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
                    replace cms.cardnum with "4XXX XXXX XXXX "+substr(c_cardnum,13)
                case substr(C_cardnum,1,1)="5"
                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "MC"
                    replace cms->cardtype with "MC"
                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
               		replace cms.cardnum with "5XXX XXXX XXXX "+substr(c_cardnum,13)
                case substr(C_cardnum,1,1)="6"
                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "DI"
                    replace cms->cardtype with "DI"
                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
                	replace cms.cardnum with "6XXX XXXX XXXX "+substr(c_cardnum,13)
                otherwise
                    c_error_code="F"
            		DO H:\report\tomom2\TOMOM_LOG_ERRORS
            		SELECT Cms
            ENDCASE
            IF LOCK('cust')
	        	replace cust->cardtype with cms->cardtype
	        endif
        else
        	** this is company we don't need to encrypt
        	replace cms->CARDNUM WITH C_CARDNUM
        	&& If this has been pre-encrypted and formated do a revised version of the below do case 03/19/2010 PA
        	IF SUBSTR(ALLTRIM(c_cardnum),6,4) = "XXXX"
        		
        		do case										&& later we should check to see that we take all these cards 
	                 case c_cardtype="PP"					&& 10-16-08 pay pal transaction 
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                case substr(C_cardnum,1,1)="3"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "AM"
	                    replace cms->cardtype with "AM"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                case substr(C_cardnum,1,1)="4"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "V"
	                    replace cms->cardtype with "V"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    *replace cms.cardnum with "4XXX XXXX XXXX "+substr(c_cardnum,13)
	                case substr(C_cardnum,1,1)="5"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "MC"
	                    replace cms->cardtype with "MC"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	               		*replace cms.cardnum with "5XXX XXXX XXXX "+substr(c_cardnum,13)
	                case substr(C_cardnum,1,1)="6"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "DI"
	                    replace cms->cardtype with "DI"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    *replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                	*replace cms.cardnum with "6XXX XXXX XXXX "+substr(c_cardnum,13)
	                otherwise
	                    c_error_code="F"
	            		DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            		SELECT Cms
	            ENDCASE
	             IF LOCK('cust')&& 1-23-12 see below for locked command before write:  
	            	replace cust.cardtype WITH cms.cardtype
	            endif
        	else
	        	 do case										&& later we should check to see that we take all these cards 
	                 case c_cardtype="PP"					&& 10-16-08 pay pal transaction 
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                
	                 case c_cardtype="AF"					&& 05/09/2016 ADDED FOR FUTURE MOTION 
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    replace cms.cardnum with 'AFFIRM'
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                    
	                 case c_cardtype="AZ"					&& 12/15/2014 Payments by Amazon PA 12/15/2014
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    replace cms.cardnum with 'AMAZON'
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                
	                 case c_cardtype="JC"					&& 05-09-16 ADDED JBC FOR FUTURE MOTION ( WE WON'T HAVE A CARD NUMBER OR NEED TO ENCRYPT
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with 'AMAZON'
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	              
	                 case c_cardtype="DC"					&& 05-09-16 ADDED dINNERS CLUB FOR FUTURE MOTION ( WE WON'T HAVE A CARD NUMBER OR NEED TO ENCRYPT
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	                    replace cms->cardtype with c_cardtype
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with 'AMAZON'
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)    
	                    
	                case substr(C_cardnum,1,1)="3"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "AM"
	                    replace cms->cardtype with "AM"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,6)+" "+substr(c_cardnum,11)
	                    *replace cms.cardnum with "3XXX XXXXXX "+substr(c_cardnum,11)
	                case substr(C_cardnum,1,1)="4"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "V"
	                    replace cms->cardtype with "V"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    *replace cms.cardnum with "4XXX XXXX XXXX "+substr(c_cardnum,13)
	                case substr(C_cardnum,1,1)="5"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "MC"
	                    replace cms->cardtype with "MC"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)  8-10-10 REMOVED WHO KNOWS WHY IT WAS STILL IN 
	                    replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	               		*replace cms.cardnum with "5XXX XXXX XXXX "+substr(c_cardnum,13)
	                case substr(C_cardnum,1,1)="6"
	                    * 1-23-12 see below for locked command before write:  replace cust->cardtype with "DI"
	                    replace cms->cardtype with "DI"
	                    *replace cust.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                    replace cms.cardnum with substr(c_cardnum,1,4)+" "+substr(c_cardnum,5,4)+" "+substr(c_cardnum,9,4)+" "+substr(c_cardnum,13)
	                	*replace cms.cardnum with "6XXX XXXX XXXX "+substr(c_cardnum,13)
	                otherwise
	                    c_error_code="F"
	            		DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            		SELECT Cms
	            ENDCASE
	            IF LOCK('cust')
					replace cust.cardtype WITH cms.cardtype
				ENDIF 
            endif
      	endif
      	if ! l_skip_mod10
      		&& this card has already been cleared 3-18-10
            STORE 0.0 TO BIN
            STORE 0.0 TO RESULT
            store .t. to pass
            STORE LEN(rtrim(C_cardnum)) TO JIM
            IF JIM<>13.AND.JIM<>15.AND.JIM<>16
                STORE .T. TO BAD_LEN
            ELSE
                STORE .F. TO BAD_LEN
            ENDIF
            *this is the checksum digit
            STORE RTRIM(C_CARDNUM) TO CNUM
            temp = val(RIGHT(RTRIM(C_CARDNUM),1))
            *count is the total number of digits for which the check is calculated
            *it needs to be less than the total since the last digit is not included in the calculation
            STORE jim - 1 TO LONG
            *bin is the binary value used as a multiplies (start w/2)
            STORE 2 TO BIN
            STORE 0 TO addem
            do while long>0
                *start calculating with 2nd to last digit in cnum
                store val(substr(cnum,long,1)) to result
                STORE RESULT * bin TO PROD
                if prod>9
                    *subtract only 9 instead of 10 since you always ad 1 back in
                    STORE prod - 9 TO PROD
                endif prod>9
                *here is where you accumulate your additive total
                STORE addem + prod TO ADDEM
                *alternate the value of bin between 1 and 2
                if bin = 2
                    STORE 1 TO bin
                else
                    STORE 2 TO bin
                endif bin =2
                * count backwards to 1
                STORE LONG - 1 TO long
            enddo while long>0
            *remove the number in the 10's place
            do while addem >10
                STORE ADDEM-10 TO addem
            enddo
            store abs(addem - 10) to end
            if .NOT.BAD_LEN.AND.Temp=end
                store .t. to pass
            else
                store .f. to pass
            endif
            IF .not. PASS
                *this is a bad card
                c_error_code="G"
            	DO H:\report\tomom2\TOMOM_LOG_ERRORS
            	SELECT Cms
            ENDIF
        endif   && need mod 10 check
    
    ENDIF
    
    DO CASE && Added mainly for TBI, but will work for anyone that imports transactions as c_Paymentod="CC" and paytype eq "PP" or "AZ"
	    case c_cardtype="PP"	&& 10-16-08 pay pal transaction 
			* 1-23-12 see below for locked command before write:  replace cust->cardtype with c_cardtype
	        replace cms->cardtype with c_cardtype
	        replace cms.cardnum with 'PAYPAL'
	  	case c_cardtype="AF"					&& 05/09/2016 
            replace cms->cardtype with c_cardtype
            replace cms.cardnum with 'AFFIRM'
        case c_cardtype="AZ"					&& 12/15/2014 Payments by Amazon PA 12/15/2014
            replace cms->cardtype with c_cardtype
            replace cms.cardnum with 'AMAZON'
	endcase    	
    
	do case		&& 08-07-03 ADDED CODE TO FIX MALFORMED EXPIRATION DATES 
		case upper(c_expires)="JAN"
			c_expires="01"+substr(c_expires,4)
		case upper(c_expires)="FEB"
			c_expires="02"+substr(c_expires,4)
		case upper(c_expires)="MAR"
			c_expires="03"+substr(c_expires,4)
		case upper(c_expires)="APR"
			c_expires="04"+substr(c_expires,4)
		case upper(c_expires)="MAY"
			c_expires="05"+substr(c_expires,4)
		case upper(c_expires)="JUN"
			c_expires="06"+substr(c_expires,4)
		case upper(c_expires)="JUL"
			c_expires="07"+substr(c_expires,4)
		case upper(c_expires)="AUG"
			c_expires="08"+substr(c_expires,4)
		case upper(c_expires)="SEP"
			c_expires="09"+substr(c_expires,4)
		case upper(c_expires)="OCT"
			c_expires="10"+substr(c_expires,4)
		case upper(c_expires)="NOV"
			c_expires="11"+substr(c_expires,4)
		case upper(c_expires)="DEC"
			c_expires="12"+substr(c_expires,4)
	endcase
	do case								&& 01/08/02 added following code to fix a unformated expiration date 
		case len(alltrim(c_expires))=4 and substr(c_expires,2,1)="/"							&& 7/01 convert to 07/01
			c_expires="0"+substr(c_expires,1,2)+substr(c_expires,3,2)							&& EXPECTING "01/01"
		case len(alltrim(c_expires))=4 and substr(c_expires,2,1)=" "							&& 7 01 convert to 07/01
			c_expires="0"+substr(c_expires,1,1)+"/"+substr(c_expires,3,2)						&& EXPECTING "01/01"
		case len(alltrim(c_expires))=4 and substr(c_expires,2,1)="-"							&& 7-01 convert to 07/01
			c_expires="0"+substr(c_expires,1,1)+"/"+substr(c_expires,3,2)						&& EXPECTING "01/01"
		case len(alltrim(c_expires))=4 and substr(c_expires,2,1)="\"							&& 7\01 convert to 07/01
			c_expires="0"+substr(c_expires,1,1)+"/"+substr(c_expires,3,2)						&& EXPECTING "01/01"
		case len(alltrim(c_expires))=4 and substr(c_expires,2,1)="_"							&& 7_01 convert to 07/01
			c_expires="0"+substr(c_expires,1,1)+"/"+substr(c_expires,3,2)						&& EXPECTING "01/01"
		case len(alltrim(c_expires))=6 and substr(c_expires,2,1)="/"							&& 7/2001 convert to 07/01
			c_expires="0"+substr(c_expires,1,2)+substr(c_expires,5,2)							&& EXPECTING "01/01"
		case len(alltrim(c_expires))=7 and substr(c_expires,3,1)="/"							&& 07/2001 convert to 07/02
			c_expires=substr(c_expires,1,3)+substr(c_expires,6,2)								&& EXPECTING "01/01"
	endcase
    if not isnull(c_expires) AND LEN(ALLTRIM(c_expires))>0										&& 08-04-11 added LEN(ALLTRIM(c_expires))>0 
    	* 1-23-12 see below for locked command before write:  replace cust->exp with C_expires
    	IF LOCK('cust')
	        replace cust->exp with C_expires
	    endif
    	replace CMS->exp with C_expires
   	endif


    do case
	    case UPPER(c_PAYMETHOD)="IN"
	        replace cms->paymethod with "IN"
	        REPLACE CMS->CHECK WITH "C"
	        REPLACE CMS->CHECKNUM WITH C_REFERENCE
	        * 1-23-12 see below for locked command before write:  replace cust->due_days with N_terms  
	        IF LOCK('cust')
	        	replace cust->paymethod with "IN"
	        	replace cms.due_days with n_terms
	        endif
	    case UPPER(C_PAYMETHOD)="CC"
	    	replace cms->approval with c_approval
	        replace cms->paymethod with "CC"
	        REPLACE CMS->CHECK WITH "C"
	        REPLACE CMS->CHECKNUM WITH C_REFERENCE
	        if c_cardnum=space(19)
	            c_error_code="E"
	            DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            SELECT Cms
	        endif
	       IF LOCK('cust')
	        	replace cust->paymethod with "CC"
	        endif	
	       && 6-02-10 moved the check and the entry of the card number to above the paymethod case statement
	        do case
	            case 2000+val(substr(C_expires,4,2))>(year(date()))
	            case 2000+val(substr(C_expires,4,2))<(year(date()))
	            	IF LOCK('cust')
	                	replace cust->expired with .t.
	                endif
	                c_error_code="H"
	            	DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            	SELECT Cms
	            case 2000+val(substr(C_expires,4,2))=(year(date())).and.val(substr(C_expires,1,2))<month(date())
	                IF LOCK('cust')
	                	replace cust->expired with .t.
	                endif
	                c_error_code="H"
	            	DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            	SELECT Cms
	        endcase
	        
	    case UPPER(C_PAYMETHOD)="CK"
	        replace CMS->paymethod with "CK"
	        REPLACE CMS->CHECK WITH "C"
	        if C_reference=space(10)
	        	c_error_code="D"
	            DO H:\report\tomom2\TOMOM_LOG_ERRORS
	            SELECT Cms
	        endif
	        REPLACE CMS->CHECKNUM WITH C_REFERENCE
	        IF LOCK('cust')
	        	replace cust->paymethod with "CK"
	        endif
	    case upper(C_PAYMETHOD)="CO."
	        replace CMS->paymethod with "CO"
	        REPLACE CMS->CHECK WITH "C"
	        REPLACE CMS->CHECKNUM WITH C_REFERENCE
	        IF LOCK('cust')
	        	replace cust->paymethod with "CO"
	        endif
	    otherwise
	       	c_error_code="d"
	       	REPLACE CMS->CHECKNUM WITH C_REFERENCE
	        DO H:\report\tomom2\TOMOM_LOG_ERRORS
	        SELECT Cms
	ENDCASE
	
	replace cms->shiplist with C_SHIPVIA
    replace cms->tax with n_tax
    replace cms->shipping with n_shipping
    * data below is overwritten when there is a ship to address involved
    replace cms->shiptype with "B"
    replace cms->ntaxrate with country->ntaxr
    DO CASE  && changes made by JC 10-5-15
    	CASE cust.country="001"
   			** if country.country="001" 
    		replace cms->staxrate with state->taxrate
    		replace cms->ctaxrate with county->ctaxr
    		replace cms->itaxrate with zip->itaxr
    		**endif
   		CASE cust.country="034" OR m_country="034"
    		*if m_country="034"							&& 5-15-01 had to split this out from the above because we don't look for a county in canada and therefore it is possible to inherit a rate from a previous order's county
			replace cms.ntaxrate with country.ntaxr
			replace cms.staxrate with state.taxrate
			replace cms.ctaxrate with 0 
			replace cms.itaxrate WITH 0 
			*endif
		OTHERWISE
			replace cms.ntaxrate with country.ntaxr
			replace cms.staxrate with 0
			replace cms.ctaxrate with 0 
			replace cms.itaxrate with 0
			
	ENDCASE 
	if upper(c_taxexempt)<>"YES"
    	replace cms->taxship with state->taxship
    endif
    *messagebox("cms tax test cms 1="+str(cms.tax))
    replace cms->userid with upper(c_oper_id)
    replace cms->sales_id with upper(c_sales_id)
    replace cms->picked with .t.
    replace cms->shipmodify with .t.					
    replace cms->catcode with upper(c_catalog)			
    replace cms->alt_order with alltrim(C_odr_num)
    replace cms->internetid with alltrim(c_internetid)
    replace cms->notyetused with l_signature
    R_TAX =0
    R_Taxm=0
    R_notaxM=0	&& * 09-08-03 jc 	changed the way we keep the running tabulation for nontaxable merchandise in cms.vntm  

    select sequence
	seek "BOX"
	if ! found()
		select inlog
		replace problems with "No box sequence"
		MESSAGEBOX("PROGRAM ABORTED NO CMS IN SEQUENCE FILE")
		quit
	else
		try=0
		DONE=.F.
		n_box=0
		do while ! done .and. try<10		&& 10 trys to get a lock on the sequence file
			if rlock()
				replace next_val with next_val+1
				n_box=next_val
				done=.t.
				unlock
			endif
			try=try+1
		enddo
    	select box
    	append blank
    	replace box.box_id with n_box
    	replace box->order with cms->order
    	replace box->box with 1
    	replace box->status with "R"				&& if one of the items is back ordered this should be "B" for back ordered
    	replace box->shiplist with cms->shiplist
    	* replace box->zone with ????				&& done in the tomom_zone.prg
    	* replace box->c_table with????				&& done in the tomom_zone.prg
    	* replace box->calc_weight with ????		&& done in the tomom_items.prg
    	replace box->custnum with n_billto
    	* replace box->value with ????				&& done in the tomom_items.prg
    	replace box->pickrecno with c_thirdparty 	&& 5-1-06 this is the third party billing 
    endif && added a new box id value 
 	* loading of products was here
    select cms
    replace cms->prevord with m_lastord
    if l_multiship 								&& 06-07-02 added this option to set multship from the data file 
    	if cms->nbor>0.and.cms->nfill>0
        	replace cms->multiship with .t.
    	endif
    else
    	replace cms.multiship with .f.
    endif
    if cms->nfill>0
        replace cms->picked with .t.
    endif
    REPLACE CMS->ORD_TOTAL WITH CMS->ORD_TOTAL+CMS->SHIPPING+CMS->TAX  && we will recalc this again in final 
    do case
    	case cms.paymethod="IN"					&& we set this to BO if any item is backorderd in tomom_items
   	 		replace cms.order_st2 with "IN"		&& or to PE if an order is placed on hold in tomom_load

		case cms.paymethod="CC"
			replace cms.order_st2 with "CA"
	endcase
    replace cms.acc_state with "HP"

endif
if ! done 
	messagebox("I can't get a lock on the sequence file")
	quit
endif
*messagebox("cms tax test cms 2="+str(cms.tax))